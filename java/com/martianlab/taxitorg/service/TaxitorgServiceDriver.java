package com.martianlab.taxitorg.service;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import org.json.JSONArray;
import org.json.JSONException;
import com.martianlab.taxitorg.AppSettings;
import com.martianlab.taxitorg.common.Order;
import com.martianlab.taxitorg.handlers.GetOrdersHandler;
import com.martianlab.taxitorg.handlers.GetOrdersListener;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

/**
 * ������ ��������. ��������� ��������� � ������ �������. ���� ��������� ��������, ���������� ����������� � ���
 * ���� ��� - �� � ������� ������ ���
 * @author awacs
 *
 */
public class TaxitorgServiceDriver extends Service implements GetOrdersListener {
	
	private static final String TAG = "TaxitorgServiceDriver"; 
	
	private static final int TIMEOUT_60_MINUTS = 60; /* 1*60*60; */
	private static final int TIMEOUT_15_MINUTS = 60; /* 15*60; */
	private static final int TIMEOUT_1_MINUTE = 60; /*1*60;*/
	
	/**
	 * ������������� �������� ��������, �������� ��� ������ �������
	 */
	private long driverId = 0; 
	
	/**
	 * ��� ������� ������ � ���
	 */
	private Intent intent; 
	
	/**
	 * ������� ����� ������ �������
	 */
	private int timeoutSec;
	
	/**
	 * ������ ������ �������
	 */
	private Timer timer;
	
	/**
	 * ����� �������� ������� ������ ������ ��������
	 */
	private ArrayList<Order> orders;
	
	/**
	 * ���������� �� ����������� � ������ �������
	 */
	private boolean isNotification = true;
	
	public ArrayList<Order> changedOrders;
	
	public ServiceDriverBinder serviceDriverBinder = new ServiceDriverBinder();
	
	@Override
	public void onCreate(){
		// ��� �������� ������
		intent = new Intent(AppSettings.BROADCAST_ACTION_DRIVER);
		Log.d(TAG, "Taxitorg driver service started.");
		
		changedOrders = new ArrayList<Order>(); 
		
	}
	
	/**
	 * ����������� ������ onBind ������� ����� ����������������� � ���� ��������
	 */
	@Override
	public IBinder onBind(Intent arg0) {
		return serviceDriverBinder;	
	}
	
    public class ServiceDriverBinder extends Binder {
    	public TaxitorgServiceDriver getService() {
	        return TaxitorgServiceDriver.this;
	    }
	}
	
    /**
     * ��������� ����������� � ���
     * @param _notify
     */
    public void setNotification( boolean _notify ){
    	isNotification = _notify;
    }
    
    /**
     * ����� �������
     */
	@Override
	public void onStart(Intent intent, int startid) {
		Log.d(TAG, TAG+"::onStart");
		orders = new ArrayList<Order>();
	}	
	
	public long getDriverId(){
		return driverId;
	}
	
	
	public void setDriverId( long driver_id ){
		if( timer != null ){
			timer.purge();
			timer.cancel();
		}
		
		this.driverId = driver_id;
		timeoutSec = TIMEOUT_60_MINUTS;
		orders = new ArrayList<Order>();
		get_orders_list();
	}
	
	/**
	 * ����� ��� ������� ������������������ �������� �� �������
	 * @author awacs
	 *
	 */
	private class requestTimer extends TimerTask {
		public void run() {
			timer.purge();
			timer.cancel();
			get_orders_list();	
		 }
	};
	
	/**
	 * ���������� � �������� ������� �� ���� ������ ������� ��������
	 */
	private void get_orders_list(){
		if( driverId > 0 ){
			new GetOrdersHandler(this).getOrders(driverId);
		}
	}
	
	/**
	 * ���������� ������ �������, ������� �������� ���� ������
	 */
	private ArrayList<Order> getChangedOrders( ArrayList<Order> oldOrderList, ArrayList<Order> newOrderList ){
		ArrayList<Order> result = new ArrayList<Order>();
		for( Order newOrder : newOrderList ){
			for( Order oldOrder : oldOrderList ){
				if(    ( oldOrder.getId() == newOrder.getId() )
					&& ( oldOrder.getStatus() != newOrder.getStatus() )
				){
					result.add(newOrder);
				}
			}
		}
		return result;
	}
	
	/**
	 * ���� ������� ����� �������� � 15-�������� �������� ������ - ��� � ��� �������
	 *                 VoyageTime - 15min                                  VoyageTime + 15min 
	 *                           v                                             v
	 * -------------------------------------------------------------------------------------------------------> time
	 *                                         ^               ^
	 *                                     CurrentTime       VoyageTime
	 * @param order
	 * @return boolean
	 */
	private boolean isNearestOrder( Order order ){
		long currentTime = System.currentTimeMillis()/1000;
		long voyageTime  = order.getVoyageTime();
		if(        currentTime < voyageTime + TIMEOUT_15_MINUTS 
				&& currentTime > voyageTime - TIMEOUT_15_MINUTS 
		){
			return true;
		} else {
			return false;
		}
	}

	/**
	 * E��� ������� ����� �������� � 5-������� ��������, �� �� �������� � 15-�������� ��� � ��� ���������
     *
	 *                                        VoyageTime - 15min                            CurrentTime + 5hours    
	 *                                                   V                                           V
	 * -------------------------------------------------------------------------------------------------------> time
	 *                   ^                                       ^
	 *               CurrentTime                              VoyageTime
	 * @param order
	 * @return boolean
	 */
	private boolean isFutureOrder( Order order ){
		long currentTime = System.currentTimeMillis()/1000;
		long voyageTime  = order.getVoyageTime();
	
		if(    currentTime < voyageTime + TIMEOUT_60_MINUTS
			&& (voyageTime - TIMEOUT_15_MINUTS) > currentTime	
		){
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * ���������� ����� ��������� ��� ����� ��������� ������ �������
	 * @param countNearest
	 * @param countFuture
	 * @return int timeout
	 */
	private int calcTimeout( int countNearest, int countFuture ){
		int result = TIMEOUT_60_MINUTS; 
		if( countFuture > 0 ){
			timeoutSec = TIMEOUT_15_MINUTS;
		}
		if( countNearest > 0 ){
			timeoutSec = TIMEOUT_1_MINUTE;
		}
		
		return result;
	}
	
	
	/**
	 * ������� � ��������� ����� �� ������� �� ������� ������� �������
	 * @param ordersJsonArray
	 */
    public void post_get_orders_list(String value){
    	
    	// ����������� � JSONArray
		JSONArray ordersJsonArray = new JSONArray();
		try {
			ordersJsonArray = new JSONArray(value);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		
		// ������� ����� ������ ������� �������, ������� ���������� ��������� � ������� �������
    	ArrayList<Order> newOrderList = new ArrayList<Order>();
    	int countNearestOrders = 0;
    	int countFutureOrders = 0;
    	for( int i=0; i<ordersJsonArray.length(); i++ ){ 
    		try {
    			Order newOrder = new Order( ordersJsonArray.getJSONArray(i) );
    			newOrderList.add(newOrder);
    			if( isNearestOrder(newOrder) ){
    				countNearestOrders++;
    			}
    			if( isFutureOrder(newOrder) ){
    				countFutureOrders++;
    			}
			} catch (JSONException e) {
				e.printStackTrace();
			}
    	}
    	
    	// ������� ����� ����� ����������� ��������� ������
    	timeoutSec = calcTimeout( countNearestOrders, countFutureOrders );
    	Log.v(TAG, "Next request: " + timeoutSec );
    	
    	// ���������� � ������ ������� ������
    	changedOrders = getChangedOrders(orders, newOrderList); 
    	
    	// �������� ����� ������ ������� � �������
    	orders = new ArrayList<Order>();
    	for( Order order : newOrderList ){
    		orders.add(order);
    	}
    	
    	Log.d(TAG, "changedOrders.size() = " + changedOrders.size());
    	
    	if( changedOrders.size() > 0 ){
    		StringBuilder sb = new StringBuilder();
	    	for( Order order : changedOrders ){
	    		sb.append(order.getId());
	    		sb.append(",");
	    		sb.append(order.getStatus());
	    		sb.append(" ");
	    	}
	    	
	    	intent.putExtra("task", "changed_orders");
	    	intent.putExtra("results", sb.toString().trim());
	    	Log.d(TAG, "Send broadcast task:" + "changed_orders" + " result='"+sb.toString().trim()+"'");
	    	sendBroadcast(intent);
    		// ��������� � NotificationBar ��� ��������� ����������
    		if( isNotification ){
    		}
    	}
    	
    	timer = new Timer();
		timer.scheduleAtFixedRate(new requestTimer(), timeoutSec*1000, timeoutSec*1000);
    }

	@Override
	public void OnCompleteGetOrdersListener(String result) {
		post_get_orders_list(result);
		
	}

	@Override
	public void OnErrorGetOrdersListener(String error_msg) {
		post_get_orders_list(null);
	}	
	
}
