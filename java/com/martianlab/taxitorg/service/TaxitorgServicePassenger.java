package com.martianlab.taxitorg.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import org.json.JSONArray;
import com.martianlab.taxitorg.R;
import com.martianlab.taxitorg.AppSettings;
import com.martianlab.taxitorg.common.ActiveOrder;
import com.martianlab.taxitorg.common.DBAdapter;
import com.martianlab.taxitorg.handlers.CheckOrderHandler;
import com.martianlab.taxitorg.handlers.CheckOrderListener;
import com.martianlab.taxitorg.handlers.GetDriverListHandler;
import com.martianlab.taxitorg.handlers.GetDriverListListener;
import com.martianlab.taxitorg.passenger.ScreenSearchOffers;

import android.app.ActivityManager;
import android.app.Service;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

public class TaxitorgServicePassenger extends Service implements GetDriverListListener, CheckOrderListener {
	
	private static final String TAG = "TaxitorgServicePassenger";
	
	
	private Context context;
	
	/**
	 * ������� ������� ����� ���������
	 */
	private int timeoutSec = 5;
	
	/**
	 * ������
	 */
	private Timer timer; 
	
	/**
	 * ������ ��� �������������� ������
	 */
	private Intent intent;
	
	/**
	 * ��� �������� ���������� �������
	 */
	public ServicePassengerBinder servicePassengerBinder = new ServicePassengerBinder();

	
	/**
	 * ������� ������ �������
	 */
	private ArrayList<ActiveOrder> orders;
	
	/**
	 * ���� ������ ������� �������
	 */
	private DBAdapter db;
	
	final Handler handler = new Handler();
	
	@Override
	public void onCreate(){
		intent = new Intent(AppSettings.BROADCAST_ACTION_PASSENGER);
		db = new DBAdapter(getApplicationContext());
		context = getApplicationContext();
	}
	
	
	
	@Override
	public void onStart(Intent intent, int startid) {
		
		Log.i(TAG, TAG+"::onStart"); 
		
		// �������� ������ �������� �������, ������� ��������� ������������
		getActiveOrders();
		if( timer == null ){
			timer = new Timer();
			timer.schedule(new requestTimer(), 0, timeoutSec*1000);
		}
	}	
	
	@Override
	public void onDestroy() {
		Log.i(TAG, "OnDestroy");
		super.onDestroy();
	}
	
	
	/**
	 * ����������� ������ onBind ������� ����� ����������������� � ���� ��������
	 */
	@Override
	public IBinder onBind(Intent intent) {
		return servicePassengerBinder;
	}
    
    public class ServicePassengerBinder extends Binder {
        public TaxitorgServicePassenger getService() {
	        return TaxitorgServicePassenger.this;
	    }
	}
    
    
    public void addOrder( ActiveOrder order ){
    	if( orders == null ){
    		orders = new ArrayList<ActiveOrder>();
    	}
    	orders.add(order);
    } 
    
    public void deleteOrder( long order_id ){
		for( ActiveOrder order : orders ){
			if( order.order_id == order_id ){
				orders.remove(order);
			}
		}
    }
    
	/**
	 * ������ ������ ���������
	 * @param status
	 * @param driver_id
	 */
	public void setStatus(long orderId, int status, long driver_id, String comment ){
		Log.d(TAG, TAG+"::setStatus " + status);
		saveOrderStatus( orderId, status, comment );
		for( ActiveOrder order : orders ){
			if( order.order_id == orderId ){
				long current_time = System.currentTimeMillis()/1000;
				switch( status ){
					case AppSettings.ORDER_STATUS_OPEN:
					case AppSettings.ORDER_STATUS_PASSENGER_DECLINE_DRIVER:						
						Log.d(TAG, "Order ID " + orderId + " setStatus ORDER_STATUS_OPEN");
						order.current_status = AppSettings.ORDER_STATUS_OPEN;
						order.driver_id = 0;
						order.last_update = current_time;						
						break;
					case AppSettings.ORDER_STATUS_PASSENGER_ACCEPT_DRIVER:
						Log.d(TAG, "Order ID " + orderId +" setStatus ORDER_STATUS_PASSENGER_ACCEPT_DRIVER");
						order.current_status = AppSettings.ORDER_STATUS_PASSENGER_ACCEPT_DRIVER;
						order.driver_id = driver_id;
						order.last_update = current_time;
						break;
					case AppSettings.ORDER_STATUS_DRIVER_SAY_ARRIVE:
						Log.d(TAG, "Order ID "+ orderId + "setStatus ORDER_STATUS_PASSENGER_ACCEPT_DRIVER");
						order.current_status = AppSettings.ORDER_STATUS_CLOSE;
						order.last_update = current_time;
						break;
					case AppSettings.ORDER_STATUS_DRIVER_DECLINE_ORDER:
						Log.d(TAG, "Order ID "+ orderId + "setStatus ORDER_STATUS_DRIVER_DECLINE_ORDER");
						order.current_status = AppSettings.ORDER_STATUS_OPEN;
						order.last_update = current_time;
						break;
						
				}
			}
		}
	}
	
	/**
	 * ��������� ����� ������ ������ � ��������� ���� ������
	 * @param order_id
	 * @param status
	 */
	private void saveOrderStatus( long order_id, int status, String comment ){
		try{
			db.open();
			db.setOrderStatus(order_id, status, comment);
			db.close();
		} catch (Error e) {
			Log.e(TAG, e.getLocalizedMessage());
		}
	}
	
	/**
	 * ������, ����������� ��� ������������ �������
	 * @author awacs
	 *
	 */
    private class requestTimer extends TimerTask {
         public void run() {
       	    handler.post(new Runnable(){
       	    	public void run(){
       	    		refreshOrders();	
       	    	}
       	    });
         }
    };	
    
    private void refreshOrders(){
    	long current_time = System.currentTimeMillis()/1000;
    	
    	if( orders == null ){
    		orders = new ArrayList<ActiveOrder>();
    	}
    	
    	for( ActiveOrder order : orders ){
    		
    		// ������ ������
    		int order_status = order.current_status;
    		
    		// ����� ��������� ��� ���������� ������
    		long timeout = current_time - order.last_update;
    		if( (order_status > AppSettings.ORDER_STATUS_OPEN) ){
    			// �������� ������� ������ ��� �������� ��������� ������
				// TODO: � ����������� �� ��������� ������        			
    			if( timeout > 59 ){
    				order.last_update = current_time;
    				new CheckOrderHandler( TaxitorgServicePassenger.this ).checkOrder(order.driver_id, order.order_id);
    			} 
    		} else {
    			// ��������� ������ ��������� ��� �������� ������
				// TODO: � ����������� �� ��������� ������
    			if( timeout > 59 ){
    				order.last_update = current_time;
    				new GetDriverListHandler( TaxitorgServicePassenger.this ).GetDriverList(order.order_id);
    			}
    		}
    	}
    }    
    

	private void getActiveOrders(){
		try{
			db.open();
			orders = db.getOpenPassengerOrders();
		} catch( Error e ){
			Log.e(TAG, e.getLocalizedMessage());
		} finally {
			db.close();
		}
	}    

	@Override
	public void OnCompleteGetDriverList(long order_id, JSONArray list) {
		Log.d(TAG, "OnCompleteGetDriverList(), order_id = " + order_id);
		long current_time = System.currentTimeMillis()/1000;
		
		if( orders == null ){
			orders = new ArrayList<ActiveOrder>();
		} 
		
		for( ActiveOrder order : orders ){
			if( order.order_id == order_id ){
				order.last_update = current_time;
				if( list != null && list.length() > 0 ){
					NotifierHelper.sendNotification( 
			      			  context
			      			, ScreenSearchOffers.class
			      			, R.drawable.notify_taxitorg_passenger
			      			, context.getString(R.string.notify_service_passenger_header)
			      			, context.getString(R.string.notify_service_passenger_driver_offer_from_driver_title)
			      			, String.format( context.getString(R.string.notify_service_passenger_driver_offer_from_driver_description), order.address_start, order.address_end)
			      			, order_id
			      			, 1
			      			, true // vibro
				    );
				}
			}
		}
		
		if( list != null ){
			Log.d(TAG, "send broadcast driver_list");
			if( list.length() > 0 ){
				intent.putExtra("task", AppSettings.REQUEST_GET_DRIVER_LIST);
				intent.putExtra("order_id", String.valueOf(order_id));
		    	intent.putExtra("results",  list.toString());
		    	sendBroadcast(intent);
			}
    	}		
		
	}

	@Override
	public void OnErrorGetDriverList(long order_id, String error_msg) {
		Log.e(TAG, error_msg);
	}
	
	
	@Override
	public void OnErrorCheckOrder(long orderId, String error_msg) {
		Log.e(TAG, error_msg);
	}

    /**
     * �������� ������ ������
     * @param value - ������ ������
     */
	@Override
	public void OnCompleteCheckOrder(long order_id, int status, String comment) {
    	Log.d(TAG, "OnCompleteCheckOrder, order_id = " + order_id + " status = " + status + " comment = " + comment);
    	
    	long current_time = System.currentTimeMillis()/1000;
		//	�������� ������ ������, ��� ������� ���������� ��� �� ����� null, � ��� ��� ����
		if( orders == null ){
			orders = new ArrayList<ActiveOrder>();
		}    	
    	
    	for( ActiveOrder order : orders ){
	    	if( order.order_id == order_id ){
	    		order.last_update = current_time;
	    		// ���� ������ ������ ���������
	    		if( order.current_status != status ){
	    			Log.d(TAG, "Order with id = " + order_id + ", new status = " + status);
	    			// ��������� ������
	    			order.current_status = status;
	    			// ����� ������ ����������� ���������, ��� ��������� � ������ ��������
	    			//if( status == AppSettings.ORDER_STATUS_DRIVER_DECLINE_ORDER ){
	    			//	order.current_status = AppSettings.ORDER_STATUS_OPEN;
	    			//	order.driver_id = 0; 
	    			//}
	    			// ��������� ���� ������
	    			saveOrderStatus( order_id, order.current_status, comment );
			    	// ������� �����������
	    			sendNotification( order );	    			
			    	// ���� ������ ������ ���������, ���������� ��� � ��������
	    			Log.d(TAG, "Send broadcast change order");
		    		intent.putExtra("task", AppSettings.REQUEST_CHECK_ORDER_STATUS);
		    		intent.putExtra("order_id", String.valueOf(order_id));
					intent.putExtra("comment", comment);
		    		intent.putExtra("results", String.valueOf(status) );
					sendBroadcast(intent);    		    	
		    	}
	    	}
    	}
    }
	
	/**
	 * �����������
	 */
	private void sendNotification( ActiveOrder order ){
    	Log.d(TAG, "sendNotification, status = ");
		
		Date voyageTimeDate = new Date(order.voyage_time*1000);
		String voyageTimeFormatString = new SimpleDateFormat("dd-MM-yyyy hh:mm", Locale.getDefault()).format(voyageTimeDate);
    	
    	switch( order.current_status ){
			case AppSettings.ORDER_STATUS_DRIVER_DECLINE_ORDER : 
				// �������� ��������� �� ������
		    	Log.v(TAG, "AppSettings.ORDER_STATUS_DRIVER_DECLINE_ORDER");
		    	
		    	NotifierHelper.sendNotification( 
	        			  context
	        			, ScreenSearchOffers.class
	        			, R.drawable.notify_taxitorg_passenger
	        			, context.getString(R.string.notify_service_passenger_header)
	        			, context.getString(R.string.notify_service_passenger_driver_cancel_title)
	        			, String.format( context.getString(R.string.notify_service_passenger_driver_cancel_description), order.address_start, order.address_end, voyageTimeFormatString) 
	        			, order.order_id
	        			, 1
	        			, true // vibro
	        	);					
		        break;
			case AppSettings.ORDER_STATUS_DRIVER_SAY_DELAY :
				// �������� ������ ��� �������������
				Log.v(TAG, "AppSettings.ORDER_STATUS_DRIVER_SAY_DELAY");
				NotifierHelper.sendNotification( 
						context
						, ScreenSearchOffers.class
						, R.drawable.notify_taxitorg_passenger
						, context.getString(R.string.notify_service_passenger_header)
						, context.getString(R.string.notify_service_passenger_driver_delay_title)
						, String.format( context.getString(R.string.notify_service_passenger_driver_delay_description), order.address_start, order.address_end, voyageTimeFormatString)
						, order.order_id
						, 1
						, true // vibro
				);
				Log.d(TAG, "ORDER_STATUS_DRIVER_SAY_DELAY");
				break;
			case AppSettings.ORDER_STATUS_DRIVER_SAY_ARRIVE :
				// �������� ������ ��� �������
				Log.v(TAG, "AppSettings.ORDER_STATUS_DRIVER_SAY_ARRIVE");
				NotifierHelper.sendNotification( 
						  context
						, ScreenSearchOffers.class
						, R.drawable.notify_taxitorg_passenger
						, context.getString(R.string.notify_service_passenger_header)
						, context.getString(R.string.notify_service_passenger_driver_arrive_title)
						, String.format( context.getString(R.string.notify_service_passenger_driver_arrive_description), order.address_start, order.address_end, voyageTimeFormatString)						
						, order.order_id
						, 1
						, true // vibro
				);
				Log.d(TAG, "ORDER_STATUS_DRIVER_SAY_ARRIVE");
				break;
		}		
	}
	
}
