package com.martianlab.taxitorg.service;

import android.R;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

public class NotifierHelper {
    
	private static final int NOTIFY_1 = 0x1001;
    
	public static void sendNotification(Context caller, Class<?> activityToLaunch, int icon, String tickerText, String title, String msg, long order_id, int numberOfEvents, boolean vibrate) {
        NotificationManager notifier = (NotificationManager) caller.getSystemService(Context.NOTIFICATION_SERVICE);
 
        // Create this outside the button so we can increment the number drawn over the notification icon.
        // This indicates the number of alerts for this event.
        final Notification notify = new Notification(R.drawable.btn_plus, "", System.currentTimeMillis());
 
        notify.icon = icon;
        notify.tickerText = tickerText;
        notify.when = System.currentTimeMillis();
        notify.number = numberOfEvents;
        notify.flags |= Notification.FLAG_AUTO_CANCEL;
 
        if (vibrate) { 
            notify.vibrate = new long[] {100, 200, 200, 200, 200, 200, 1000, 200, 200, 200, 1000, 200};
        }
 
        Intent toLaunch = new Intent(caller, activityToLaunch);
        toLaunch.putExtra("order_id", order_id);
        
        PendingIntent intentBack = PendingIntent.getActivity(caller, 0, toLaunch, PendingIntent.FLAG_ONE_SHOT);
        
        notify.setLatestEventInfo(caller, title, msg, intentBack);
        notifier.notify(NOTIFY_1, notify);
    }
 
    public static void clear(Activity caller) {
        NotificationManager notifier = (NotificationManager) caller.getSystemService(Context.NOTIFICATION_SERVICE);
        notifier.cancelAll();
    }
}
