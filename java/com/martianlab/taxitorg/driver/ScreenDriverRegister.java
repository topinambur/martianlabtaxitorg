package com.martianlab.taxitorg.driver;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;

import com.martianlab.taxitorg.R;
import com.martianlab.taxitorg.AppSettings;
import com.martianlab.taxitorg.ScreenMainInfoActivity;
import com.martianlab.taxitorg.common.Driver;
import com.martianlab.taxitorg.handlers.RegisterHandler;
import com.martianlab.taxitorg.handlers.RegisterListener;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.text.Spannable;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.BufferType;
import android.widget.Toast;

/**
 * �������� ����������� ������ ��������
 * @author awacs
 *
 */
public class ScreenDriverRegister extends Activity implements RegisterListener {
	
	private static final String TAG = "ScreenDriverRegister";
	
	private EditText loginEditText;
	private EditText passwordEditText;
	private EditText passwordRetypeEditText;
	private EditText phonenumberEditText;
	private CheckBox rememberCheckBox;
	private CheckBox licenseCheckBox;
	private TextView licenseTextView;
	
	private Driver driver;
	
	private String login;
	private String password;
	private String passwordRetype;
	private String phonenumber; 
	private boolean remember;	
	
	private Button buttonRegister;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
    	super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_driver_register);
        
        // ���������
        TextView title = (TextView) this.findViewById(R.id.header_title);
        title.setText( getString(R.string.title_screen_driver_register));
        
        Button backButton = (Button)this.findViewById(R.id.header_backbutton);
        backButton.setOnClickListener(new OnClickListener(){
 			@Override
 			public void onClick(View v) {
 				finish();
 			}
        }); 
        
    	buttonRegister = (Button) findViewById(R.id.screen_driver_register_signup_button);
        loginEditText = (EditText) findViewById(R.id.screen_driver_registration_login);
        passwordEditText = (EditText) findViewById(R.id.screen_driver_register_password);
        passwordRetypeEditText = (EditText) findViewById(R.id.screen_driver_register_password_retype);
        phonenumberEditText = (EditText) findViewById(R.id.screen_driver_register_phonenumber);
        rememberCheckBox = (CheckBox) findViewById(R.id.screen_driver_register_remember);
        licenseCheckBox = (CheckBox) findViewById(R.id.screen_driver_register_license_checkbox);
        licenseTextView = (TextView) findViewById(R.id.screen_driver_register_license_label);

        // ���������� ������ ������������ ���������� � ������ �� ��������
        licenseTextView.setText( getString(R.string.button_screen_driver_register_license), BufferType.SPANNABLE );
        licenseTextView.setMovementMethod(LinkMovementMethod.getInstance());
        
        ClickableSpan clickSpan = new ClickableSpan() {
        	@Override
            public void onClick(View widget) {
                Intent intent = new Intent();
                intent.setClass(ScreenDriverRegister.this, ScreenMainInfoActivity.class);
                intent.putExtra("content", "license");
                startActivity(intent);
            } 
        };
        
        Spannable spannable = (Spannable)licenseTextView.getText();        
        spannable.setSpan(clickSpan, 0, licenseTextView.getText().length()-1,  Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        licenseCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener(){
			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {
				buttonRegister.setEnabled( isChecked );
			}
        });
    }
    
    /**
     * ���������� �������� � ����������
     * @param _login
     * @param _password
     * @param _remember
     */
    private void storeLoginInPreferences(String _login, String _password, boolean _remember){
    	SharedPreferences settings = getSharedPreferences(AppSettings.APP_PREFERENCES, Context.MODE_PRIVATE);
    	Editor editor = settings.edit();
    	editor.putString( AppSettings.APP_PREFERENCES_LOGIN, _login );
    	editor.putString( AppSettings.APP_PREFERENCES_PASSWORD, _password );
    	editor.putBoolean( AppSettings.APP_PREFERENCES_REMEMBER, _remember );
    	editor.putLong( AppSettings.APP_PREFERENCES_LASTLOGIN, System.currentTimeMillis()/1000 );
    	editor.commit();
    }    
    
    public void button_screen_driver_register_cancel_click(View v){
    	finish();
    }
    
    public void button_screen_driver_register_signup_click(View v){
    	register(); 
    }
    
    private void register(){ 
    	login = loginEditText.getText().toString();
    	password = passwordEditText.getText().toString();
    	passwordRetype = passwordRetypeEditText.getText().toString();
    	phonenumber = phonenumberEditText.getText().toString(); 
    	remember = rememberCheckBox.isChecked();
    	// TODO: �������� �������� �� ���������� �����/������/�������
 
    	if( password.equals(passwordRetype) ){
	    	driver = new Driver(login, password, phonenumber);
	    	List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		    nameValuePairs.add(new BasicNameValuePair("request",AppSettings.REQUEST_CREATE_DRIVER));
		    nameValuePairs.add(new BasicNameValuePair("login", driver.getLogin()));
		    nameValuePairs.add(new BasicNameValuePair("password", driver.getPassword()));
		    nameValuePairs.add(new BasicNameValuePair("phone", driver.getPhone() ));
		    String paramString = URLEncodedUtils.format(nameValuePairs, "utf-8");
	    	new RegisterHandler(this).execute(AppSettings.API_TAXITORG_DOMAIN + "?" + paramString);
	    	buttonRegister.setEnabled(false);
    	} else {
	        Toast toast = Toast.makeText(this, this.getString(R.string.error_passwords_not_equal), Toast.LENGTH_LONG);
	        toast.setGravity(Gravity.TOP, 0, 75);
	        toast.show();
    	}
    }

    
    /**
     * ����������� ���������. ���� driverId > 0 - �������� �������� ������������ ��������
     * @param driverId
     */
	@Override
	public void OnCompleteRegister(long driverId) {
		if( driverId > 0 ){
    		// ��������� ��������� �����/������ � �.�. ��� ����������� ���������
    		storeLoginInPreferences(login, password, remember);
    		
	    	driver.setId(driverId);
	    	// ���������� �������� �������� � ����������
	    	AppSettings.setDriver(driver);
	    	// �������� �������� ������������ ��������
    		Intent intent = new Intent();
	    	intent.setClass(this, ScreenDriverMain.class);
	    	intent.putExtra(AppSettings.KEY_DRIVER_ID, driverId);
	    	startActivity(intent);
	    	finish();
    	} else {
            Toast toast = Toast.makeText(this, "Error: Invalid driver ID", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.TOP, 0, 75);
            toast.show();
    	}
    	this.buttonRegister.setEnabled(true);		
    }

	@Override
	public void OnErrorRegister(String error_msg) {
        Toast toast = Toast.makeText(this, error_msg, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.TOP, 0, 75);
        toast.show();
        this.buttonRegister.setEnabled(true);
    }
    
}
