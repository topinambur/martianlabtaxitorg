package com.martianlab.taxitorg.driver;

import com.martianlab.taxitorg.R;
import com.martianlab.taxitorg.AppSettings;
import com.martianlab.taxitorg.common.ImageManager;
import com.martianlab.taxitorg.handlers.DownloadImageHandler;
import com.martianlab.taxitorg.handlers.DownloadImageListener;
import com.martianlab.taxitorg.handlers.UpdateDriverInfoHandler;
import com.martianlab.taxitorg.handlers.UpdateDriverInfoListener;
import com.martianlab.taxitorg.handlers.UploadImageListener;
import com.martianlab.taxitorg.handlers.UploadImageTask;

import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;

/**
 * ����� ��� �������������� �������������� ������ ��������
 * @author awacs
 *
 */
public class ScreenDriverUpdateInfo extends Activity implements UploadImageListener,  DownloadImageListener, UpdateDriverInfoListener {
	
	private static final String TAG = "ScreenDriverUpdateInfo";
	
	final int ACTIVITY_CAMERA_FACE = 1232;
	final int ACTIVITY_CAMERA_CAR = 1233;
	final int ACTIVITY_SELECT_IMAGE1 = 1234;
	final int ACTIVITY_SELECT_IMAGE2 = 1235;
	
    public static ImageView imageFace;
    public static ImageView imageCar;

    private ProgressDialog pd;
    
    private EditText nicknameEditText;
    private EditText brandEditText;
    private EditText modelEditText;
    private EditText yearEditText;
    private EditText maxPassengerEditText;
    private EditText annotationEditText;
    private CheckBox showPhoneCheckBox;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_driver_update_info);
        
        // ���������
        TextView title = (TextView) this.findViewById(R.id.header_title);
        title.setText( this.getString(R.string.title_screen_driver_update_info) );        
        
        Button backButton = (Button)this.findViewById(R.id.header_backbutton);
        backButton.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				finish();
			}
        });        

        Button completeButton = (Button)this.findViewById(R.id.header_completebutton);
        completeButton.setText(getString(R.string.button_save));
        completeButton.setVisibility(View.VISIBLE);
        completeButton.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				updateDriver();
			}
        });        
        
        nicknameEditText = (EditText)findViewById(R.id.screen_driver_update_info_nickname);
        showPhoneCheckBox = (CheckBox) findViewById(R.id.screen_driver_update_info_showphone);
        brandEditText = (EditText)findViewById(R.id.screen_driver_update_info_brand);
        modelEditText = (EditText)findViewById(R.id.screen_driver_update_info_model);
        yearEditText = (EditText)findViewById(R.id.screen_driver_update_info_year);
        maxPassengerEditText = (EditText)findViewById(R.id.screen_driver_update_info_max_passenger);
        imageFace = (ImageView) findViewById(R.id.image1_screen_driver_update_info);
        imageCar = (ImageView) findViewById(R.id.image2_screen_driver_update_info);
        annotationEditText = (EditText) findViewById(R.id.screen_driver_update_info_annotation);
        init();
    }
    
    /**
     * ������������� ����� �����
     */
    private void init(){
        nicknameEditText.setText(AppSettings.currentDriver.getNickname());
        brandEditText.setText(AppSettings.currentDriver.getBrand());
        modelEditText.setText(AppSettings.currentDriver.getModel());
        yearEditText.setText( String.valueOf(AppSettings.currentDriver.getYear()) );
        maxPassengerEditText.setText( String.valueOf(AppSettings.currentDriver.getMaxPassenger()) );
        annotationEditText.setText(AppSettings.currentDriver.getAnnotation()); 
        showPhoneCheckBox.setChecked( AppSettings.currentDriver.isShowPhone() );
        
        String path = Environment.getExternalStorageDirectory() + AppSettings.DIR_TAXITORG;
        
        // �������� ����������� � ������� ��� �������������
        if( AppSettings.currentDriver.getImage1() != null ){
        	imageFace.setImageBitmap( AppSettings.currentDriver.getImage1() ); 
        } else {
        	new DownloadImageHandler(this).getImage( AppSettings.currentDriver.getId(), AppSettings.POSTFIX_IMAGE_FACE, path );        	
        }        
        if( AppSettings.currentDriver.getImage2() != null ){
        	imageCar.setImageBitmap( AppSettings.currentDriver.getImage2() ); 
        } else {
        	new DownloadImageHandler(this).getImage( AppSettings.currentDriver.getId(), AppSettings.POSTFIX_IMAGE_CAR, path );        	
        }
    }
    
    // ===============================
    // Camera
    // ===============================
    
    public void button_screen_driver_update_info_camera1_click(View v){
    	Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        startActivityForResult(intent, ACTIVITY_CAMERA_FACE);    	
    }

    public void button_screen_driver_update_info_camera2_click(View v){
    	Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        startActivityForResult(intent, ACTIVITY_CAMERA_CAR);    	
    }
    
    public void button_screen_driver_update_info_gallery1_click(View v){
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(i, ACTIVITY_SELECT_IMAGE1);    	
    }
    
    public void button_screen_driver_update_info_gallery2_click(View v){
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(i, ACTIVITY_SELECT_IMAGE2);    	
    }    
    
	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    	
    	ImageManager im = new ImageManager(this, 240, 240);
    	switch( requestCode ){
    	    case ACTIVITY_CAMERA_FACE:
	            if( resultCode == RESULT_OK) {
	                try {
	                    // �������� ����������� � ������������ ��� � ������
		            	Bitmap bitmap = im.setIsScale(true)
		                	.setIsResize(true)
		                	.setIsCrop(true)
		                	.setUseOrientation(true)
		                	.getFromBitmap( (Bitmap) data.getExtras().get("data") );
	
		            	if( bitmap != null ){
			            	imageFace.setImageBitmap(bitmap);
			            	AppSettings.currentDriver.setImage1(bitmap);
			            	new UploadImageTask(this, AppSettings.POSTFIX_IMAGE_FACE).execute(bitmap);
		            	}
	                } catch (Exception e) {
	                    Toast.makeText(this, "Failed to load", Toast.LENGTH_SHORT).show();
	                    Log.e(TAG, e.toString());
	                }	            	
	            }
	            break;
    	    case ACTIVITY_CAMERA_CAR:
	            if( resultCode == RESULT_OK) { 
	                try {
	                    // �������� ����������� � ������������ ��� � ������
		            	Bitmap bitmap = im.setIsScale(true)
		                	.setIsResize(true)
		                	.setIsCrop(true)
		                	.setUseOrientation(true)
		                	.getFromBitmap( (Bitmap) data.getExtras().get("data") );
		            	if( bitmap != null ){
			            	AppSettings.currentDriver.setImage2(bitmap);
		                    imageCar.setImageBitmap(bitmap);
			                new UploadImageTask(this, AppSettings.POSTFIX_IMAGE_CAR).execute(bitmap);
		            	}
	                } catch (Exception e) {
	                    Toast.makeText(this, "Failed to load", Toast.LENGTH_SHORT).show();
	                    Log.e(TAG, e.toString());
	                }
	            }
	            
	            break;
            case ACTIVITY_SELECT_IMAGE1:
                if(resultCode == RESULT_OK){  
	            	Bitmap bitmap = im.setIsScale(true)
	                	.setIsResize(true)
	                	.setIsCrop(true)
	                	.setUseOrientation(true)
	                	.getFromFile(data.getData().toString());
                 	 
                    if( bitmap != null ){
	                	imageFace.setImageBitmap(bitmap);
	                    AppSettings.currentDriver.setImage1(bitmap);
	                    new UploadImageTask(this, 1).execute(bitmap);
                    }
                }            	
                break;
            case ACTIVITY_SELECT_IMAGE2:
                if(resultCode == RESULT_OK){  
	            	Bitmap bitmap = im.setIsScale(true)
	                	.setIsResize(true)
	                	.setIsCrop(true)
	                	.setUseOrientation(true)
	                	.getFromFile(data.getData().toString());
                    if( bitmap != null ){
	                    imageCar.setImageBitmap(bitmap);
	                    AppSettings.currentDriver.setImage2(bitmap);
	                    new UploadImageTask(this, 2).execute(bitmap);
                    }
                }            	
                break;                
       }
    }    
    
   
    // ========================================================
    // Download image
    // ========================================================
    
	@Override
	public void onDownloadImageComplete(int imageId, Bitmap data) {
		switch( imageId ){
		   case AppSettings.POSTFIX_IMAGE_FACE:
			   AppSettings.currentDriver.setImage1(data);
			   imageFace.setImageBitmap(data);
			   break;
		   case AppSettings.POSTFIX_IMAGE_CAR:
			   AppSettings.currentDriver.setImage2(data);
			   imageCar.setImageBitmap(data);
			   break;
		}	
	}
	
	@Override
	public void onDownloadImageError(String error_msg) {
	}	
	
	// =======================================================
	// Upload image
	// =======================================================
	
	@Override
	public void OnBeginUpload() {
		pd = new ProgressDialog(this);
		pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		pd.setMessage("Uploading Picture...");
		pd.setCancelable(false);
		pd.show();	
	}

	@Override
	public void OnProgressUpload(int progress) {
		pd.setProgress(progress);
	}

	@Override
	public void OnEndUpload() {
		pd.dismiss();
	}

	@Override
	public void OnErrorUpload(String errorMsg) {
		pd.dismiss();
	}	
	
	
	// =======================================================
	// Update driver
	// =======================================================
	
	/**
	 * �������� ������ � ����� � ���������� � AsyncTask ��� ����������� �������
	 */
	private void updateDriver(){
		AppSettings.currentDriver.setNickname( nicknameEditText.getText().toString() );
		AppSettings.currentDriver.setBrand( brandEditText.getText().toString() );
		AppSettings.currentDriver.setModel( modelEditText.getText().toString() );
		AppSettings.currentDriver.setYear( Integer.parseInt( yearEditText.getText().toString()) );
		AppSettings.currentDriver.setMaxPassenger( Integer.parseInt(maxPassengerEditText.getText().toString()) );
		AppSettings.currentDriver.setAnnotation( annotationEditText.getText().toString() );
		AppSettings.currentDriver.setShowPhone( showPhoneCheckBox.isChecked() );
        new UpdateDriverInfoHandler(this).updateDriver(AppSettings.currentDriver.getId(), AppSettings.currentDriver.getParams());
    }	
	
	@Override
	public void OnUpdateDriverInfoError(String error_msg) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void OnUpdateDriverInfoComplete() {
		finish();
	}    
    
}
