package com.martianlab.taxitorg.driver;

import java.util.ArrayList;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.PolylineOptions;
import com.martianlab.taxitorg.AppSettings;
import com.martianlab.taxitorg.R;
import com.martianlab.taxitorg.common.ErrorAlert;
import com.martianlab.taxitorg.common.Order;
import com.martianlab.taxitorg.handlers.RouteHandler;
import com.martianlab.taxitorg.handlers.RouteListener;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

/**
 * ����������� ���� � ������ �������� � ����������� � ������
 * TODO: �� ���� ���������� ���������
 * 
 * @author awacs
 *
 */
public class ScreenDriverOrderInfo extends FragmentActivity implements RouteListener {
	
	private static final String TAG = "ScreenDriverOrderInfo"; 
	
	private Order currentOrder;
	
	private GoogleMap mMap;
	
	/** 
	 * ����������� ���� � ����������� � ������
	 */
	private PopupWindow popUpOrderInfo;
	
	
	/** 
	 * ��������� �������� ��������
	 */
	private ArrayList<LatLng> route;
	
	/**
	 * 
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_screen_driver_order_info);
		
        Button backButton = (Button) findViewById(R.id.header_backbutton);
        backButton.setOnClickListener(new OnClickListener(){
        	@Override
			public void onClick(View v) {
        		finish();
			}
        });
        
        TextView title = (TextView) findViewById(R.id.header_title);
        title.setText(getString(R.string.title_screen_driver_order_list));		
		
        setUpMapIfNeeded();        
        
        Bundle extras = getIntent().getExtras();
        if( extras != null && extras.containsKey("order") ){
        	currentOrder = (Order) extras.getSerializable("order");
			calculateRoute(
					  currentOrder.getStartLatitude()
					, currentOrder.getStartLongitude()
					, currentOrder.getEndLatitude()
					, currentOrder.getEndLongitude()
					, AppSettings.FINE_ROUTE
			);
        }
        
	}
	
    /**
     * ������������� �����
     */
    private void setUpMapIfNeeded() {
        // �������������� ����� ��� �������������
    	if (mMap == null) { 
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.screen_driver_order_info_mapview)).getMap();
        }
    	
    }
    
    @Override
    protected void onPause(){
	   	 if( popUpOrderInfo != null && popUpOrderInfo.isShowing() ){
		     popUpOrderInfo.dismiss(); 
		 }    
	
		 super.onPause();
    }
    
    @Override
	public void onBackPressed(){
    	if( popUpOrderInfo != null && popUpOrderInfo.isShowing() ){
    		// ��������� ���� � ��������� �����������
    		popUpOrderInfo.dismiss();
    	} else {
    		super.onBackPressed();
    	}
    }    
    
    /**
     * ����� Google Navigator
     * @param v
     */
    public void screen_driver_order_info_button_navigator_click(View v){
    	StringBuilder uriSb = new StringBuilder();
    	uriSb.append("http://maps.google.com/maps?");
    	uriSb.append("saddr=");
    	uriSb.append(currentOrder.getStartLatitude()).append(",").append(currentOrder.getStartLongitude());
    	uriSb.append("&");
    	uriSb.append("daddr=");
    	uriSb.append(currentOrder.getEndLatitude()).append(",").append(currentOrder.getEndLongitude());
    	
    	Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uriSb.toString()) );
    	startActivity(intent);
    	
    }

    /**
     * �������� ���� � ����������� � ������
     * @param v
     */
    public void screen_driver_order_info_button_info_click(View v){
    	
    	Order order = currentOrder;
    	
		DisplayMetrics metrics = this.getApplicationContext().getResources().getDisplayMetrics();
    	int width = metrics.widthPixels;
    	int height = metrics.heightPixels;    	
    	
    	popUpOrderInfo = new PopupWindow(this); 
    	 
    	LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View popLayout = inflater.inflate(R.layout.popup_driver_order_info, null);
    	
        popUpOrderInfo.setContentView(popLayout);
        popLayout.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        popUpOrderInfo.setWidth( width );
        
        int defaultHeight = Math.round( height * 0.8f ); 
        
        if( defaultHeight > popLayout.getMeasuredHeight() ){
        	popUpOrderInfo.setHeight( defaultHeight );
        } else {
        	popUpOrderInfo.setHeight( popLayout.getMeasuredHeight() );        	
        }
		
        popUpOrderInfo.showAtLocation(popLayout, Gravity.CENTER, 0, 0);
        
        // ����� ��������
		TextView startAddress = (TextView) popLayout.findViewById(R.id.label_popup_driver_order_info_address_start);
		startAddress.setText(order.getAddressStart()); 
		 
		TextView endAddress = (TextView) popLayout.findViewById(R.id.label_popup_driver_order_info_address_end);
		endAddress.setText(order.getAddressEnd());
		
		// ������� � ���������
		LinearLayout meetingAeroportLayout = (LinearLayout) popLayout.findViewById(R.id.popup_driver_order_info_meeting_aeroport_layout);
		if( order.isAeroportMeeting() ){
			meetingAeroportLayout.setVisibility(View.VISIBLE); 
		} else {
			meetingAeroportLayout.setVisibility(View.GONE);
		}
		
		TextView flightNumberTextView =  (TextView) popLayout.findViewById(R.id.popup_driver_order_info_meeting_aeroport_flightNumber);
		flightNumberTextView.setText(order.getFlightNumber());
		CheckBox meetWithNameplate =  (CheckBox) popLayout.findViewById(R.id.popup_driver_order_info_meeting_aeroport_meetWithNameplate);
		meetWithNameplate.setVisibility(View.GONE);
		meetWithNameplate.setChecked( order.getMeetWithNameplate() );
		if( order.getMeetWithNameplate() ){
			meetWithNameplate.setVisibility(View.VISIBLE);
		}
			
		TextView aeroportAnnotationTextView =  (TextView) popLayout.findViewById(R.id.popup_driver_order_info_meeting_aeroport_annotation);
		aeroportAnnotationTextView.setText(order.getNamePlateText());
		
		// ������������
		TextView vehicleClassTextView = (TextView) popLayout.findViewById(R.id.popup_driver_order_info_vehicle_class);
		vehicleClassTextView.setText(order.getVehicleClass());
		
		CheckBox isPassengerSmoke =  (CheckBox) popLayout.findViewById(R.id.popup_driver_order_info_isPassengerSmoke);
		CheckBox isDriverSmoke =  (CheckBox) popLayout.findViewById(R.id.popup_driver_order_info_isDriverSmoke);
		CheckBox isConditioner = (CheckBox) popLayout.findViewById(R.id.popup_driver_order_info_isConditioner);
		CheckBox isChildSeat =  (CheckBox) popLayout.findViewById(R.id.popup_driver_order_info_isChildSeat);
		CheckBox isBaggage =  (CheckBox) popLayout.findViewById(R.id.popup_driver_order_info_isBaggage);
		CheckBox isSoberDriver =  (CheckBox) popLayout.findViewById(R.id.popup_driver_order_info_isSoberDriver);
		CheckBox isMyMusic =  (CheckBox) popLayout.findViewById(R.id.popup_driver_order_info_isMyMusic);
		
		isPassengerSmoke.setVisibility(View.GONE);
		isDriverSmoke.setVisibility(View.GONE);
		isConditioner.setVisibility(View.GONE);
		isChildSeat.setVisibility(View.GONE);
		isBaggage.setVisibility(View.GONE);
		isSoberDriver.setVisibility(View.GONE);
		isMyMusic.setVisibility(View.GONE);
		if( order.getIsPassengerSmoke() ){
			isPassengerSmoke.setVisibility(View.VISIBLE);
		}
		if( order.getIsDriverSmoke() ){
			isDriverSmoke.setVisibility(View.VISIBLE);
		}
		if( order.getIsConditioner() ){
			isConditioner.setVisibility(View.VISIBLE);
		}
		if( order.getIsChildSeat() ){
			isChildSeat.setVisibility(View.VISIBLE);
		}
		if( order.getIsBaggage() ){
			isBaggage.setVisibility(View.VISIBLE);
		}
		if( order.getIsSoberDriver() ){
			isSoberDriver.setVisibility(View.VISIBLE);
		}
		if( order.getIsMyMusic() ){
			isMyMusic.setVisibility(View.VISIBLE);
		}
		
		TextView annotation = (TextView) popLayout.findViewById(R.id.label_popup_driver_order_info_annotation);
		annotation.setText(order.getAnnotation());
    }    
    
    /**
     * ��������� ���� � ��������� �����������
     * @param v
     */
    public void button_popup_driver_order_info_cancel_click(View v){
    	if( popUpOrderInfo != null && popUpOrderInfo.isShowing() ){
    		popUpOrderInfo.dismiss();
    	}    	
    }
    
	// ===============================================================
	//  Calc Route
	// ===============================================================
	
    private void calculateRoute( double latStart, double lonStart, double latEnd, double lonEnd, int accuracy  ){
    	new RouteHandler( this ).calculateRoute(latStart, lonStart, latEnd, lonEnd, accuracy);
    }    
	
	private void drawRoute(){
		mMap.clear();
		mMap.addPolyline((new PolylineOptions().color(Color.BLUE).width(10)).addAll(route));		
	}
    
	@Override
	public void onRouteCalcBegin() {
	}

	@Override
	public void onBoundsCalculated(LatLng southwest, LatLng northeast) {
		LatLngBounds bounds = new LatLngBounds.Builder().include(southwest).include(northeast).build(); 
		try{
			mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 50));
		} catch( Exception e ){
			Log.e(TAG, e.getLocalizedMessage());
			e.printStackTrace();
		}
	}

	@Override
	public void onRouteCompleted(ArrayList<LatLng> r, long distance, long duration) {
		route = r;
		drawRoute();
	}

	@Override
	public void onRouteCalcError(String error_msg) {
		new ErrorAlert(this).showErrorDialog(this.getString(R.string.offline_title), error_msg);
	}

}
