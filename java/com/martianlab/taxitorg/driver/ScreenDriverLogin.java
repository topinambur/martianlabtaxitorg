package com.martianlab.taxitorg.driver;

import org.json.JSONArray;
import org.json.JSONException;

import com.martianlab.taxitorg.R;
import com.martianlab.taxitorg.AppSettings;
import com.martianlab.taxitorg.common.Driver;
import com.martianlab.taxitorg.common.ErrorAlert;
import com.martianlab.taxitorg.handlers.AuthorizeHandler;
import com.martianlab.taxitorg.handlers.AuthorizeListener;
import com.martianlab.taxitorg.service.TaxitorgServiceDriver;
import com.martianlab.taxitorg.service.TaxitorgServiceDriver.ServiceDriverBinder;

import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

/**
 * ���� ������/�����������
 * @author awacs
 *
 */

public class ScreenDriverLogin extends Activity implements AuthorizeListener {

	private final static String TAG = "ScreenDriverLogin";
	
	private EditText loginEditText;
	private EditText passwordEditText;
	private CheckBox rememberCheckBox;
	
	private String login;
	private String password;
	private boolean remember;
	
	private LinearLayout progress;
	 
	/**
	 * ��� ������� ��������
	 */
	private boolean isBoundDriverServer = false;
	private TaxitorgServiceDriver serviceDriver;
	private ServiceConnection serviceDriverConnection;	
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState); 
        setContentView(R.layout.activity_screen_driver_login);
        
        progress = (LinearLayout) findViewById(R.id.header_progress);
        
        Button backButton = (Button) findViewById(R.id.header_backbutton);
        backButton.setOnClickListener(new OnClickListener(){
        	@Override
			public void onClick(View v) {
        		finish();
			}
        });
        
        TextView title = (TextView) findViewById(R.id.header_title);
        title.setText(getString(R.string.title_screen_driver_login));
        
        loginEditText = (EditText) findViewById(R.id.screen_driver_login_login);
        passwordEditText = (EditText) findViewById(R.id.screen_driver_login_password);
        rememberCheckBox = (CheckBox) findViewById(R.id.screen_driver_login_remember);
        
		serviceDriverConnection = new ServiceConnection() {
		    public void onServiceConnected(ComponentName name, IBinder service) {
		        Log.d(TAG, "onServiceConnected");
		    	ServiceDriverBinder serviceDriverBinder = (ServiceDriverBinder) service;
		        serviceDriver = serviceDriverBinder.getService();
		        isBoundDriverServer = true; 
		        initScreen();
		    }
		    public void onServiceDisconnected(ComponentName name) {
		    	Log.d(TAG, "onServiceDisconnected");
		    	serviceDriver = null;
		    	isBoundDriverServer = false;
		    }
	    };        
	    
    }
    
    
    private void initScreen(){
    	loadLoginInPreferences();
    	if( serviceDriver.getDriverId() > 0 ){ 
    		//
    		// ���� ������ �������, ������ �� �������� ������ ���� ��� � AppSettings::currentDriver 
    		// ����� ������ ����������� ������ (driverId, ���������� �������)
    		//
    		Intent intent = new Intent();
    		intent.setClass(this, ScreenDriverMain.class);
    		intent.putExtra(AppSettings.KEY_DRIVER_ID, AppSettings.currentDriver.getId());
    		startActivity(intent); 
    		finish();
    	} else { 
    		//
        	// � ������ ��� �� ������� driver_id, ���������� ���������
    		//
    		if( remember ){
        		// �����/������ �� ��������
        		loginEditText.setText( login );
        		passwordEditText.setText( password );
        		rememberCheckBox.setChecked(remember);
        	}
        }	 
    }
    
    
	@Override 
	public void onResume() {
		super.onResume(); 
		if( !isBoundDriverServer ){
			Log.d(TAG, "bindService");
			bindService(new Intent(this, TaxitorgServiceDriver.class), serviceDriverConnection, Context.BIND_AUTO_CREATE);
		}
	}
	
    @Override
    protected void onPause(){
    	if( isBoundDriverServer ){
    		isBoundDriverServer = false;
			Log.d(TAG, "unbindService");
    		unbindService(serviceDriverConnection);
    	}
    	super.onPause();
    }		
	
    
    
    /**
     * ��������� �������� �� ����������� ��������
     */
    private void loadLoginInPreferences(){
    	SharedPreferences settings = getSharedPreferences(AppSettings.APP_PREFERENCES, Context.MODE_PRIVATE);
    	login = settings.getString(AppSettings.APP_PREFERENCES_LOGIN, "");
    	password = settings.getString(AppSettings.APP_PREFERENCES_PASSWORD, "");
    	remember = settings.getBoolean(AppSettings.APP_PREFERENCES_REMEMBER, false);
    	//lastlogin = settings.getLong(AppSettings.APP_PREFERENCES_LASTLOGIN, 0);
    }
    
    /**
     * ���������� �������� � ����������
     * @param _login
     * @param _password
     * @param _remember
     */
    private void storeLoginInPreferences(String _login, String _password, boolean _remember){
    	SharedPreferences settings = getSharedPreferences(AppSettings.APP_PREFERENCES, Context.MODE_PRIVATE);
    	Editor editor = settings.edit();
    	editor.putString( AppSettings.APP_PREFERENCES_LOGIN, _login );
    	editor.putString( AppSettings.APP_PREFERENCES_PASSWORD, _password );
    	editor.putBoolean( AppSettings.APP_PREFERENCES_REMEMBER, _remember );
    	editor.putLong( AppSettings.APP_PREFERENCES_LASTLOGIN, System.currentTimeMillis()/1000 );
    	editor.commit();
    }
    
    /**
     * ������ Login. �������� ������� ������������
     * @param v
     */
    public void button_screen_driver_main_login_click(View v){
    	login = loginEditText.getText().toString();
    	password = passwordEditText.getText().toString();
    	remember = rememberCheckBox.isChecked();
    	Driver driver = new Driver(login, password);
    	Log.d(TAG, "login="+driver.getLogin() + "pass="+ driver.getPassword());
    	new AuthorizeHandler(this).authorize(driver.getLogin(), driver.getPassword()); 
    }
    
    /**
     * ������ "�����������". ��������� ������ ����������� ������ ��������
     * @param v
     */
    public void button_screen_driver_main_signup_click(View v){
    	Intent intent = new Intent();
    	intent.setClass(this, ScreenDriverRegister.class);
    	startActivity(intent);
    	finish();
    }
    
    
    public void button_screen_driver_login_view_map_click(View v){
    	Intent intent = new Intent();
    	intent.setClass(this, ScreenDriverOrdersMap.class);
    	startActivity(intent);
    }


    @Override
	public void OnBeginAuthorize() {
    	progress.setVisibility(View.VISIBLE);
	}

    /**
     * ������ ����� ����� �����������
     * @param driverId
     */
	@Override
	public void OnEndAuthorize( JSONArray driverParamsJSONArray ) {
    	progress.setVisibility(View.INVISIBLE);
    	// ������ ������ � �������� �� �������
    	Driver driver = null;
    	try{
    		driver = new Driver(driverParamsJSONArray);
    	} catch( JSONException e ){
    		Log.e(TAG, e.getMessage());
    	}
    	
    	long driver_id = driver.getId(); 
    	
    	if( driver_id > 0 ){
        	storeLoginInPreferences(login, password, remember);

    		// ���������� �������� � AppSettings
	    	AppSettings.setDriver(driver);
	    	
	    	// �������� � ������ ������
	    	serviceDriver.setDriverId( driver_id );
	    	
	    	// ��������� layout � ��������� ������������ ��������
	    	Intent intent = new Intent();
	    	intent.setClass(this, ScreenDriverMain.class);
	    	intent.putExtra(AppSettings.KEY_DRIVER_ID, driver_id);
	    	startActivity(intent);
	    	finish();
    	}    	
	}

	/**
	 * ������ �����������
	 */
	@Override
	public void OnErrorAuthorize(String errorMsg) {
		progress.setVisibility(View.INVISIBLE);
		if( errorMsg.equalsIgnoreCase("Offline") ){
			new ErrorAlert(this).showErrorDialog(this.getString(R.string.offline_title), this.getString(R.string.offline_description));
		} else {
			new ErrorAlert(this).showErrorDialog(this.getString(R.string.auth_error_title), errorMsg);
		}
	}

}
