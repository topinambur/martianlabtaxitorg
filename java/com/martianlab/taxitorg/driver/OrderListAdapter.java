package com.martianlab.taxitorg.driver;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.martianlab.taxitorg.AppSettings;
import com.martianlab.taxitorg.R;
import com.martianlab.taxitorg.common.Order;

/**
 * ������� ��� ����������� ������ ������� �������� � ���� �����
 * @author awacs
 *
 */
public class OrderListAdapter extends ArrayAdapter<Order>{
	
	private Context context;
	private ArrayList<Order> orders;
	private int viewId;
	
	private OrderListListener listener;
	
	public OrderListAdapter(Context context, int textViewResourceId,	ArrayList<Order> orders) {
		super(context, textViewResourceId, orders);
		this.context = context;
		
		listener = (OrderListListener) context;
		
		
		this.orders = orders;
		this.viewId = textViewResourceId;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View row=inflater.inflate(viewId, parent, false);
		
		if( orders != null ){
			
			View descriptionLayout = (View) row.findViewById(R.id.driver_order_row_description_layout); 
			TextView addressStart = (TextView)row.findViewById(R.id.driver_order_row_address_start);
			TextView addressFinish = (TextView)row.findViewById(R.id.driver_order_row_address_finish);
			TextView price = (TextView)row.findViewById(R.id.driver_order_row_price);
			TextView currency = (TextView)row.findViewById(R.id.driver_order_row_currency);
			TextView status = (TextView)row.findViewById(R.id.driver_order_row_status);
			
			ImageButton declineOrderButton = (ImageButton) row.findViewById(R.id.row_driver_order_decline_button);
			ImageButton showMapButton = (ImageButton) row.findViewById(R.id.row_driver_order_map);
			ImageButton delayButton = (ImageButton) row.findViewById(R.id.row_driver_order_delay);
			ImageButton arriveButton = (ImageButton) row.findViewById(R.id.row_driver_order_arrive);
			ImageButton phone = (ImageButton) row.findViewById(R.id.row_driver_button_phone);
						
			final Order order = orders.get(position);
			addressStart.setText( order.getAddressStart() );
			addressFinish.setText( order.getAddressEnd() );
			price.setText( order.getPrice() );
			
			String currencyStr = "RUR";
			if( order.getCurrency().equalsIgnoreCase("rur") ){
				currencyStr = context.getString(R.string.rur);
			} else if ( order.getCurrency().equalsIgnoreCase("eur") ) {
				currencyStr = context.getString(R.string.eur);
			} else if ( order.getCurrency().equalsIgnoreCase("usd") ) {
				currencyStr = context.getString(R.string.usd);
			}

			currency.setText( currencyStr );
			
			if( !order.getPhonenumber().equals("") ){
				phone.setVisibility(View.VISIBLE);
			}
			
			long voyagetime = order.getVoyageTime();
			if( System.currentTimeMillis()/1000 > (voyagetime + AppSettings.TIMEOUT_ORDER_FOR_DRIVER) ){
				// ������������ ����� �������� �������
				descriptionLayout.setBackgroundColor( Color.RED );
			} else if( System.currentTimeMillis()/1000 + AppSettings.TIMEOUT_NEAREST_ORDER_FOR_DRIVER < voyagetime ){
			   // ������� ����� �������� �������
				descriptionLayout.setBackgroundColor( Color.GREEN );
			} else {
				// ������� ����� ���������� ������
				descriptionLayout.setBackgroundColor( Color.YELLOW );	
			}
			
			// ������ ������
			String statusStr = "";
			switch( order.getStatus() ){
			   case AppSettings.ORDER_STATUS_OPEN: 
				   // ������ 
				   statusStr = context.getString( R.string.text_screen_driver_order_list_order_open );
			       break;
			   case AppSettings.ORDER_STATUS_PASSENGER_DECLINE_DRIVER: 
				   status.setBackgroundColor(Color.RED);
				   // �����;
				   statusStr = context.getString( R.string.text_screen_driver_order_list_passenger_decline_driver );
			       break;
			   case AppSettings.ORDER_STATUS_PASSENGER_ACCEPT_DRIVER: 
				   //�����������
				   status.setBackgroundColor(Color.GREEN);
				   statusStr = context.getString( R.string.text_screen_driver_order_list_passenger_accept_driver );
			       break;
			   case AppSettings.ORDER_STATUS_PASSENGER_DECLINE_ORDER: 
				   status.setBackgroundColor(Color.RED);
				   // �������
				   statusStr = context.getString( R.string.text_screen_driver_order_list_passenger_decline_order );
				   break;
			   case AppSettings.ORDER_STATUS_DRIVER_SAY_DELAY: 
				   status.setBackgroundColor(Color.YELLOW);
				   // ������. 
				   statusStr = context.getString( R.string.text_screen_driver_order_list_driver_say_delay );
				   break;
			}
			 
			phone.setOnClickListener( new OnClickListener(){
				@Override
				public void onClick(View v) {
					listener.OnCallToPhone( order );
				}
			});
			
			status.setText(statusStr);
			
			showMapButton.setOnClickListener( new OnClickListener(){
				@Override
				public void onClick(View v) {
					listener.OnShowOrderOnMap(order); 
				}
			});
			
			// ������ ���������� �� ������
			declineOrderButton.setOnClickListener( new OnClickListener(){
				@Override
				public void onClick(View v) {
					listener.OnDeclineOrder(order);
				}
				
			});
			delayButton.setOnClickListener( new OnClickListener(){
				@Override
				public void onClick(View v) {
					listener.OnDelayOrder(order);
				}
				
			});
			arriveButton.setOnClickListener( new OnClickListener(){
				@Override
				public void onClick(View v) {
					listener.OnArriveOrder(order);
				}
			});
			
		}
		
		return row;
	}
}