package com.martianlab.taxitorg.driver;

import com.martianlab.taxitorg.common.Order;

public interface OrderListListener {
	void OnDeclineOrder( Order order );
	void OnDelayOrder( Order order );
	void OnArriveOrder( Order order );
	void OnCompleteOrder( Order order );
	void OnShowOrderOnMap( Order order );
	void OnCallToPhone( Order order );
}
