package com.martianlab.taxitorg.driver;

import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;

import com.martianlab.taxitorg.R;
import com.martianlab.taxitorg.AppSettings;
import com.martianlab.taxitorg.common.Driver;
import com.martianlab.taxitorg.common.Order;
import com.martianlab.taxitorg.handlers.GetOrdersHandler;
import com.martianlab.taxitorg.handlers.GetOrdersListener;
import com.martianlab.taxitorg.handlers.SetOrderStatusHandler;
import com.martianlab.taxitorg.handlers.SetOrderStatusListener;
import com.tjerkw.slideexpandable.library.SlideExpandableListAdapter;

import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.app.Activity; 
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;


public class ScreenDriverCurrentOrdersList extends Activity implements GetOrdersListener, SetOrderStatusListener, OrderListListener {
	
	private static final String TAG = "ScreenDriverCurrentOrdersList";
	
	/**
	 * ������� ������ �������
	 */
	private ArrayList<Order> orders;
	
	private long driverId = 1234567;
	
	private LinearLayout progress;
	
	private Button backButton; 
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        
    	super.onCreate(savedInstanceState); 
        setContentView(R.layout.activity_screen_driver_orders_list);
        
        getWindow().addFlags(LayoutParams.FLAG_KEEP_SCREEN_ON);
        
        progress = (LinearLayout) findViewById(R.id.header_progress);
        backButton = (Button) findViewById(R.id.header_backbutton);
        backButton.setOnClickListener(new OnClickListener(){
        	@Override
			public void onClick(View v) {
        		finish();
			}
        });
        
        TextView title = (TextView) findViewById(R.id.header_title);
        title.setText(getString(R.string.title_screen_driver_login));
        
        
        Bundle extras = getIntent().getExtras(); 
        if( extras != null ){
        	if( extras.containsKey("driver_id") ){
    	        driverId = extras.getLong("driver_id");
    	        get_orders_list();
        	}
        }
        
    }
    
    
    
	@Override 
	public void onResume() {
		super.onResume();
		// ����������� ��������� ��� ������� ��������
		Log.d(TAG, "registerReceiver " + AppSettings.BROADCAST_ACTION_DRIVER);
		registerReceiver(broadcastReceiver, new IntentFilter(AppSettings.BROADCAST_ACTION_DRIVER));
	}
	
    @Override
    protected void onPause(){
   		// ������� ��������� ��� ������� ��������
    	Log.d(TAG, "unregisterReceiver");
    	// �������� ��������� �����������
       	unregisterReceiver(broadcastReceiver);
    	super.onPause();
    }		
    
    private void get_orders_list(){
		// �������� ������ ������� � �������
		if( driverId > 0 ){
			progress.setVisibility(View.VISIBLE);
			new GetOrdersHandler(this).getOrders(driverId);
		} else {
			Log.e(TAG, "Invalid Driver ID");
		}
    }
    
    /**
     * ������ JSON ����� �� ������� ������� ��� ������� ��������
     * @param orders 
     */
    public void post_get_orders_list(String value){

		JSONArray ordersJsonArray = new JSONArray(); 
		try {
			ordersJsonArray = new JSONArray(value);
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}   

    	// ������������ JSON � ��������
    	orders = new ArrayList<Order>();
    	for( int i=0; i<ordersJsonArray.length(); i++ ){
    		JSONArray orderJSONArray;
			try {
				orderJSONArray = ordersJsonArray.getJSONArray(i);
	    		orders.add( new Order( orderJSONArray ) );				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}

    	orderListRedraw(orders);
    	
		progress.setVisibility(View.INVISIBLE);
    }
    
    private void orderListRedraw( ArrayList<Order> orders ){
    	OrderListAdapter adapter = new OrderListAdapter(this, R.layout.row_driver_order, orders);
		ListView list = (ListView)this.findViewById(R.id.screen_driver_current_orders_list);
		list.setAdapter(
			new SlideExpandableListAdapter(
				adapter,
				R.id.expandable_toggle_button,
				R.id.expandable
			)
		);    	
    }
    
    public void set_order_status( int order_status, long orderId, String comment ){
    	progress.setVisibility(View.VISIBLE);
    	Driver driver = new Driver();
    	driver.setId(driverId);
    	
    	new SetOrderStatusHandler(this).setStatus(orderId, driver, order_status, comment);
    }
    
    
    /**
     * �������� ��������� �� �������
     */
	private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String task = intent.getStringExtra("task");
			String results = intent.getStringExtra("results");
			Log.d(TAG, "Receive task:" + task + " result='"+results+"'");
			// ���������� ������ � ������� �������
			if( task.equals("changed_orders") ){ 
				String[] ids = results.split(" ");
				for( int i=0; i< ids.length; i++ ){
					String prms[] = ids[i].split(","); // id,status
					long id = 0;
					int status = 0;
					try{
						id = Long.parseLong(prms[0]);
						status = Integer.parseInt(prms[1]);
					} catch(NumberFormatException e){
						e.printStackTrace();
					}
					for( Order order : orders ){
						if( order.getId() == id ){
							order.setStatus(status);
						}
					}

				}
				
				orderListRedraw(orders);
			}

		}	    
	};
	
	
	@Override
	public void OnCompleteGetOrdersListener(String result) {
		post_get_orders_list( result );
	}



	@Override
	public void OnErrorGetOrdersListener(String error_msg) {
		Log.e(TAG, error_msg);
	}



	@Override
	public void OnSetOrderStatusComplete(int order_status, Driver driver, long order_id, String comment) {
    	// ����� ���������� ������� ������ ������������� ���� �������
    	get_orders_list();
    	progress.setVisibility(View.INVISIBLE);
	}



	@Override
	public void OnSetOrderStatusError(long order_id, String error_msg) {
		Log.e(TAG, error_msg);		
	}

	// =============================================
	//
	// =============================================

	@Override
	public void OnDeclineOrder(Order order) {
		String comment = "";
		set_order_status( AppSettings.ORDER_STATUS_DRIVER_DECLINE_ORDER, order.getId(), comment );
	}


	@Override
	public void OnCallToPhone(Order order) {
		Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+order.getPhonenumber()));
		startActivity(intent);
	}	    	
	
	/**
	 * ������� ������ ��� �������� �������������
	 */
	@Override
	public void OnDelayOrder(Order _order) {
		
		final Order order = _order;
		
		AlertDialog.Builder alert = new AlertDialog.Builder(this);
		alert.setTitle( getString(R.string.screen_driver_order_list_dialog_say_delay_title) );
		alert.setMessage( getString(R.string.screen_driver_order_list_dialog_say_delay_description) );
		final EditText input = new EditText(this);
		alert.setView(input);

		alert.setPositiveButton(getString(R.string.button_ok), new DialogInterface.OnClickListener() {
		    public void onClick(DialogInterface dialog, int whichButton) {
				
				StringBuilder sb = new StringBuilder().append(getString(R.string.text_driver_say_delay)).append(" ").append(getString(R.string.string_minute_short));
				String value = input.getText().toString();
				if( !value.equals("") ){ 
					sb.append(" - ").append(value);
				}
				set_order_status( AppSettings.ORDER_STATUS_DRIVER_SAY_DELAY, order.getId(), sb.toString() );
			}
		});

		alert.setNegativeButton(getString(R.string.button_cancel), new DialogInterface.OnClickListener() {
		    public void onClick(DialogInterface dialog, int whichButton) {
			    dialog.cancel();
		    }
		});

		alert.show();		
		
	}



	@Override
	public void OnArriveOrder(Order order) {
		set_order_status( AppSettings.ORDER_STATUS_DRIVER_SAY_ARRIVE, order.getId(), "" );
	}



	@Override
	public void OnCompleteOrder(Order order) {
		/* set_order_status( AppSettings.ORDER_STATUS_CLOSE, orderId, "" ); */
	}



	@Override
	public void OnShowOrderOnMap(Order order) {
		Intent intent = new Intent();
		intent.setClass(this, ScreenDriverOrderInfo.class);
		intent.putExtra("order", order);
		startActivity(intent);
	}

}
