package com.martianlab.taxitorg.driver;

import com.martianlab.taxitorg.R;
import com.martianlab.taxitorg.AppSettings;
import com.martianlab.taxitorg.service.TaxitorgServiceDriver;
import com.martianlab.taxitorg.service.TaxitorgServiceDriver.ServiceDriverBinder;

import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;

/**
 * ��������� ����� ��� ��������
 * @author awacs
 *
 */
public class ScreenDriverMain extends Activity {
	
	private final static String TAG = "ScreenDriverMain";
	
	private long driverId; 
	
	/**
	 * ��� ������� ��������
	 */
	private boolean isBoundDriverServer = false;
	private TaxitorgServiceDriver serviceDriver;
	private ServiceConnection serviceDriverConnection;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_driver_main);
        Bundle extras = getIntent().getExtras(); 
        
        // ���������
        TextView title = (TextView) this.findViewById(R.id.header_title);
        title.setText( getString(R.string.title_screen_driver_main));
        
        Button backButton = (Button)this.findViewById(R.id.header_backbutton);
        backButton.setOnClickListener(new OnClickListener(){
 			@Override
 			public void onClick(View v) {
 				finish();
 			}
        });         
        
        driverId = 0;
        if( extras != null ){
	        if( extras.containsKey( AppSettings.KEY_DRIVER_ID ) ){
				driverId = extras.getLong( AppSettings.KEY_DRIVER_ID );
			}
        }
        
		serviceDriverConnection = new ServiceConnection() {
		    public void onServiceConnected(ComponentName name, IBinder service) {
		        ServiceDriverBinder serviceDriverBinder = (ServiceDriverBinder) service;
		        serviceDriver = serviceDriverBinder.getService();
		        serviceDriver.setNotification(false); 
		        isBoundDriverServer = true; 
		    }
		    public void onServiceDisconnected(ComponentName name) {
		        isBoundDriverServer = false;
		    }
	    };          
	    
    }
    
	@Override 
	public void onResume() {
		super.onResume();
		if( !isBoundDriverServer ){
			Log.d(TAG, "bindService");
			bindService(new Intent(this, TaxitorgServiceDriver.class), serviceDriverConnection, Context.BIND_AUTO_CREATE);
		}
		registerReceiver(broadcastReceiver, new IntentFilter(AppSettings.BROADCAST_ACTION_DRIVER));
	    if( serviceDriver != null ){
			serviceDriver.setNotification(false);
	    }
	}     
    
    @Override
    protected void onPause(){
    	if( isBoundDriverServer ){
    		isBoundDriverServer = false;
    		unbindService(serviceDriverConnection); 
    	}
   		unregisterReceiver(broadcastReceiver);
   		if( serviceDriver != null ){
   			serviceDriver.setNotification(true);
   		}
    	super.onPause();
    }	
	
    /**
     * �������� ��������� �� �������
     */
	private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String task = intent.getStringExtra("task");
			String results = intent.getStringExtra("results");
			// TODO: ���������� ������ � ������� �������
			if( task.equals("changed_orders") ){
				Log.d(TAG, results);
			//	showDrivers(results);
			}
		}
	};	
	
	
    
    /**
     * ����� �������� �������������� �������������� ���������� ��������
     * @param v
     */
    public void button_screen_driver_main_change_info_click(View v){
    	Intent intent = new Intent();
    	intent.setClass(this, ScreenDriverUpdateInfo.class);
    	startActivity(intent);
    }

    /**
     * ����� �������� ��������� ����������� �� �����
     * @param v
     */
    public void button_screen_driver_main_view_map_click(View v){
    	Intent intent = new Intent();
    	intent.setClass(this, ScreenDriverOrdersMap.class);
    	intent.putExtra("driver_id", AppSettings.currentDriver.getId());
    	startActivity(intent);
    }

    /**
     * ����� �������� ��������� ����� ������� �������
     * @param v
     */
    public void button_screen_driver_main_orders_click(View v){
    	Intent intent = new Intent();
    	intent.setClass(this, ScreenDriverCurrentOrdersList.class);
    	intent.putExtra("driver_id", driverId);
    	startActivity(intent);    
    }

}
