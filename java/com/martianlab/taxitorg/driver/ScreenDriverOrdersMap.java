package com.martianlab.taxitorg.driver;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import com.martianlab.taxitorg.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.martianlab.taxitorg.AppSettings;
import com.martianlab.taxitorg.common.GeoUtility;
import com.martianlab.taxitorg.common.Order;
import com.martianlab.taxitorg.handlers.AcceptOrderHandler;
import com.martianlab.taxitorg.handlers.AcceptOrderListener;
import com.martianlab.taxitorg.handlers.GetOrderInfoHandler;
import com.martianlab.taxitorg.handlers.GetOrderInfoListener;
import com.martianlab.taxitorg.handlers.GetOrdersInRadiusHandler;
import com.martianlab.taxitorg.handlers.OrdersInRadiusListener;
import com.martianlab.taxitorg.handlers.RouteHandler;
import com.martianlab.taxitorg.handlers.RouteListener;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.text.InputType;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * ����� ��� ����������� �������
 * @author awacs
 *
 */
public class ScreenDriverOrdersMap 
           extends FragmentActivity 
           implements   LocationListener, LocationSource
                      , OrdersInRadiusListener, OnMarkerClickListener
                      , RouteListener, AcceptOrderListener
                      , GetOrderInfoListener
{
	private final static String TAG = "ScreenDriverOrdersMapNew";
	
	private GoogleMap mMap;
	
	private LocationManager lManager;
	private OnLocationChangedListener mListener;
	
	private ArrayList<OrderMarker> orderMarkers;
	
	private static double mLatitude;
	private static double mLongitude;
	
	private boolean firstLoadOrders = true;
	
	private GetOrderInfoHandler orderInfoHandler;
	private RouteHandler routeHandler;
	private AcceptOrderHandler acceptOrderHandler;
	
	private LinearLayout orderForm;
	private TextView orderFormDistance;
	private TextView orderFormVoyageTime;
	private TextView orderFormPrice;
	private TextView orderFormCurrency;
	private Button   orderFormAcceptButton;
	private Button   orderFormAuctionButton;
	
	private CheckBox orderFormAddArriveTimeCheckBox;
	private EditText orderFormArriveTimeTextEdit;
	
	private long currentOrderId; 
	private String currentOrderCurrency;
	
	private long currentDriverId;
	
	/** 
	 * ����������� ���� � ����������� � ������
	 */
	private PopupWindow popUpOrderInfo;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_driver_map);
        
        // ���������
        TextView title = (TextView) this.findViewById(R.id.header_title);
        title.setText( getString(R.string.title_screen_driver_map));
        
        Button backButton = (Button)this.findViewById(R.id.header_backbutton);
        backButton.setOnClickListener(new OnClickListener(){
 			@Override
 			public void onClick(View v) {
 				finish(); 
 			}
        });        
        
        currentOrderId = 0;
        currentOrderCurrency = "";
        currentDriverId = 0;
        Bundle extras = getIntent().getExtras();
        if( extras != null ){
        	if( extras.containsKey("driver_id") ){
        		currentDriverId = extras.getLong("driver_id");
        	}
        }
        
        orderForm = (LinearLayout) findViewById(R.id.screen_driver_map_order_form);
        orderFormDistance = (TextView) findViewById(R.id.screen_driver_map_view_distance_value);
        orderFormVoyageTime = (TextView) findViewById(R.id.screen_driver_map_view_voyageTime_value);
        orderFormPrice = (TextView) findViewById(R.id.screen_driver_map_view_price_value);
        orderFormCurrency = (TextView) findViewById(R.id.screen_driver_map_view_currency_value);
        orderFormAcceptButton = (Button) findViewById(R.id.screen_driver_map_view_accept_order_button);
        orderFormAuctionButton = (Button) findViewById(R.id.screen_driver_map_view_auction_order_button);
        // � ����������� �� �������������� �������� ������ ������ ������
        orderFormAcceptButton.setEnabled( currentDriverId > 0 );
        orderFormAuctionButton.setEnabled( currentDriverId > 0 );
        
        orderFormAddArriveTimeCheckBox = (CheckBox) findViewById(R.id.screen_driver_map_arrive_time_checkbox);
        orderFormArriveTimeTextEdit = (EditText) findViewById(R.id.screen_driver_map_arrive_time_arrive_time);
        
        orderFormAddArriveTimeCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked){
               	orderFormArriveTimeTextEdit.setEnabled(isChecked);
               	if( isChecked ){
               		orderFormArriveTimeTextEdit.setVisibility(View.VISIBLE);
               	} else {
               		orderFormArriveTimeTextEdit.setVisibility(View.GONE);
               	}
            }
        });
        
        setUpMapIfNeeded(); 
    }

    private void setUpMapIfNeeded() {
    	
        lManager = (LocationManager) getSystemService(LOCATION_SERVICE);
    	
        mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapView)).getMap();
        if( mMap != null ){ 
	        mMap.setMyLocationEnabled(true);
	        // ����� �� ����� ��������� ������
	        mMap.setOnMarkerClickListener(this);    	
        }
        // ��� ��������� �������� ���� ��������� ��������� �������
        if( lManager != null ){
            Location l = null;
            List<String> providers = lManager.getProviders(true);
            for (int i=providers.size()-1; i>=0; i--) {
                l = lManager.getLastKnownLocation(providers.get(i));
                if (l != null) break; 
            }
            if (l != null) {
            	Log.d(TAG, "����� ������ ������ �� ��������� ��������� �����");
                mLatitude = l.getLatitude();
                mLongitude = l.getLongitude();
            	mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLatitude, mLongitude), GeoUtility.zoomLevel(AppSettings.DEFAULT_RADIUS_FOR_DRIVER_MAP*2)));
                new GetOrdersInRadiusHandler(this).getOrders(mLatitude, mLongitude, AppSettings.DEFAULT_RADIUS_FOR_DRIVER_MAP*2);
            }            
        }
    }	

    @Override	
	protected void onPause() {
		 if( lManager != null ){
			 lManager.removeUpdates(this);
		 }

    	 if( popUpOrderInfo != null && popUpOrderInfo.isShowing() ){
    	     popUpOrderInfo.dismiss(); 
		 }    

    	 super.onPause();
	 }

    @Override
	protected void onResume() {
		super.onResume(); 
		setUpMapIfNeeded();
		firstLoadOrders = true;
		if( lManager == null ){
			lManager = (LocationManager) getSystemService(LOCATION_SERVICE);
		}
		checkLocationAccess();
	}
    
    private void checkLocationAccess(){
		if( lManager != null ){ 
	        // ���� �� �������� ����������� ��������������
            if( !lManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && !lManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER) ){
    			buildAlertMessageNoGps(); 
    		} else {
    			Log.d(TAG, "RequestLocationUpdates");
    			lManager.requestLocationUpdates( lManager.getBestProvider(new Criteria(), true), 0, 0, this);
    		}
		}    	
    }
    
	
    @Override
	public void onBackPressed(){
    	if( popUpOrderInfo != null && popUpOrderInfo.isShowing() ){
    		// ��������� ���� � ��������� �����������
    		popUpOrderInfo.dismiss();
    	} else if( orderForm.getVisibility() == View.VISIBLE ) {
    		// ��������� ���� � ����� ����������� � ���������� ������� �� ������� �����
    		orderForm.setVisibility(View.GONE);
    		mMap.clear();
    		drawMarkers();
    	} else {
    		super.onBackPressed();
    	}
    }
    
	/**
	 * �� �������� ����������� ��������������
	 */
	private void buildAlertMessageNoGps() {
		    final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		    builder.setMessage(getString(R.string.dialog_gps_disabled))
		           .setCancelable(false)
		           .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
		               public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
		                   startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
		               }
		           })
		           .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
		               public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
		                    dialog.cancel();
		               }
		           });
		    final AlertDialog alert = builder.create();
		    alert.show();
	}		
	

	@Override
	public void onLocationChanged(Location location) {

		mLatitude = location.getLatitude();
		mLongitude = location.getLongitude();
		double speed = location.getSpeed();
		String provider = location.getProvider();
		int    time = new Time(location.getTime()).getSeconds();
		Log.d(TAG, "onLocationChanged - ������: " + mLatitude + " | �������: " + mLongitude + " | speed: " + speed
				+ " | provider: " + provider + " | time: " + time + "s");
		if( firstLoadOrders ){
			// ������������� ����������� �������� ������� 
			new GetOrdersInRadiusHandler(this).getOrders(mLatitude, mLongitude, AppSettings.DEFAULT_RADIUS_FOR_DRIVER_MAP*2);
			firstLoadOrders = false; 
		}
		
		if( mListener != null ){
			mListener.onLocationChanged( location );
		}
		
	}
	
	private void addOrderToMap(long order_id, double price, String currency, long voyageTime, double distance, LatLng startPoint, LatLng endPoint, String title, String snippet) {
		
		float marker_color = BitmapDescriptorFactory.HUE_CYAN;
		if(voyageTime > System.currentTimeMillis()/1000){
			marker_color = BitmapDescriptorFactory.HUE_ROSE;
		}
		
		Marker marker = mMap.addMarker(new MarkerOptions()
           .position(startPoint)
           .icon(BitmapDescriptorFactory.defaultMarker(marker_color)));
		orderMarkers.add(new OrderMarker( marker.getId(), order_id, price, currency, voyageTime, startPoint, endPoint, marker_color ));
	}
	
	@Override
	public void onProviderDisabled(String provider) {
		Log.d(TAG, "Provider disabled. GPS is off");
	}

	@Override
	public void onProviderEnabled(String provider) {
		Log.d(TAG, "Provider enabled. GPS is on");
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		//���������� ��� ��������� ������� ����������
		Log.d(TAG, "Provider " + provider + " status " + status + " changed");
		
	}

	@Override
	public void activate(OnLocationChangedListener listener) {
		mListener = listener;
	}

	@Override
	public void deactivate() {
		mListener = null;
	}

	@Override
	public void OnGetOrdersInRadius() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void OnReceiveOrdersInRadius(JSONArray ordersArray) {
		mMap.clear();
		mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLatitude, mLongitude), GeoUtility.zoomLevel(AppSettings.DEFAULT_RADIUS_FOR_DRIVER_MAP*2)));
		orderMarkers = new ArrayList<OrderMarker>();
		for (int i = 0; i < ordersArray.length(); i++) {
			JSONArray order;
			try {
				order = ordersArray.getJSONArray(i);
				long   order_id       = order.getLong(0);
				double startLatitude  = order.getDouble(1);
				double startLongitude = order.getDouble(2);
				double endLatitude    = order.getDouble(3);
				double endLongitude   = order.getDouble(4);
				long   voyagetime     = order.getLong(5);
				double price          = order.getDouble(6);
				String currency       = order.getString(7);
				double distance       = order.getDouble(8);
				 
				LatLng startPoint = new LatLng( startLatitude, startLongitude);
				LatLng endPoint = new LatLng( endLatitude, endLongitude);
				addOrderToMap(order_id, price, currency, voyagetime, distance, startPoint, endPoint, "Unknown", "Unknown");
			} catch (JSONException e) {
				Log.e(TAG, e.getMessage());
				e.printStackTrace();
			}
		}	
	}

	@Override
	public void OnErrorGetOrdersInRadius(String error_msg) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onRouteCalcBegin() {
		// TODO Auto-generated method stub
		
	} 
	
	/**
	 * ������������ �������
	 */
	private void drawMarkers(){
		for( OrderMarker order : orderMarkers ){
			float markerId = order.color;
			// ������ ���� ������� ���� �� ����������
			if( order.isViewed ){
				markerId = BitmapDescriptorFactory.HUE_ORANGE;
			}

			Marker marker = mMap.addMarker(new MarkerOptions()
	        .position(order.startPoint)
	        .icon(BitmapDescriptorFactory.defaultMarker(markerId)));
			// Id ��� ��������� �� �����
			order.marker_id = marker.getId();
		}		
	}
	
	@Override
	public void onRouteCompleted(ArrayList<LatLng> route, long distance, long duration) {
		mMap.clear();
		/* drawMarkers(); */ 
		// ������������ ���������
		mMap.addPolyline((new PolylineOptions().color(Color.BLUE).width(10)).addAll(route));
		orderFormDistance.setText( String.valueOf( Math.round( distance/1000) ) ); // km
		/*private EditText orderFormDriverComment;*/				
	}

	/**
	 * ��� �� ������� � �������
	 * @param marker
	 * @return
	 */
	@Override
	public boolean onMarkerClick(Marker marker) {
		final OrderMarker o = getOrderMarker(marker.getId());
		o.isViewed = true;
		Log.d(TAG, "onMarkerClick, id="+marker.getId());
		Log.d(TAG, "size array of markers = " + orderMarkers.size() );
		Log.d(TAG, "searched marker = " + o );
		if( o != null ){
		    routeHandler = new RouteHandler(this);
			routeHandler.calculateRoute(
		    		  o.startPoint.latitude
		    		, o.startPoint.longitude
		    		, o.endPoint.latitude
		    		, o.endPoint.longitude
		    		, AppSettings.FINE_ROUTE
		    );			
		}
		
		showDialogAtMarker(o);
		
		return false;
	}
	
	private void showDialogAtMarker( OrderMarker orderMarker ){
		orderForm.setVisibility(View.VISIBLE);
		
		currentOrderId = orderMarker.order_id;
		currentOrderCurrency = orderMarker.currency;
		
		Date voyageTimeDate = new Date(orderMarker.voyageTime*1000);
		String voyageTimeFormatString = new SimpleDateFormat("dd-MM-yyyy hh:mm", Locale.getDefault()).format(voyageTimeDate);
		
		orderFormVoyageTime.setText( voyageTimeFormatString );
		orderFormPrice.setText( String.valueOf(orderMarker.price) );
		orderFormCurrency.setText( String.valueOf(orderMarker.currency) );
	}
	
	
	/**
	 * ������ ������� �����
	 * @param v
	 */
	public void button_screen_driver_map_view_accept_order_click(View v){
		if( currentDriverId > 0 ){
			StringBuilder sb = new StringBuilder();
			sb.append("����.");
			
			if( orderFormAddArriveTimeCheckBox.isChecked() && !orderFormArriveTimeTextEdit.getText().toString().equals("") ){
				sb.append(" ").append(" ������� �� ").append( orderFormArriveTimeTextEdit.getText().toString() ).append(" �����.");
			}
			
			acceptOrderHandler = new AcceptOrderHandler( ScreenDriverOrdersMap.this );
			acceptOrderHandler.acceptOrder(currentOrderId, currentDriverId, mLatitude, mLongitude, sb.toString());
			orderForm.setVisibility(View.INVISIBLE);
		}
	}
	
	/**
	 * ������ ������� ����� � �����������
	 * @param v
	 */
	public void button_screen_driver_map_view_auction_order_click(View v){
		if( currentDriverId > 0 ){
			AlertDialog.Builder alert = new AlertDialog.Builder(this);
			alert.setTitle( getString(R.string.screen_driver_map_view_auction_title) );
			alert.setMessage( getString(R.string.screen_driver_map_view_auction_description) + " (" + currentOrderCurrency + ")" );
			final EditText input = new EditText(this);
			input.setInputType(InputType.TYPE_CLASS_NUMBER);
			alert.setView(input);

			alert.setPositiveButton(getString(R.string.button_ok), new DialogInterface.OnClickListener() {
			    public void onClick(DialogInterface dialog, int whichButton) {
					
					StringBuilder sb = new StringBuilder();
					if( orderFormAddArriveTimeCheckBox.isChecked() && !orderFormArriveTimeTextEdit.getText().toString().equals("") ){
						sb.append(" ").append(" ������� �� ").append( orderFormArriveTimeTextEdit.getText().toString() ).append(" �����. ");
					}
					sb.append("��� ����� ").append(input.getText().toString()).append(" ").append(currentOrderCurrency).append(".");

					acceptOrderHandler = new AcceptOrderHandler( ScreenDriverOrdersMap.this );
					acceptOrderHandler.acceptOrder(currentOrderId, currentDriverId, mLatitude, mLongitude, sb.toString());
					orderForm.setVisibility(View.INVISIBLE);
				}
			});

			alert.setNegativeButton(getString(R.string.button_cancel), new DialogInterface.OnClickListener() {
			    public void onClick(DialogInterface dialog, int whichButton) {
				    dialog.cancel();
			    }
			});

			alert.show();		

		}
	}
	
	/**
	 * ������ ���������� � ������
	 * @param v
	 */
	public void button_screen_driver_map_view_info_order_click(View v){
		orderInfoHandler = new GetOrderInfoHandler( this );
		orderInfoHandler.getOrderInfo(currentOrderId);
	}
	
	/**
	 * ������ ���������� � ������
	 * @param v
	 */
	public void button_screen_driver_map_view_cancel_click(View v){
		orderForm.setVisibility(View.GONE);
		mMap.clear();
		drawMarkers();
	}	
	
	private OrderMarker getOrderMarker( String search_id ){
		for( OrderMarker o : orderMarkers ){
			Log.d(TAG, "marker_id" + o.marker_id + "search_id" + search_id);
			if( o.marker_id.equalsIgnoreCase(search_id) ){
				return o;
			}
		}
		return null;
	}
	
	private class OrderMarker{
		public String marker_id;
		public long order_id;
		public double price;
		public String currency;
		public long voyageTime;
		public LatLng startPoint; 
		public LatLng endPoint;
		public boolean isViewed;
		public float color;
		public OrderMarker( String id, long order_id, double _price, String _currency, long vt, LatLng start, LatLng stop, float _color ){
			this.marker_id = id;
			this.order_id = order_id;
			this.startPoint = start;
			this.endPoint = stop;
			this.voyageTime = vt;
			this.price = _price;
			this.currency = _currency;
			this.isViewed = false;
			this.color = _color;
		}
	}
	
	
	/**
	 * �� RouteHandler �������� �������� �������������� �����
	 */
	@Override
	public void onBoundsCalculated(LatLng southwest, LatLng northeast) {
		LatLngBounds bounds = new LatLngBounds.Builder()
	        .include(southwest)
	        .include(northeast)
	        .build();
		mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 50));
	}

	@Override
	public void onAcceptCompleted() {
		
		mMap.clear();
		drawMarkers();

		// ���������� ���������� ���� "������ �������, �������� ������������� ���������"
		AlertDialog.Builder dialog = new AlertDialog.Builder(this);
		dialog.setTitle( getString(R.string.screen_driver_map_dialog_accept_offer_title) );
		dialog.setMessage( getString(R.string.screen_driver_map_dialog_accept_offer_description) );
		dialog.setPositiveButton(getString(R.string.screen_driver_map_dialog_accept_offer_button_positive), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
		    	if( currentDriverId > 0 ){
					Intent intent = new Intent();
			    	intent.setClass(ScreenDriverOrdersMap.this, ScreenDriverCurrentOrdersList.class);
			    	intent.putExtra("driver_id", currentDriverId);
			    	startActivity(intent);
			    	finish();
		    	}
			}
		});
		dialog.setNegativeButton( getString(R.string.button_cancel) , new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
			}
		});
		dialog.setCancelable(true);
		dialog.show();
	
	}

	@Override
	public void onAcceptError(String error_msg) {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * �������� ���� � ����������� � ������
	 */
	@Override
	public void OnBeginGetOrderInfo() {
	}

	
	
	/**
	 * �������� ���������� � ������
	 */
	@Override
	public void OnEndGetOrderInfo(Order order) {
    	
		DisplayMetrics metrics = this.getApplicationContext().getResources().getDisplayMetrics();
    	int width = metrics.widthPixels;
    	int height = metrics.heightPixels;    	
    	
    	popUpOrderInfo = new PopupWindow(this); 
    	 
    	LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View popLayout = inflater.inflate(R.layout.popup_driver_order_info, null);
    	
        popUpOrderInfo.setContentView(popLayout);
        popLayout.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        popUpOrderInfo.setWidth( width );
        
        int defaultHeight = Math.round( height * 0.8f ); 
        
        if( defaultHeight > popLayout.getMeasuredHeight() ){
        	popUpOrderInfo.setHeight( defaultHeight );
        } else {
        	popUpOrderInfo.setHeight( popLayout.getMeasuredHeight() );        	
        }
		
        popUpOrderInfo.showAtLocation(popLayout, Gravity.CENTER, 0, 0);
        
        // ����� ��������
		TextView startAddress = (TextView) popLayout.findViewById(R.id.label_popup_driver_order_info_address_start);
		startAddress.setText(order.getAddressStart()); 
		 
		TextView endAddress = (TextView) popLayout.findViewById(R.id.label_popup_driver_order_info_address_end);
		endAddress.setText(order.getAddressEnd());
		
		// ������� � ���������
		LinearLayout meetingAeroportLayout = (LinearLayout) popLayout.findViewById(R.id.popup_driver_order_info_meeting_aeroport_layout);
		if( order.isAeroportMeeting() ){
			meetingAeroportLayout.setVisibility(View.VISIBLE); 
		} else {
			meetingAeroportLayout.setVisibility(View.GONE);
		}
		
		TextView flightNumberTextView =  (TextView) popLayout.findViewById(R.id.popup_driver_order_info_meeting_aeroport_flightNumber);
		flightNumberTextView.setText(order.getFlightNumber());
		CheckBox meetWithNameplate =  (CheckBox) popLayout.findViewById(R.id.popup_driver_order_info_meeting_aeroport_meetWithNameplate);
		meetWithNameplate.setVisibility(View.GONE);
		meetWithNameplate.setChecked( order.getMeetWithNameplate() );
		if( order.getMeetWithNameplate() ){
			meetWithNameplate.setVisibility(View.VISIBLE);
		}
			
		TextView aeroportAnnotationTextView =  (TextView) popLayout.findViewById(R.id.popup_driver_order_info_meeting_aeroport_annotation);
		aeroportAnnotationTextView.setText(order.getNamePlateText());
		
		// ������������
		TextView vehicleClassTextView = (TextView) popLayout.findViewById(R.id.popup_driver_order_info_vehicle_class);
		vehicleClassTextView.setText(order.getVehicleClass());
		
		CheckBox isPassengerSmoke =  (CheckBox) popLayout.findViewById(R.id.popup_driver_order_info_isPassengerSmoke);
		CheckBox isDriverSmoke =  (CheckBox) popLayout.findViewById(R.id.popup_driver_order_info_isDriverSmoke);
		CheckBox isConditioner = (CheckBox) popLayout.findViewById(R.id.popup_driver_order_info_isConditioner);
		CheckBox isChildSeat =  (CheckBox) popLayout.findViewById(R.id.popup_driver_order_info_isChildSeat);
		CheckBox isBaggage =  (CheckBox) popLayout.findViewById(R.id.popup_driver_order_info_isBaggage);
		CheckBox isSoberDriver =  (CheckBox) popLayout.findViewById(R.id.popup_driver_order_info_isSoberDriver);
		CheckBox isMyMusic =  (CheckBox) popLayout.findViewById(R.id.popup_driver_order_info_isMyMusic);
		
		isPassengerSmoke.setVisibility(View.GONE);
		isDriverSmoke.setVisibility(View.GONE);
		isConditioner.setVisibility(View.GONE);
		isChildSeat.setVisibility(View.GONE);
		isBaggage.setVisibility(View.GONE);
		isSoberDriver.setVisibility(View.GONE);
		isMyMusic.setVisibility(View.GONE);
		if( order.getIsPassengerSmoke() ){
			isPassengerSmoke.setVisibility(View.VISIBLE);
		}
		if( !order.getIsDriverSmoke() ){
			isDriverSmoke.setVisibility(View.VISIBLE);
		}
		if( order.getIsConditioner() ){
			isConditioner.setVisibility(View.VISIBLE);
		}
		if( order.getIsChildSeat() ){
			isChildSeat.setVisibility(View.VISIBLE);
		}
		if( order.getIsBaggage() ){
			isBaggage.setVisibility(View.VISIBLE);
		}
		if( order.getIsSoberDriver() ){
			isSoberDriver.setVisibility(View.VISIBLE);
		}
		if( order.getIsMyMusic() ){
			isMyMusic.setVisibility(View.VISIBLE);
		}
		
		TextView annotation = (TextView) popLayout.findViewById(R.id.label_popup_driver_order_info_annotation);
		annotation.setText(order.getAnnotation());
		
	}
	
	public void button_popup_driver_order_info_cancel_click(View v){
		popUpOrderInfo.dismiss();
	}
	
	@Override
	public void OnErrorGetOrderInfo(String errorMsg) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onRouteCalcError(String error_msg) {
		// TODO Auto-generated method stub
		
	}
	
}
