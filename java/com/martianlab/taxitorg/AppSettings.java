package com.martianlab.taxitorg;

import com.martianlab.taxitorg.common.Driver;
import com.martianlab.taxitorg.common.Order;

/**
 * TODO:

import com.martianlab.taxitorg.common.Driver;
import com.martianlab.taxitorg.common.Order;

/**
 * ����� ��� �������� ���������� �������� ����������
 * @author awacs
 *
 */
public class AppSettings {
	
	public static final String TAG = "AppSettings"; 
	
	/**
	 * ������ ����������
	 */
	public static String lang = "ru";
	 
	/**
	 * ������� ������
	 */ 
	public final static String API_TAXITORG_DOMAIN = "http://taxitorg.com/server/server.php";
    public final static String URL_IMAGES = "http://taxitorg.com/server/images/";
    public final static String DIR_TAXITORG = "/taxitorg/";
	public final static String API_TAXITORG_URI_UPLOAD_IMAGE = "http://taxitorg.com/server/upload_image.php";

	public final static String IMAGES_EXTENSION = ".jpg";
	public final static int POSTFIX_IMAGE_FACE = 1; 
	public final static int POSTFIX_IMAGE_CAR = 2;
	
	/**
	 * �������� ���� ������
	 * ���� ������������ ��������� � ��������� ���� ������, ���������� ����� ��������� ������!
	 */
	public final static int DATABASE_VERSION = 12;
	public final static String DATABASE_NAME = "taxitorg.db";
	
	/**
	 * ������������ ������ ��� ���� ������������
	 */
	public final static int MAX_IMAGE_SIZE = 240;
	
	public final static float PRICE_PER_KILOMETERS = 0.5f;
	
	/**
	 * ������� ������ ����� �� ��������� ��� �������� � ���������
	 * ��� ������ ����� ������������� ��������������� �������, ������� ���������� ������ �������
	 */
	public final static double DEFAULT_RADIUS_FOR_PASSENGER_MAP = 1;
	public final static double DEFAULT_RADIUS_FOR_DRIVER_MAP = 50;
	
	
	/**
	 * ����� ����� ������ (���) ����� ����������� ���� � ( �� �.�. ������� �������)
	 * �� ��������� 1 ���
	 */
	public final static int DEFAULT_ORDER_LIFETIME = 60 * 60; 

	
	/**
	 * ����� � ��������, ����� �������� ����� ��������� ������������ ��� ��������
	 * ������������ � ������������ ������ �������
	 */
	public static final long TIMEOUT_ORDER_FOR_DRIVER = 60*15;
	
	
	/**
	 * ������� ����� � ������ �������� ���������� �������
	 * ����� � �������� ��� ���������� ������ (5 �����)
	 */
	public static final long TIMEOUT_NEAREST_ORDER_FOR_DRIVER = 5 * 60 * 60;
	
	
	
	/**
	 * ��� ����� ��������
	 */
	public static final String APP_PREFERENCES = "taxitorg_settings";
	public static final String APP_PREFERENCES_REMEMBER = "driver_remember";
	public static final String APP_PREFERENCES_LOGIN = "driver_login";
	public static final String APP_PREFERENCES_PASSWORD = "driver_password";
	public static final String APP_PREFERENCES_LASTLOGIN = "last_login";
	public static final String APP_PREFERENCES_LOCALE = "en";
	
	/**
	 * ������� ��������
	 */
	public static Driver currentDriver;
	
	/**
	 * ������� �����
	 */
	public static final Order currentOrder = new Order();
	
	/**
	 * ���� ��� �������� ������
	 */
	public final static String KEY_DRIVER_ID = "driver_id";
	public final static String KEY_ORDER_ID = "order_id";
	
	/**
	 * ������ �� ����������� ��������
	 */
	public final static String REQUEST_AUTHORIZE_DRIVER = "authorize_driver";

	/**
	 * ������ �� ������ ��������
	 */
	public final static String REQUEST_RATE_DRIVER = "rate_driver";
	
	
	/**
	 * ������ �� �������� ��������
	 */
	public final static String REQUEST_CREATE_DRIVER = "create_driver";
	
	/**
	 * �������� ���������� � ��������
	 */
	public final static String REQUEST_UPDATE_DRIVER = "update_driver";
	
	/**
	 * ������ �� ��������� ������� � �������� �������
	 */
	public final static String REQUEST_GET_ORDERS = "get_orders";

	
	/**
	 * ������ �� ��������� ���������� ������
	 */
	public final static String REQUEST_GET_ORDER_INFO = "get_order_info";

	/**
	 * ������ �� ��������� ���������� ��������
	 */
	public final static String REQUEST_GET_DRIVER_INFO = "get_driver_info";	
	
	/**
	 * ������ �� �������� ������ ���������
	 */
	public final static String REQUEST_ACCEPT_ORDER = "accept_order";
	
	
	public final static String REQUEST_SET_ORDER_STATUS = "set_order_status";
	
	/**
	 * ��������� ������ �������� ��������� ������
	 */
	public final static String REQUEST_GET_ORDERS_BY_DRIVER_ID = "get_orders_by_driver_id";
	
	/**
	 * ������ �� �������� ������
	 */
	public final static String REQUEST_CANCEL_ORDER = "cancel_order";	
	
	/**
	 * ������ �� �������� ������ ����������
	 */
	public final static String REQUEST_CREATE_ORDER = "create_order";
	
	/**
	 * ������ �� ������ ��������� �� ������ ������
	 */
	public final static String REQUEST_GET_DRIVER_LIST = "get_driver_list"; 
	
	
	
	public final static String REQUEST_CHECK_ORDER_STATUS = "check_order_status";
	
	/**
	 * ���� � ���� ������
	 */
	public final static String DB_PATH = "/data/data/com.martianlab.taxitorg/databases/";
	
	/**
	 * ������ ��������
	 */
	public static final String BROADCAST_ACTION_DRIVER = "com.martianlab.taxitorg.service.TaxitorgServiceDriver";
	
	/**
	 * ������ ���������
	 */
	public static final String BROADCAST_ACTION_PASSENGER = "com.martianlab.taxitorg.service.TaxitorgServicePassenger";
	
	/**
	 * ��� ������� ������ ���������� �����, ��������� ��� �������� ����� ������� �����. 
	 * ��� ������ ������������� ����� �������� ���������� ��������� ����-��������:
	 */
	public final static String KEY_DIRECTION = "direction";
	public final static int KEY_DIRECTION_START = 1;
	public final static int KEY_DIRECTION_FINISH = 2;
	
	/**
	 * ��� ������� ��������� ���������� � �/� �������� - ������������� ��� ���������� �� ��������, �.�. ������������ ���� ��������
	 */
	public final static String KEY_TYPE_STATION = "type_station";
	public final static int KEY_TYPE_STATION_AIRPORT = 1;
	public final static int KEY_TYPE_STATION_RAILROAD = 2;
	
	
	/**
	 * ��������� ������
	 */
	public static final int ORDER_STATUS_CLOSE = 0;
	public static final int ORDER_STATUS_OPEN = 1;
	public static final int ORDER_STATUS_PASSENGER_DECLINE_DRIVER = 2;
	public static final int ORDER_STATUS_PASSENGER_ACCEPT_DRIVER = 3;
	public static final int ORDER_STATUS_DRIVER_DECLINE_ORDER = 4;
	public static final int ORDER_STATUS_PASSENGER_DECLINE_ORDER = 5;
	public static final int ORDER_STATUS_DRIVER_SAY_DELAY = 6;	
	public static final int ORDER_STATUS_DRIVER_SAY_ARRIVE = 7;	
	
	
	
	public static boolean rememberDriver = false;
	public static long driverId = 1234567;
	public static String driverLogin = "newbie";
	public static String driverPassword = "123123";

	
	/**
	 * ��� ������� ���������������� ��������
	 */
	public static final int FINE_ROUTE = 1;
	
	/**
	 * ��� ������� ���������������� ��������
	 */
	public static final int COARSE_ROUTE = 2;	
	
	
	public static final int COLOR_ROUTE = 0xFFFF0000;
	
	
	
	
	public static long getDriverId(){
		return driverId;
	}
	
	public static void setDriver( Driver d ){
		currentDriver = d;
	}
	
	
}
