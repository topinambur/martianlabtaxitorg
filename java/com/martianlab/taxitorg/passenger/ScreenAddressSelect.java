package com.martianlab.taxitorg.passenger;

import java.util.ArrayList;
import java.util.List;
 
import com.martianlab.taxitorg.R;
import com.martianlab.taxitorg.AppSettings;
import com.martianlab.taxitorg.common.DBAdapter;
 
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;
 
/**
 * ����� ������ ��� ����� ������ ��� ������ (��� ���������� � ���������)
 * @author �������������
 *
 */
public class ScreenAddressSelect extends ListActivity implements OnItemClickListener{
	
	private final static String TAG = "ScreenAddressSelect";
	
	public String data;
	public List<String> suggest;
	public AutoCompleteTextView addressInputTextView;
	public ArrayAdapter<String> aAdapter;
	
	private int direction; 
	
	private List<Address> autoCompleteSuggestionAddresses;
	
	private DBAdapter db = new DBAdapter(this); 
	private Cursor mCursor;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState); 
		setContentView(R.layout.activity_screen_passenger_address_select); 
		 
		Bundle extras = getIntent().getExtras();
		if( extras != null ){
			direction = extras.getInt( AppSettings.KEY_DIRECTION );
		}
		
		suggest = new ArrayList<String>();
		addressInputTextView = (AutoCompleteTextView) findViewById(R.id.screen_passenger_address_select_input_address);
		addressInputTextView.setOnItemClickListener(this);
		addressInputTextView.addTextChangedListener(new TextWatcher(){
	 
			public void afterTextChanged(Editable editable) {
			} 
	 
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}
	 
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				String newText = s.toString();
				new GeocodeParser().execute(newText);
			}
		});
		
	} // end function onCreate
	
	@Override 
	public void onResume() {
		loadLatestAddresses();
		super.onResume();
	}
	
	@Override 
	public void onPause() {
		mCursor.close();
		super.onPause();
	}

	private void loadLatestAddresses(){
		// ���������� ��������� �������������� ������
		try {
			db.open();
			mCursor = db.getLatestAddresses();

			// Get the rows from the database and create the item list
			String[] from = new String[] {
					DBAdapter.KEY_ADDRESSES_ROWID,
					DBAdapter.KEY_ADDRESSES_ADDRESS,
					DBAdapter.KEY_ADDRESSES_LATITUDE,
					DBAdapter.KEY_ADDRESSES_LONGITUDE
			};
			AddressCursorAdapter addressListAdapter = new AddressCursorAdapter(this, R.layout.row_latest_address, mCursor, from, null);
			setListAdapter(addressListAdapter); 
			db.close();
		} catch (Exception ex) {
			Toast toast = Toast.makeText(getApplicationContext(), ex.toString(), Toast.LENGTH_LONG);
			toast.show(); 
		}		
	}
	
	
	/**
	 * �� ��������� ������ ���������� ������ ������� 
	 * @author awacs
	 *
	 */
	private class GeocodeParser extends AsyncTask<String,String,String>{
		
		@Override
		protected String doInBackground(String... key) {
			
			String newText = key[0];
			newText = newText.trim();
			newText = newText.replace(" ", "+");
			try{
				autoCompleteSuggestionAddresses = new Geocoder(getBaseContext()).getFromLocationName(newText, 10);
				suggest = new ArrayList<String>();
				for (Address a : autoCompleteSuggestionAddresses) {
					suggest.add( getAddressLine(a) );
				}
			}catch(Exception e){
				Log.w("Error", e.getMessage());
			}
	
			runOnUiThread(new Runnable(){
		
				public void run(){
					aAdapter = new ArrayAdapter<String>(getApplicationContext(),R.layout.item_list, suggest);
					addressInputTextView.setAdapter(aAdapter);
					aAdapter.notifyDataSetChanged();
				}
			});
		 
			return null;
		}
		 
	}

	/**
	 * �������� ��������� ������������� ������ �� ������� ���� Address
	 * @param a
	 * @return String
	 * 
	 * TODO: ��� ��� ���������� �������������, � ���� ����� ���-�� ������
	 */
	private String getAddressLine(Address a){
		String addressLine = "";
		int maxAddressLineIndex = a.getMaxAddressLineIndex();
		if( maxAddressLineIndex != -1){
			for( int i=0; i<maxAddressLineIndex; i++ ){
				addressLine += a.getAddressLine(i) + ", ";
			}
		}
		return addressLine;
	}
	
	/**
	 * ���� �� ���������� ������ � ������������������ ��������, � ����������� �� ���������� direction
	 * ������ �� ������/������� ������������ � start ��� end, � ����� � ��������� ������������� ������
	 * @author awacs 
	 */
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		Log.d(TAG, TAG+"::setOnItemSelectedListener");
		if (arg2 < autoCompleteSuggestionAddresses.size()) {
			Address selected = autoCompleteSuggestionAddresses.get(arg2);
			double lat = selected.getLatitude();
			double lon = selected.getLongitude();
			String addr = getAddressLine(selected);
			setGeoPointCurrentOrder(direction, lat, lon, addr);
			saveAddressToDB( addr, lat, lon );
		}
	}
	
	/**
	 * ���� �� ������ ����������� �������
	 */
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);

		mCursor.moveToPosition(position);
		long address_id = mCursor.getLong(mCursor.getColumnIndexOrThrow(DBAdapter.KEY_ADDRESSES_ROWID));
		String address = mCursor.getString(mCursor.getColumnIndexOrThrow(DBAdapter.KEY_ADDRESSES_ADDRESS));
		double latitude = mCursor.getDouble(mCursor.getColumnIndexOrThrow(DBAdapter.KEY_ADDRESSES_LATITUDE));
		double longitude = mCursor.getDouble(mCursor.getColumnIndexOrThrow(DBAdapter.KEY_ADDRESSES_LONGITUDE));

		setGeoPointCurrentOrder( direction, latitude, longitude,  address);
		saveAddressToDB( address, latitude, longitude );
		finish();
	} // onListItemClick	
	
	
	private void setGeoPointCurrentOrder( int dir, double lat, double lon,  String addr){
        if( dir == AppSettings.KEY_DIRECTION_START ){
        	AppSettings.currentOrder.setStartAddress(addr);
        	AppSettings.currentOrder.setStartPoint(lat, lon);
        }
        if( dir == AppSettings.KEY_DIRECTION_FINISH ){
        	AppSettings.currentOrder.setEndAddress(addr);
        	AppSettings.currentOrder.setEndPoint(lat, lon);		        
        }
		
	}
	
	public void button_screen_address_select_back_click(View v){
		finish();
	}
	
	/**
	 * ����� �������� ��� �������� ����� �� �����
	 * @author awacs 
	 * @param v
	 */
	public void button_screen_address_select_nearest_click(View v){
    	Intent intent = new Intent(); 
	    intent.putExtra(AppSettings.KEY_DIRECTION, direction);
    	intent.setClass(this, ScreenPassengerMapActivity.class); 
	    startActivityForResult(intent, 1);      	
	}

	
	/**
	 * ����� �������� ��� ������ ���������
	 * @author awacs 
	 * @param v
	 */
	public void button_screen_address_select_airports_click(View v){
		Log.d(TAG, "button_screen_address_select_airports_click");
		Intent intent = new Intent(); 
	    intent.putExtra(AppSettings.KEY_DIRECTION, direction);
	    intent.putExtra(AppSettings.KEY_TYPE_STATION, AppSettings.KEY_TYPE_STATION_AIRPORT);
    	intent.setClass(this, ScreenAirportSelection.class); 
	    startActivity(intent);
	    finish();
	}
	
	/**
	 * ����� �������� ��� ������ �/� �������
	 * @author awacs 
	 * @param v
	 */
	public void button_screen_address_select_railwayStations_click(View v){
		Log.d(TAG, "button_screen_address_select_railwayStations_click");
    	Intent intent = new Intent(); 
	    intent.putExtra(AppSettings.KEY_DIRECTION, direction);
	    intent.putExtra(AppSettings.KEY_TYPE_STATION, AppSettings.KEY_TYPE_STATION_RAILROAD);
    	intent.setClass(this, ScreenAirportSelection.class); 
	    startActivity(intent);
	    finish();
	}

	
	/**
	 * ��������� ������ �� �������������� ������ �������� (����� ����� �� �����)
	 * @author awacs
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    if (data == null) {
	        return;
	    }
	    if( resultCode == RESULT_OK ){
		    String address = data.getStringExtra("address");
		    double latitude = data.getDoubleExtra("latitude", 0.0d);
		    double longitude = data.getDoubleExtra("longitude", 0.0d);
		    Log.d(TAG, " address " + address + " latitude = " +String.valueOf(latitude) + " longitude = " + String.valueOf(longitude) );
		    setGeoPointCurrentOrder( direction, latitude, longitude, address );
		    saveAddressToDB( address, latitude, longitude );
		    finish();
	    }
	}
	
	private void saveAddressToDB( String address, double latitude, double longitude ){
		try {
			db.open();
			db.insert_address(address, latitude, longitude);
			db.close();
		} catch (Exception ex) {
			Toast toast = Toast.makeText(getApplicationContext(), ex.toString(), Toast.LENGTH_LONG);
			toast.show();
		}		
	}

    private class AddressCursorAdapter extends SimpleCursorAdapter {

		private Cursor c;
		private Context context;

		public AddressCursorAdapter(Context context, int layout, Cursor c,	String[] from, int[] to) {
			super(context, layout, c, from, to);

			this.c = c;
			this.context = context;

		}

		public View getView(int pos, View inView, ViewGroup parent) {

			View v = inView;

			// Associate the xml file for each row with the view
			if (v == null) {
				LayoutInflater inflater = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				v = inflater.inflate(R.layout.row_latest_address, null);
			}

			this.c.moveToPosition(pos);

			// final long rowid = this.c.getLong(this.c.getColumnIndex(VerblistDatabaseAdapter.KEY_VERBS_ROWID));
			String sAddress = this.c.getString(this.c.getColumnIndex(DBAdapter.KEY_ADDRESSES_ADDRESS));
			// String sRussian = this.c.getString(c.getColumnIndexOrThrow(VerblistDatabaseAdapter.KEY_VERBS_RUSSIAN));
			Log.d(TAG, sAddress);
			TextView tvAddress = (TextView) v.findViewById(R.id.row_latest_address_addressline);
			tvAddress.setText(sAddress);

			return (v);

		} // function getView

	} // class PhraseCursorAdapter	
	
	
}