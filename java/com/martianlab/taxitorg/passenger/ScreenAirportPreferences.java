package com.martianlab.taxitorg.passenger;

import com.martianlab.taxitorg.R;
import com.martianlab.taxitorg.AppSettings;
import com.martianlab.taxitorg.views.Switch;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.app.Activity;
import android.content.Intent;

/**
 * ������������ ��� ������ ��������� (��������� � ���������, ����� �����, ���������� � �.�...)
 * 
 * @author awacs
 *
 */
public class ScreenAirportPreferences extends Activity {
	
	private EditText     flightNumberEditText;
	private EditText     nameplateTextEditText;
	private TextView     airportNameTextView;
	private Switch       meetNameplateToggleButton;
	
	private String airportName; 
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_passenger_airport_preferences);

        airportNameTextView = (TextView) this.findViewById(R.id.screen_passenger_airport_preferences_airport_name);
        meetNameplateToggleButton = (Switch) this.findViewById(R.id.screen_passenger_airport_preferences_meet_nameplate);
        flightNumberEditText = (EditText) this.findViewById(R.id.screen_passenger_airport_preferences_flightnumber);
        nameplateTextEditText = (EditText) this.findViewById(R.id.screen_passenger_airport_preferences_nameplate_text);        
        
        Button backButton = (Button) this.findViewById(R.id.header_backbutton);
        backButton.setOnClickListener( new OnClickListener(){
			@Override 
			public void onClick(View v) {
				finish();
			}
        });
        
        TextView title = (TextView) this.findViewById(R.id.header_title);
        title.setText(getString(R.string.title_screen_passenger_airport_preferences));
        
        Button completeButton = (Button) this.findViewById(R.id.header_completebutton);
        completeButton.setVisibility(View.VISIBLE);
        completeButton.setOnClickListener( new OnClickListener(){
			@Override
			public void onClick(View v) {
		    	Intent returnIntent = new Intent();
		    	returnIntent.putExtra("meetNameplate", meetNameplateToggleButton.isChecked());
		    	returnIntent.putExtra("flightNumber", flightNumberEditText.getText().toString());
		    	returnIntent.putExtra("nameplateText", nameplateTextEditText.getText().toString());
		    	setResult(RESULT_OK, returnIntent); 
		    	finish();
			}
        });        
        
 	    Bundle extras = getIntent().getExtras();
 	    if( extras != null ){
 		    if( extras.containsKey( "airport_name" ) ){
 			    airportName = extras.getString( "airport_name" );
 		    }
 	    }
         
        airportNameTextView.setText(airportName);
    }
    
    
    /**
     * ���������� ������ ������������ � ������� �����
     * @param v
     */
    public void button_screen_passenger_airport_preferences_complete_click( View v ){
    }
    
    
    /**
     * ����� ��� ���������� ����������
     * @param v
     */
    public void button_screen_passenger_airport_preferences_back_click( View v ){
    	Intent returnIntent = new Intent();
    	setResult(RESULT_CANCELED, returnIntent);        
    	finish();
    }
    
}
