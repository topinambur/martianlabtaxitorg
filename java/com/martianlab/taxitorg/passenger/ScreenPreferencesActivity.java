package com.martianlab.taxitorg.passenger;

import com.martianlab.taxitorg.AppSettings;
import com.martianlab.taxitorg.R;
import com.martianlab.taxitorg.views.Switch;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.app.Activity;

/**
 * ���� ������ ������������/����� ������
 * @author awacs
 *
 */
public class ScreenPreferencesActivity extends Activity {
	
	private String[] vehicleClasses = {"������", "�������", "������"};
	
	private String vehicleClass = "";
	
	private Switch isChildSeat;
	private Switch isPassengerSmoke;
	private Switch isDriverSmoke;
	private Switch isSoberDriver;
	private Switch isConditioner;
	private Switch isBaggage;
	private Switch isMyMusic;
	private TextView annotation;
	
	
	
	
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_passenger_preferences);
        
        vehicleClasses[0] = getString(R.string.screen_passenger_preferences_vehicle_class_econom);
        vehicleClasses[1] = getString(R.string.screen_passenger_preferences_vehicle_class_comfort);
        vehicleClasses[2] = getString(R.string.screen_passenger_preferences_vehicle_class_business);
        
        // ���������
        TextView title = (TextView) this.findViewById(R.id.header_title);
        title.setText( getString(R.string.title_screen_passenger_preferences));
        
        // ������ "�����"
        Button backButton = (Button)this.findViewById(R.id.header_backbutton);
        backButton.setOnClickListener(new OnClickListener(){
 			@Override
 			public void onClick(View v) {
 				finish(); 
 			}
        });
        
        // ������ "������"
        Button completeButton = (Button)this.findViewById(R.id.header_completebutton);
        completeButton.setVisibility(View.VISIBLE);
        completeButton.setOnClickListener(new OnClickListener(){
 			@Override
 			public void onClick(View v) {
 				AppSettings.currentOrder.setTripPreferences(
 					  vehicleClass
 					, !isPassengerSmoke.isChecked()
 					, isDriverSmoke.isChecked()
 					, !isConditioner.isChecked()
 					, !isBaggage.isChecked()
 					, !isChildSeat.isChecked()
 					, !isSoberDriver.isChecked()
 					, !isMyMusic.isChecked()
 					, annotation.getText().toString()
 				);
 				
 				finish(); 
 			}
        });        
     
        // ������� ������ ����������
        ArrayAdapter<String> adapterVehicleClasses = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, vehicleClasses);
        adapterVehicleClasses.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);        
        
        // ����� ������ ����������
        Spinner spinner = (Spinner) findViewById(R.id.spinner_screen_passenger_preferences_classSelector);
    	spinner.setAdapter(adapterVehicleClasses);
    	spinner.setPrompt("Class");
    	spinner.setSelection(0);
    	spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
        	@Override
        	public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        		setVehicleClass(vehicleClasses[position]);
        	}
        	@Override
        	public void onNothingSelected(AdapterView<?> arg0) {
        	}
    	});         
        
        // �������������
        //findViewById(R.id.button_screen_passenger_preferences_vehicleClass);
        isPassengerSmoke = (Switch) findViewById(R.id.button_screen_passenger_preferences_isPassengerSmoke);
        isDriverSmoke = (Switch) findViewById(R.id.button_screen_passenger_preferences_isDriverSmoke);
        isConditioner = (Switch) findViewById(R.id.button_screen_passenger_preferences_isConditioner);
        isChildSeat = (Switch) findViewById(R.id.button_screen_passenger_preferences_isChildSeat);
        isBaggage = (Switch) findViewById(R.id.button_screen_passenger_preferences_isBaggage);
        isSoberDriver = (Switch) findViewById(R.id.button_screen_passenger_preferences_isSoberDriver);
        isMyMusic = (Switch) findViewById(R.id.button_screen_passenger_preferences_isMyMusic);        
        // ���������
        annotation = (TextView) findViewById(R.id.screen_passenger_preferences_annotation);
    }
    
    private void setVehicleClass( String value ){
    	this.vehicleClass = value;
    }
}
