package com.martianlab.taxitorg.passenger;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import com.martianlab.taxitorg.AppSettings;
import com.martianlab.taxitorg.R;
import com.martianlab.taxitorg.views.Switch;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.TimePicker.OnTimeChangedListener;

/**
 * �������� ������ ������� �������� 
 * @author awacs
 *
 */
public class ScreenTimeSelectActivity extends FragmentActivity {
	
	private static final String TAG = "ScreenTimeSelectActivity";
	
	private static final int MODE_CURRENT_TIME = 0;
	private static final int MODE_CUSTOM_TIME = 1;
	
	private long voyageTime = 0;
	
	private int mode;
	
	private TextView title;
	private Button backButton;
	private Button completeButton;
	
	private DatePicker mDatePicker;
	private TimePicker mTimePicker;
	
	private LinearLayout dateTimePickerLayout;
	
	private TextView txtVoyageTime;
	private TextView txtVoyageTimeTitle;

	private DateFormat mDateFormat;

	private Switch isCurrentTime;
	
	private boolean isCustomTimeChanged;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_passenger_time_select);
        
        
        txtVoyageTimeTitle = (TextView) findViewById(R.id.screen_passenger_time_select_current_time_title);
        txtVoyageTime = (TextView) findViewById(R.id.screen_passenger_time_select_current_time_label);
        
        
        mDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
        
        dateTimePickerLayout = (LinearLayout) findViewById(R.id.screen_passenger_time_select_datetime_picker);
        
        mDatePicker = (DatePicker) findViewById(R.id.screen_passenger_time_select_date_picker);
        mTimePicker = (TimePicker) findViewById(R.id.screen_passenger_time_select_time_picker);

        // �����
        backButton = (Button) this.findViewById(R.id.header_backbutton);
        backButton.setOnClickListener( new OnClickListener(){
			@Override
			public void onClick(View v) {
				finish(); 
			}
        });
        
        title = (TextView) this.findViewById(R.id.header_title);
        title.setText( getString(R.string.title_screen_passenger_time_select) );
        
        completeButton = (Button) this.findViewById(R.id.header_completebutton);
        completeButton.setVisibility(View.VISIBLE);
        completeButton.setOnClickListener( new OnClickListener(){
			@Override
			public void onClick(View v) {
				AppSettings.currentOrder.setVoyageTime(voyageTime);
				finish();
			}
        });        
        
        isCurrentTime = (Switch) findViewById(R.id.screen_passenger_time_select_current_time_switcher);
        isCurrentTime.setOnCheckedChangeListener(new OnCheckedChangeListener(){
			@Override
			public void onCheckedChanged(CompoundButton buttonView,	boolean isChecked) {
		    	if( mode == MODE_CUSTOM_TIME ){
		    		setMode( MODE_CURRENT_TIME );
		    	} else {
		    		setMode( MODE_CUSTOM_TIME );
		    	}				
			}
        });
        
        init();
        
    }
    
    private void init(){
        
    	Calendar cal = Calendar.getInstance();
    	
    	isCustomTimeChanged = false;
    	voyageTime = AppSettings.currentOrder.getVoyageTime();
        if( voyageTime > 0 ){
        	setMode( MODE_CUSTOM_TIME );
        	isCustomTimeChanged = true;
        	cal.setTimeInMillis(((long) voyageTime) * 1000L);
        } else {
        	setMode( MODE_CURRENT_TIME );
        	voyageTime = cal.getTimeInMillis() / 1000L;
    	}
    	
        mDatePicker.init(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE), new OnDateChangedListener() {
			@Override
			public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
				setCustomTime();
			}
        	
        });
        
        mTimePicker.setIs24HourView(android.text.format.DateFormat.is24HourFormat(this));
        mTimePicker.setCurrentHour(cal.get(Calendar.HOUR_OF_DAY));
        mTimePicker.setCurrentMinute(cal.get(Calendar.MINUTE));
        mTimePicker.setOnTimeChangedListener(new OnTimeChangedListener(){
			@Override
			public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
				setCustomTime();
			}
        });        
        
        // ���������� �������
        Thread myThread = null;

        Runnable runnable = new CountDownRunner();
        myThread= new Thread(runnable);   
        myThread.start();
    }
    
    
    private void setMode( int _mode ){
    	switch(_mode){
    		case MODE_CURRENT_TIME:
    			Log.d(TAG, "MODE_CURRENT_TIME");
    			mode = MODE_CURRENT_TIME; 
    			txtVoyageTimeTitle.setText(getString(R.string.button_screen_time_select_current_time));
    			dateTimePickerLayout.setVisibility(View.GONE);
    			break;
    		case MODE_CUSTOM_TIME:
    			Log.d(TAG, "MODE_CUSTOM_TIME");
    			mode = MODE_CUSTOM_TIME;
    			txtVoyageTimeTitle.setText(getString(R.string.button_screen_time_select_custom_time));
				Calendar cal = Calendar.getInstance();
				cal.setTimeInMillis(((long) voyageTime) * 1000L);
				txtVoyageTime.setText(mDateFormat.format(cal.getTime()));
    			dateTimePickerLayout.setVisibility(View.VISIBLE);
    			break;
    	}
    }
    
    public void doWork() {
        runOnUiThread(new Runnable() {
            public void run() {
                try {
                	Calendar cal = Calendar.getInstance(); 
                	if( mode != MODE_CURRENT_TIME ){
                		if( isCustomTimeChanged ){
                			cal.setTimeInMillis(((long) voyageTime) * 1000L);
                		}
                	}
                	txtVoyageTime.setText(mDateFormat.format(cal.getTime()));
                } catch( Exception e ){
                	e.printStackTrace();
                	//Log.d(TAG, e.getLocalizedMessage());
                }
            }
        });
    }    
    
    class CountDownRunner implements Runnable{
        // @Override
        public void run() {
                while(!Thread.currentThread().isInterrupted()){
                    try {
                    doWork();
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                            Thread.currentThread().interrupt();
                    }catch(Exception e){
                    }
                }
        }
    }    
    
    private void setCustomTime(){

    	isCustomTimeChanged = true;
    	
    	Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, mDatePicker.getYear());
        cal.set(Calendar.MONTH, mDatePicker.getMonth());
        cal.set(Calendar.DAY_OF_MONTH, mDatePicker.getDayOfMonth());
        cal.set(Calendar.HOUR_OF_DAY, mTimePicker.getCurrentHour());
        cal.set(Calendar.MINUTE, mTimePicker.getCurrentMinute());
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);        
        
        txtVoyageTime.setText(mDateFormat.format(cal.getTime()));
        
        voyageTime = cal.getTimeInMillis() / 1000L;
    }
    
    

}
