package com.martianlab.taxitorg.passenger;

import java.io.IOException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

import com.martianlab.taxitorg.R;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.martianlab.taxitorg.AppSettings;
import com.martianlab.taxitorg.common.ErrorAlert;
import com.martianlab.taxitorg.common.GeoUtility;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

/**
 * �������� ������ ������ ��� ������ �������� ����� �� �����
 * 
 * @author awacs
 */
public class ScreenPassengerMapActivity extends FragmentActivity implements LocationListener, LocationSource, OnMarkerClickListener, OnMapLongClickListener, OnMarkerDragListener {
	
	private final static String TAG = "ScreenMapActivity"; 
	
	private GoogleMap mMap;
	
	private LocationManager lManager;
	private OnLocationChangedListener mListener;
	
	private static double mLatitude;
	private static double mLongitude;
	
	
	private TextView addressLineTextView;
	
	private String selectedAddressLine;
	private double selectedLatitude;
	private double selectedLongitude;
	
	private boolean firstLoad = true; 
	
	private TextView title;
	private Button backButton;
	private Button completeButton;
	
	private int direction; 	
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState); 
        setContentView(R.layout.activity_screen_passenger_map);
        
        // ��������� Google Play Services
        //int isGMSAvailable =
        //    GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        
        Bundle extras = getIntent().getExtras();
		direction = AppSettings.KEY_DIRECTION_START;
        if( extras != null ){
			direction = extras.getInt( AppSettings.KEY_DIRECTION );
		}
        
        addressLineTextView = (TextView)findViewById(R.id.screen_passenger_map_address_line);
        
        // title
        title = (TextView) findViewById(R.id.header_title);
        if( direction == AppSettings.KEY_DIRECTION_START ){
        	title.setText(getString( R.string.title_screen_passenger_map_start_address ));        	
        } else {
        	title.setText(getString( R.string.title_screen_passenger_map_finish_address ));
        }
        
        
    	// ������ "Complete" �������� ���������� ������ ������������ � �����
        completeButton = (Button) findViewById(R.id.header_completebutton);
        completeButton.setVisibility(View.VISIBLE);
        completeButton.setEnabled(false);
        completeButton.setOnClickListener( new OnClickListener(){
			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				intent.putExtra("address", selectedAddressLine);
				intent.putExtra("latitude", selectedLatitude);
				intent.putExtra("longitude", selectedLongitude);
				setResult(RESULT_OK, intent);
				finish(); 
			}
        });

        backButton = (Button) findViewById(R.id.header_backbutton);
        backButton.setOnClickListener( new OnClickListener(){
			@Override
			public void onClick(View v) {
				finish();
			}
        });        
        
		lManager = (LocationManager) getSystemService(LOCATION_SERVICE);
		
        setUpMapIfNeeded(); 
    }
    
	public void button_screen_passenger_map_back_click(View v){
		finish();
	}
	

	/**
	 * �� �������� ����������� ��������������
	 */
	private void buildAlertMessageNoGps() {
		    final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		    builder.setMessage(getString(R.string.dialog_gps_disabled))
		           .setCancelable(false)
		           .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
		               public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
		                   startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
		               }
		           })
		           .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
		               public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
		                    dialog.cancel();
		               }
		           });
		    final AlertDialog alert = builder.create();
		    alert.show();
	}	
    
    private void setUpMapIfNeeded() {
        if (mMap == null) {
        	firstLoad = true;
        	mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapView)).getMap();
        	mMap.setMyLocationEnabled(true);
            mMap.setOnMarkerClickListener(this);
            mMap.setOnMarkerDragListener(this);
            mMap.setOnMapLongClickListener(this);
            
            // ��� ��������� �������� ���� ��������� ��������� �������
            if( lManager != null ){
	            Location l = null;
	            List<String> providers = lManager.getProviders(true);
	            for (int i=providers.size()-1; i>=0; i--) {
	                l = lManager.getLastKnownLocation(providers.get(i));
	                if (l != null) break;
	            }
	            if (l != null) {
	                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(l.getLatitude(), l.getLongitude()), GeoUtility.zoomLevel(AppSettings.DEFAULT_RADIUS_FOR_PASSENGER_MAP)));
	            }
            }
        }
    }    

	
	 protected void onPause() {
		 if( lManager != null ){ 
			 lManager.removeUpdates(this);
		 }
  	     super.onPause();		  
	 }

	
	protected void onResume() {
		super.onResume();
		setUpMapIfNeeded();
		if( lManager != null ){
			firstLoad = true;
	        // ���� �� �������� ����������� ��������������
            if( !lManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && !lManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER) ){
    			buildAlertMessageNoGps(); 
    		} else {
    			lManager.requestLocationUpdates( lManager.getBestProvider(new Criteria(), true), 0, 0, this);
    		}
		}
	}

	@Override
	public void activate(OnLocationChangedListener listener) {
		mListener = listener;
	}

	@Override
	public void deactivate() {
		mListener = null;
	}

	@Override
	public void onLocationChanged(Location location) {
		mLatitude = location.getLatitude();
		mLongitude = location.getLongitude();
		double speed = location.getSpeed();
		String provider = location.getProvider();
		int    time = new Time(location.getTime()).getSeconds();
		Log.d(TAG, "onLocationChanged - ������: " + mLatitude + " | �������: " + mLongitude + " | speed: " + speed
				+ " | provider: " + provider + " | time: " + time + "s");
		if( mListener != null ){
			mListener.onLocationChanged( location );
		}
		if( firstLoad ){
			firstLoad = false;
			mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLatitude, mLongitude), GeoUtility.zoomLevel(AppSettings.DEFAULT_RADIUS_FOR_PASSENGER_MAP)));
		}		
	}

	@Override
	public void onProviderDisabled(String provider) {
		Log.d(TAG, "Provider disabled. GPS is off");
	}

	@Override
	public void onProviderEnabled(String provider) {
		Log.d(TAG, "Provider enabled. GPS is on");
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		Log.d(TAG, "Provider " + provider + " status " + status + " changed");
	}

	@Override
	public boolean onMarkerClick(Marker marker) {
		return false;
	}

	@Override
	public void onMapLongClick(LatLng point) {
    	selectedLatitude = point.latitude;
    	selectedLongitude = point.longitude;
    	
		mMap.clear();
		mMap.addMarker(new MarkerOptions()
        .position(point)
        .draggable(true)
        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));    	
		
		completeButton.setEnabled(false);
		new GetAddressTask(this).execute(point);
	}	 
	
	@Override
	public void onMarkerDrag(Marker marker) {
	}

	@SuppressLint("NewApi")
	@Override
	public void onMarkerDragEnd(Marker marker) {
		LatLng point = marker.getPosition();
		selectedLatitude = point.latitude;
		selectedLongitude = point.longitude;
		// geocodingAddressBySelectedPoint();
		completeButton.setEnabled(false);
   	 	if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD && Geocoder.isPresent()){
   	 		new GetAddressTask(this).execute(point);
   	 	} else {
   	 		Log.e(TAG, "Geocoder is not present");
   	 	}
	}

	@Override
	public void onMarkerDragStart(Marker marker) {
	}
	
	private void post_decoding_latlng( String address ){
		if( !address.equals("") ){
			selectedAddressLine = address;
		} else {
			// ���� ����� �� ������������, ����������� ���� ������ ������ � �������
			selectedAddressLine = "lat: " + String.format("%1$,.4f", selectedLatitude) + " lon: " + String.format("%1$,.4f", selectedLongitude);
		}
		
		Log.d(TAG, "lat: " + selectedLatitude + " lon: " + selectedLongitude + " address: " + selectedAddressLine);
		
		addressLineTextView.setText( selectedAddressLine );
		completeButton.setEnabled(true);
	}
	
	
	private class GetAddressTask extends AsyncTask<LatLng, Void, String> {
		
		private Context context;
		
		public GetAddressTask( Context ctx ){
			this.context = ctx;
		}
		
		protected String doInBackground(LatLng... points) {
		    	 Geocoder gCoder = new Geocoder(context);
		    	 
		    	 LatLng point = points[0];
		     	 ArrayList<Address> addresses;
				 try {
				     addresses = (ArrayList<Address>) gCoder.getFromLocation(point.latitude, point.longitude, 1);
					 if (addresses != null && addresses.size() > 0) {
					     return addresses.get(0).getAddressLine(0);
					 }
				 } catch (IOException e) { 
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			return "";
	     }

	     protected void onPostExecute(String result) {
	         post_decoding_latlng(result);
	     }
	 }
}