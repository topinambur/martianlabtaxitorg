package com.martianlab.taxitorg.passenger;

import java.util.ArrayList;

import com.martianlab.taxitorg.R;
import com.martianlab.taxitorg.AppSettings;
import com.martianlab.taxitorg.common.ActiveOrder;
import com.martianlab.taxitorg.common.DBAdapter;
import com.martianlab.taxitorg.service.TaxitorgServicePassenger;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.WindowManager.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.ListActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

/***
 * ������� �������� ���������. ������� ������� ������ ������� � ������ ������� ����� �����
 * @author awacs
 *
 */
public class ScreenPassengerMain extends ListActivity {
	
	private static final String TAG = "ScreenPassengerMain";
	
	private TextView title;
	private Button backButton;
	
	/**
	 * ���� ������ ������� ������������
	 */
	private DBAdapter db;
	
	private int mOrdersCount; 
	
	private boolean isPassengerServiceStarted;
	
	private ArrayList<ActiveOrder> mOrders;
	
	/**
	 * �����������
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_screen_passenger_main);
		
        // ��� �������� ������� �� ����� �����
        getWindow().addFlags(LayoutParams.FLAG_KEEP_SCREEN_ON);
		
		title = (TextView) this.findViewById(R.id.header_title);
		title.setText(getString(R.string.title_screen_passenger_main));
		
        backButton = (Button) this.findViewById(R.id.header_backbutton);
        backButton.setOnClickListener( new OnClickListener(){
			@Override
			public void onClick(View v) {
				finish();
			}
        });		
        
        // ���������� ���� ������
        db = new DBAdapter(this);
        
        mOrdersCount = getOrdersCount();
        isPassengerServiceStarted = isTaxitorgServicePassengerRunning();
        Log.d(TAG, "Count of active orders = " + mOrdersCount);
        
        if( mOrdersCount > 0 && !isPassengerServiceStarted ){
        	// ���������� ������ ������ ���������/������� �������
    		Log.d(TAG, "Start passenger service");
        	Intent intent = new Intent(this, TaxitorgServicePassenger.class);
    		startService(intent);        	
        }
	}
	
	@Override 
	public void onResume() {
		super.onResume();
        getOrdersList();		
		registerReceiver(broadcastReceiver, new IntentFilter(AppSettings.BROADCAST_ACTION_PASSENGER));
	}
	
    @Override
    protected void onPause(){ 
    	unregisterReceiver(broadcastReceiver);
    	super.onPause();
    }
	
	
	@Override
	protected void onListItemClick(ListView items, View v, int position, long id) {
		ActiveOrder order = (ActiveOrder)items.getItemAtPosition(position);
    	Intent intent = new Intent(); 
	    intent.setClass(this, ScreenSearchOffers.class); 
	    intent.putExtra("order_id", order.order_id);
	    startActivity(intent);
		
	}
    
	private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String task = intent.getStringExtra("task");
			String orderId = intent.getStringExtra("order_id");
			String results = intent.getStringExtra("results");
			Log.d(TAG, task + " -> " + results);
			if( task.equals( AppSettings.REQUEST_GET_DRIVER_LIST ) ){
				parseDriverOffers(orderId, results);
			}
			
			if( task.equals( AppSettings.REQUEST_CHECK_ORDER_STATUS ) ){
				parseOrderStatus( orderId, results );
			}
		}
	};		

	
	/**
	 * ������ ����������� �� ���������
	 * @param serializedString
	 */
	private void parseDriverOffers( String _order_id, String drivers ){
		// �������� ������ ���� orderId,driversList

		long order_id = Long.parseLong(_order_id);
		int countOffers = 0;
		for( ActiveOrder order : mOrders ){
			if( order.order_id == order_id ){
				countOffers += order.setDriverOffers(drivers);
			}
		}
		if( countOffers > 0 ){
			refreshList();
		}
	}
	
	/**
	 * ������ ��������� ������
	 */
	private void parseOrderStatus( String _order_id, String _status ){
		long order_id = Long.parseLong(_order_id);
		int current_status = Integer.parseInt( _status );
		
		boolean isChanged = false;
		for( ActiveOrder order : mOrders ){
			if( order.order_id == order_id ){
				if( order.current_status != current_status ){
					order.setCurrentStatus(current_status);
					isChanged = true;
				}
			}
		}		
		
		if( isChanged ){
			refreshList();
		}
	}
	
	/** 
	 * �������� ������� ������ �������
	 */
	private void refreshList(){
		OrdersCursorAdapter addressListAdapter = new OrdersCursorAdapter(this, mOrders);
		setListAdapter(addressListAdapter);
	}
	
	
	/**
	 * �������� ���������� �������� ���������������� �������
	 */
	private int getOrdersCount(){
		int res = 0;
		try{
			db.open();
			res = db.getOrdersCount();
		} catch(Exception ex){
			Toast toast = Toast.makeText(getApplicationContext(), ex.toString(), Toast.LENGTH_LONG);
			toast.show(); 
        } finally {
			db.close();
        }
		return res;
	}
	
	/**
	 * �������� ������ ������� �������
	 */
	private void getOrdersList(){
        
		// ���������� ������
        try{
        	db.open();
        	mOrders = db.getOpenPassengerOrders();
			refreshList();
        } catch(Exception ex){
			Toast toast = Toast.makeText(getApplicationContext(), ex.toString(), Toast.LENGTH_LONG);
			toast.show(); 
        } finally {
        	db.close();
        }
	}
	
	/**
	 * ������ "������� ����� �����"
	 * @param v
	 */
	public void button_screen_passenger_main_neworder_click( View v ){
    	Intent intent = new Intent(); 
	    intent.setClass(this, ScreenPassengerOrder.class); 
	    startActivity(intent); 
	}
	

	
	/**
	 * ������� ������ �������� �������
	 * @author awacs
	 *
	 */
    private class OrdersCursorAdapter extends ArrayAdapter<ActiveOrder> {

		private Context context;
		private ArrayList<ActiveOrder> items;
		
		public OrdersCursorAdapter(Context context, ArrayList<ActiveOrder> orders) {
			super(context, R.layout.row_passenger_order);

			this.items = orders;
			this.context = context;

		}

		public int getCount() {
			return items.size();
		}
		
		public ActiveOrder getItem( int position ){
			return items.get(position);
		}
		

	    public long getItemId(int position) {
	        return items.get(position).order_id;
	    }		
		
		public View getView(int pos, View inView, ViewGroup parent) {

			View v = inView;
			
			// Associate the xml file for each row with the view
			if (v == null) {
				LayoutInflater inflater = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE); 
				v = inflater.inflate(R.layout.row_passenger_order, null);
			}

			ActiveOrder order = items.get(pos);

			String sAddressStart = order.address_start;
			String sAddressEnd   = order.address_end;
			int current_status   = order.current_status;
			long voyageTime      = order.voyage_time;
			
			TextView tvAddressStart = (TextView) v.findViewById(R.id.row_passenger_order_address_start);
			TextView tvAddressEnd = (TextView) v.findViewById(R.id.row_passenger_order_address_end); 
			TextView tvCurrentStatus = (TextView) v.findViewById(R.id.row_passenger_status_text);
			tvAddressStart.setText(sAddressStart);
			tvAddressEnd.setText(sAddressEnd);

			if( current_status == AppSettings.ORDER_STATUS_OPEN ){
				if( order.driverOffers.size() > 0 ){
					tvCurrentStatus.setText(getString(R.string.screen_passenger_main_offer));
				} else {
					tvCurrentStatus.setText(getString(R.string.screen_passenger_main_search));
				}
			} else {
				switch( current_status ){
					case AppSettings.ORDER_STATUS_CLOSE:
						tvCurrentStatus.setText(getString(R.string.screen_passenger_main_order_closed));
						break;
					case AppSettings.ORDER_STATUS_PASSENGER_DECLINE_DRIVER:
						tvCurrentStatus.setText(getString(R.string.screen_passenger_main_order_passenger_decline_driver));
						break;
					case AppSettings.ORDER_STATUS_PASSENGER_ACCEPT_DRIVER:
						tvCurrentStatus.setText(getString(R.string.screen_passenger_main_order_passenger_accept_driver));
						break;
					case AppSettings.ORDER_STATUS_DRIVER_DECLINE_ORDER:
						tvCurrentStatus.setText(getString(R.string.screen_passenger_main_order_driver_decline_order));
						break; 
					case AppSettings.ORDER_STATUS_PASSENGER_DECLINE_ORDER:
						tvCurrentStatus.setText(getString(R.string.screen_passenger_main_order_passenger_decline_order));
						break;
					case AppSettings.ORDER_STATUS_DRIVER_SAY_DELAY:
						tvCurrentStatus.setText(getString(R.string.screen_passenger_main_order_driver_say_delay));
						break; 
					case AppSettings.ORDER_STATUS_DRIVER_SAY_ARRIVE:
						tvCurrentStatus.setText(getString(R.string.screen_passenger_main_order_driver_say_arrive));
						break;
				}
			}
			
			return (v);

		} // function getView

	} // class PhraseCursorAdapter	
    
    
	/**
	 * ��������� ������� �� ������ ������ ���������
	 * @return
	 */
	private boolean isTaxitorgServicePassengerRunning() {
		ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
			if (AppSettings.BROADCAST_ACTION_PASSENGER.equals(service.service.getClassName())) {
				return true; 
			}
		}
		return false;
	}

}
