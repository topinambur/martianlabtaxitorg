package com.martianlab.taxitorg.passenger;

import java.io.IOException;
import java.util.ArrayList;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import com.martianlab.taxitorg.R;
import com.martianlab.taxitorg.AppSettings;
import com.martianlab.taxitorg.common.DBAdapter;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.XmlResourceParser;

/**
 * ����� ��� ������ �� ������ ��������� ��� �/� �������
 * @author awacs
 *
 */
public class ScreenAirportSelection extends Activity /*ListActivity*/ {
	
	private final static String TAG = "ScreenAirportSelectionActivity";
	
	private ArrayList<Item> items;
	
	private int direction;
	private int typeStation;

	private DBAdapter db = new DBAdapter(this);
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
	   super.onCreate(savedInstanceState);
	   setContentView(R.layout.activity_screen_passenger_airport_selection);
	   
       // ���������
       TextView title = (TextView) this.findViewById(R.id.header_title);
       
       Button backButton = (Button)this.findViewById(R.id.header_backbutton);
       backButton.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				finish();
			}
       });
	   
	   // �������� �� ��������� ����������� (����� ����� ����� ��������� �����/������/������� � ������ ����� ������� Order)
	   Bundle extras = getIntent().getExtras();
	   if( extras != null ){
		   if( extras.containsKey(AppSettings.KEY_DIRECTION ) ){
			   direction = extras.getInt( AppSettings.KEY_DIRECTION );
		   }
		   if( extras.containsKey(AppSettings.KEY_TYPE_STATION) ){
			   typeStation = extras.getInt(AppSettings.KEY_TYPE_STATION);
		   }
		   
	   }
	   
	   // ������ XML �� ������� ����������, ������ ������������� ��������� ��������
	   switch( typeStation ){
	       case AppSettings.KEY_TYPE_STATION_AIRPORT:
	    	   loadAirportsFromXML( this.getResources().getXml(R.xml.airports) );
			   title.setText( this.getString(R.string.title_screen_airport_select) );	    	   
	    	   break;
	       case AppSettings.KEY_TYPE_STATION_RAILROAD: 
	           loadAirportsFromXML( this.getResources().getXml(R.xml.railway_stations) );
			   title.setText( this.getString(R.string.title_screen_railway_station_select) );	           
	           break;
	   }
	   
	   ListView listView = (ListView) findViewById(R.id.screen_passenger_airport_selection_list);
	   listView.setAdapter(new MyCustomAdapter(ScreenAirportSelection.this, R.layout.row_airport, items));
	   listView.setOnItemClickListener( new OnItemClickListener(){
			@Override
			public void onItemClick(AdapterView<?> l, View v, int position, long id) {
				selectAirport(l, position );
			}
	   });
	}
	
	private void selectAirport(AdapterView<?> l, int position){
		Item airport = (Item)l.getItemAtPosition(position);

		double lat = airport.latitude;
		double lon = airport.longitude;
		String addr = airport.name;
    	Log.d(TAG, TAG+"::Set start/end point: latitude="+lat+"longitude="+lon);
    	Log.d(TAG, TAG+"::Set start/end address: " + addr);
		Log.d(TAG, "direction = " + direction + " " + AppSettings.KEY_DIRECTION_START + " " + AppSettings.KEY_DIRECTION_FINISH);
		saveAddressToDB( addr, lat, lon );		
		
    	if( direction == AppSettings.KEY_DIRECTION_START ){
			Log.d(TAG, "KEY_DIRECTION_START");
			AppSettings.currentOrder.setStartAddress(addr);
			AppSettings.currentOrder.setStartPoint(lat, lon);
        }
        if( direction == AppSettings.KEY_DIRECTION_FINISH ){
        	Log.d(TAG, "KEY_DIRECTION_FINISH");
        	AppSettings.currentOrder.setEndAddress(addr);
        	AppSettings.currentOrder.setEndPoint(lat, lon);		        
        }
        
        if( typeStation == AppSettings.KEY_TYPE_STATION_AIRPORT && direction == AppSettings.KEY_DIRECTION_START ){
	    	Intent intent = new Intent(); 
	    	intent.setClass(this, ScreenAirportPreferences.class);
	    	intent.putExtra("airport_name", addr);
		    startActivityForResult(intent, 1);
        } else {
        	finish();
        }
        
	}

	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 1) {
		     if(resultCode == RESULT_OK){
		         boolean meetNameplate = data.getBooleanExtra("meetNameplate", false);
		         String flightNumber = data.getStringExtra("flightNumber");
		         String nameplateText = data.getStringExtra("nameplateText");
		     	 AppSettings.currentOrder.setAirportPreferences( meetNameplate, flightNumber, nameplateText);
		     	 finish();
		     }
		}		
	}
	
	/**
	 * �������� ������ ���������� �� XML
	 * @param xrp
	 */
	private void loadAirportsFromXML( XmlResourceParser xrp ){
		
		items = new ArrayList<Item>();
		
		try {

	        int eventType = xrp.getEventType();
	        while (eventType != XmlPullParser.END_DOCUMENT) {
	            if(eventType == XmlPullParser.START_TAG) {
	                if(xrp.getName().equals( "item" )) {
	                    addItem(xrp);
	                }
	            }
	            eventType = xrp.next();
	        }
		}catch(IOException ioe) {
	        Log.e("Exception", ioe.getStackTrace().toString());
		}catch(XmlPullParserException xppe) {
	        Log.e("Exception", xppe.getStackTrace().toString());
		}finally{ 
	        xrp.close();
		} 		
	}	
	
	/**
	 * ������� ������ XML � ������� �� ���������
	 * @param xrp
	 */
	private void addItem(XmlResourceParser xrp){
        String name = ""; 
        String city = "";
        String latitude = "";
        String longitude = "";
    	for(int i=0; i<xrp.getAttributeCount(); i++) {
    		if( xrp.getAttributeName(i).equals("name") ){
    			name = xrp.getAttributeValue(i);
    		} else if( xrp.getAttributeName(i).equals("city") ){
        		city = xrp.getAttributeValue(i);
    		} else if( xrp.getAttributeName(i).equals("latitude") ){
    			latitude = xrp.getAttributeValue(i);
    		} else if ( xrp.getAttributeName(i).equals("longitude") ){
    			longitude = xrp.getAttributeValue(i);
    		}
    	}
    	
    	items.add( new Item(name, Integer.parseInt(city), Double.parseDouble(latitude), Double.parseDouble(longitude)) );
	}
	
	
	/**
	 * ��������� ����� ��� �������� ���������
	 * @author awacs
	 *
	 */
	private class Item{
		public String name = "NoName";
		public int city = 0;
		public double latitude = 0.0d;
		public double longitude = 0.0d;
		
		public Item( String _name, int _city, double _latitude, double _longitude ){
			name = _name;
			city = _city;
			latitude = _latitude;
			longitude = _longitude;
		}
	}
	
	
	private void saveAddressToDB( String address, double latitude, double longitude ){
		try {
			db.open();
			db.insert_address(address, latitude, longitude);
			db.close();
		} catch (Exception ex) {
			Toast toast = Toast.makeText(getApplicationContext(), ex.toString(), Toast.LENGTH_LONG);
			toast.show();
		}		
	}	
	
	/**
	 * ������� ��� ����������� ������ ���������� � ���� �����
	 * @author awacs
	 *
	 */
	private class MyCustomAdapter extends ArrayAdapter<Item> {
		
		private ArrayList<Item> _items;
		
		private int viewId;
		
		public MyCustomAdapter(Context c, int textViewResourceId,	ArrayList<Item> litems) {
			super(c, textViewResourceId, litems);
			//this.context = c;
			this._items = litems;
			this.viewId = textViewResourceId; 
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LayoutInflater inflater=getLayoutInflater();
			View row=inflater.inflate(viewId, parent, false);
			TextView label = (TextView)row.findViewById(R.id.airport_row_label);
			label.setText(_items.get(position).name);
			return row;
		}
	}	
	
}