package com.martianlab.taxitorg.passenger;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;

import com.martianlab.taxitorg.R;
import com.google.android.gms.maps.model.LatLng; 
import com.martianlab.taxitorg.AppSettings;
import com.martianlab.taxitorg.common.DBAdapter;
import com.martianlab.taxitorg.driver.ScreenDriverOrdersMap;
import com.martianlab.taxitorg.handlers.AcceptOrderHandler;
import com.martianlab.taxitorg.handlers.RouteHandler;
import com.martianlab.taxitorg.handlers.RouteListener;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class ScreenPassengerOrder extends Activity implements RouteListener {
	

	
	private final static String TAG = "ScreenPassengerOrder";
	
	
	private String[] currencies = {"rur", "eur", "usd"};
	
	/**
	 * ���������� ���� ������ �����������
	 */
	private PopupWindow popUp;	
	
	/**
	 * 
	 */
	private TextView startAddressLine; 
	private TextView endAddressLine;
	private EditText priceEditText;
	private TextView voyageTimeText;
	
	private RouteHandler routeHandler;
	
	/**
	 * ������� ��������� ���� ������
	 */
	private DBAdapter db;
	
	private DateFormat mDateFormat;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	
    	Log.i( TAG, TAG+"::OnCreate()" );
    	
        super.onCreate(savedInstanceState); 
        setContentView(R.layout.activity_screen_passenger_order);
        
        mDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
        
        // ���������
        TextView title = (TextView) this.findViewById(R.id.header_title);
        title.setText( this.getString(R.string.title_screen_passenger_order) );
        
        // ������ �����
        Button backButton = (Button)this.findViewById(R.id.header_backbutton);
        backButton.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				finish();
			}
        });
        
        // ������ "Complete"
        Button completeButton = (Button)this.findViewById(R.id.header_completebutton);
        completeButton.setVisibility(View.VISIBLE);
        completeButton.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				showCreateOrderPopUp();
			}
        });        
        
    	// ���� ������
        db = new DBAdapter(this);
        
        // ������� ��� ������ �����
        ArrayAdapter<String> adapter�urrencies = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, currencies);
        adapter�urrencies.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        
        startAddressLine = (TextView) findViewById(R.id.screen_passenger_order_start_address);
        endAddressLine = (TextView) findViewById(R.id.screen_passenger_order_end_address);
        voyageTimeText = (TextView) findViewById(R.id.screen_passenger_order_voyagetime);
        
        priceEditText = (EditText) findViewById(R.id.screen_passenger_order_price);
        priceEditText.addTextChangedListener( priceTextWatcher );
        
        
        
        
        Spinner spinner = (Spinner) findViewById(R.id.spinner_screen_passenger_order_currencySelector);
    	spinner.setAdapter(adapter�urrencies);
    	spinner.setPrompt("Currency");
    	spinner.setSelection(0);
    	spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
        	@Override
        	public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        		setCurrency(currencies[position]);
        	}
        	@Override
        	public void onNothingSelected(AdapterView<?> arg0) {
        	}
    	});        

        init();
     
    }
    
    private void setCurrency( String cur ){
    	Log.d(TAG, "Set currency: " + cur);
    	AppSettings.currentOrder.setCurrency( cur );
    }
   
    private void setPrice( String value ){
    	Log.d(TAG, "Set price: " + value);
    	Pattern p = Pattern.compile("[0-9\\.]+");
    	Matcher m = p.matcher(value);
    	if( m.matches() ){
        	AppSettings.currentOrder.setPrice( Float.parseFloat( value ) );    		
    	}
    }

    
    /**
     * �������� ������� ����� � ���� ������
     * @return
     */
    private long saveCurrentOrder(){
    	long res = -1;

    	// ���������� ������
        try{
        	db.open();
        	res = db.saveOrder(AppSettings.currentOrder); 
        	db.close();
        } catch(Exception ex){
			Toast toast = Toast.makeText(getApplicationContext(), ex.toString(), Toast.LENGTH_LONG);
			toast.show(); 
        }		
        Log.d(TAG, "Saved local orderId = " + res);
    	return res;
    }    
    
    private TextWatcher priceTextWatcher = new TextWatcher(){
		@Override
		public void afterTextChanged(Editable s) {
			setPrice( s.toString() );
		}
		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,	int after) {
		}
		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
		}
    };
    
    /**
     * ���� ������� ������ ���������, ��������� ����� �� �������� ������ �����������
     * ����� ����� �� ������� ������� ����� ������������ (���� �� ���), ��� ������ ����� �� AppSettings.currentOrder
     */
    private void init(){
    	if( !AppSettings.currentOrder.getAddressStart().equals("") ){
    		startAddressLine.setText(AppSettings.currentOrder.getAddressStart());
    	}
    	if( !AppSettings.currentOrder.getAddressEnd().equals("") ){
    		endAddressLine.setText(AppSettings.currentOrder.getAddressEnd());        		
    	}
    	
    	long voyageTime = AppSettings.currentOrder.getVoyageTime(); 
    	if( AppSettings.currentOrder.getVoyageTime() > 0 ){
    		Calendar cal = Calendar.getInstance();
    		cal.setTimeInMillis(((long) voyageTime) * 1000L);
    		voyageTimeText.setText(mDateFormat.format(cal.getTime()));
    	} else {
    		voyageTimeText.setText(getString(R.string.screen_passenger_order_currenttime_hint));
    	}
    	
    	JSONArray params = AppSettings.currentOrder.getParamsAsJSONArray(); 
    }
     
    
    @Override
    protected void onPause(){
    	Log.d(TAG, "onPause()");
    	if( popUp != null && popUp.isShowing() ){
    		popUp.dismiss();
    	}
    	super.onPause();
    }
    
    @Override 
    protected void onResume(){
    	Log.d(TAG, "onResume()");
    	super.onResume();
    	init();
    }

    
	/**
	 * ������ �����
	 * @param v
	 */
	public void btn_back_screen_passenger_order_click(View v){
		finish();
	}
	
    
    /**
     * ������� ����� �����������
     * @param v
     */
    public void btn_setStartAddress_screen_passenger_order_click( View v ){
    	Log.d(TAG, "btn_setStartAddress_screen_passenger_order_click");
    	Intent intent = new Intent(); 
	    intent.setClass(this, ScreenAddressSelect.class);
	    intent.putExtra(AppSettings.KEY_DIRECTION, AppSettings.KEY_DIRECTION_START);
	    startActivity(intent);
    }

    /**
     * ������� ����� ����������
     * @param v
     */
    public void btn_setEndAddress_screen_passenger_order_click( View v ){
    	Log.d(TAG, "btn_setEndAddress_screen_passenger_order_click");
    	Intent intent = new Intent(); 
	    intent.setClass(this, ScreenAddressSelect.class); 
	    intent.putExtra(AppSettings.KEY_DIRECTION, AppSettings.KEY_DIRECTION_FINISH);
	    startActivity(intent);
    }

    /**
     * ������� ����� �����������
     * @param v
     */
    public void btn_timeSelect_screen_passenger_order_click( View v){
    	Log.d(TAG, "btn_timeSelect_screen_passenger_order_click");
    	Intent intent = new Intent();
    	intent.setClass(this, ScreenTimeSelectActivity.class);
    	startActivity(intent);
    } 
    
    /**
     * ������� ������������
     * @param v
     */
    public void btn_preferencesSelect_screen_passenger_order_click( View v ){
    	Log.d(TAG, "btn_preferencesSelect_screen_passenger_order_click");
    	Intent intent = new Intent(); 
	    intent.setClass(this, ScreenPreferencesActivity.class); 
	    startActivity(intent);     	
    }
    
    
    /**
     * ������ ��������� �������
     * @param v
     */
    public void btn_priceSelect_screen_passenger_order_click(View v){
    	Log.d(TAG, "btn_priceSelect_screen_passenger_order_click");
    }

    
    /**
     * ���������� �����
     * @param v
     */
    public void button_popup_order_create_search_click(View v){
    	Log.d(TAG, "button_popup_order_create_search_click");
    	
    	long row_id = saveCurrentOrder();
    	
    	if( row_id > -1 ){
	    	Intent intent = new Intent(); 
		    intent.setClass(this, ScreenSearchOffers.class); 
		    intent.putExtra("row_id", row_id);
		    intent.putExtra("create_order", true);
		    intent.putExtra("phonenumber", false);
		    intent.putExtra("demonstration", false);
		    startActivity(intent);
	    	popUp.dismiss();
	    	this.finish();
    	}
    }
    
    /**
     * ���������� ����� � �������� �������
     * @param v
     */
    public void button_popup_order_create_search_and_show_phone_click(View v){
    	Log.d(TAG, "button_popup_order_create_search_and_show_phone_click");

		AlertDialog.Builder alert = new AlertDialog.Builder(this);
		alert.setTitle( getString(R.string.screen_passenger_order_dialog_phone_title) );
		final EditText input = new EditText(this);
		input.setInputType(InputType.TYPE_CLASS_PHONE);
		alert.setView(input);

		alert.setPositiveButton(getString(R.string.button_ok), new DialogInterface.OnClickListener() {
		    public void onClick(DialogInterface dialog, int whichButton) {
		    	AppSettings.currentOrder.setPhonenumber( input.getText().toString()  );
		    	long row_id = saveCurrentOrder();
		    	if( row_id > -1 ){
			    	Intent intent = new Intent(); 
				    intent.setClass(ScreenPassengerOrder.this, ScreenSearchOffers.class);
				    intent.putExtra("row_id", row_id);
				    intent.putExtra("create_order", true);
				    intent.putExtra("phonenumber", true);
				    intent.putExtra("demonstration", false);
				    startActivity(intent);     	
			    	dialog.cancel();
				    finish();
		    	}		    	
		    }
		});

		alert.setNegativeButton(getString(R.string.button_cancel), new DialogInterface.OnClickListener() {
		    public void onClick(DialogInterface dialog, int whichButton) {
			    dialog.cancel();
		    }
		});

		alert.show();    	
    	
    }
    
    /**
     * ������������
     * @param v
     */
    public void button_popup_order_create_demonstration_click(View v){
    	Log.d(TAG, "button_popup_order_create_demonstration_click");
    	long row_id = saveCurrentOrder();
    	
    	if( row_id > -1 ){
	    	Intent intent = new Intent(); 
		    intent.setClass(this, ScreenSearchOffers.class);
		    intent.putExtra("row_id", row_id);
		    intent.putExtra("create_order", true);
		    intent.putExtra("phonenumber", false);
		    intent.putExtra("demonstration", true);
		    startActivity(intent);     	
		    popUp.dismiss();
		    this.finish();
    	}
    }
    
    /**
     * ������� ���������� ���� ������������� ������
     * @param v
     */
    public void button_popup_order_create_cancel_click(View v){
    	Log.d(TAG, "button_popup_order_create_cancel_click");
    	popUp.dismiss();
    }
    
    /**
     * ���������� ��������� ��������� ������
     */
    public void screen_passenger_order_button_calculate_price_click(View v){
    	routeHandler = new RouteHandler(this);
    	routeHandler.calculateRoute(
    			  AppSettings.currentOrder.getStartLatitude()
    			, AppSettings.currentOrder.getStartLongitude()
    			, AppSettings.currentOrder.getEndLatitude()
    			, AppSettings.currentOrder.getEndLongitude()
    			, AppSettings.COARSE_ROUTE
    	);
    }
    
    /**
     * ������� POPUP � ���������� ���������� ������ (�������, ������� � �������� �����, ������������)
     * @param v
     */
    private void showCreateOrderPopUp(){
    	
    	DisplayMetrics metrics = this.getApplicationContext().getResources().getDisplayMetrics();
    	int width = metrics.widthPixels;
    	int height = metrics.heightPixels;    	
    	
    	popUp = new PopupWindow(this); 
    	 
    	LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View popLayout = inflater.inflate(R.layout.popup_order_create, null);
    	
        popUp.setContentView(popLayout);
        popLayout.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        popUp.setWidth( width );
        
        int defaultHeight = Math.round( height * 0.8f ); 
        
        if( defaultHeight > popLayout.getMeasuredHeight() ){
        	popUp.setHeight( defaultHeight );
        } else {
        	popUp.setHeight( popLayout.getMeasuredHeight() );        	
        }

        
        popUp.showAtLocation(popLayout, Gravity.BOTTOM, 0, 0);
        
    }

	@Override
	public void onRouteCalcBegin() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onBoundsCalculated(LatLng southwest, LatLng northeast) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onRouteCompleted(ArrayList<LatLng> route, long distance,
			long duration) {
		double price = routeHandler.getDistance() * AppSettings.PRICE_PER_KILOMETERS;
		priceEditText.setText( String.valueOf(Math.round(price*40)) );	
	}

	@Override
	public void onRouteCalcError(String error_msg) {
		// TODO Auto-generated method stub
		
	}	

	
	
}
