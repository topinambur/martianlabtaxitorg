package com.martianlab.taxitorg.passenger;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;


import com.martianlab.taxitorg.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.PolylineOptions;
import com.martianlab.taxitorg.AppSettings;
import com.martianlab.taxitorg.common.ActiveOrder;
import com.martianlab.taxitorg.common.DBAdapter;
import com.martianlab.taxitorg.common.Driver;
import com.martianlab.taxitorg.common.DriverOffer;
import com.martianlab.taxitorg.common.ErrorAlert;
import com.martianlab.taxitorg.common.GeoUtility;
import com.martianlab.taxitorg.common.Order;
import com.martianlab.taxitorg.handlers.CancelOrderHandler;
import com.martianlab.taxitorg.handlers.CancelOrderListener;
import com.martianlab.taxitorg.handlers.CreateOrderHandler;
import com.martianlab.taxitorg.handlers.CreateOrderListener;
import com.martianlab.taxitorg.handlers.DownloadImageHandler;
import com.martianlab.taxitorg.handlers.DownloadImageListener;
import com.martianlab.taxitorg.handlers.GetDriverInfoHandler;
import com.martianlab.taxitorg.handlers.GetDriverInfoListener;
import com.martianlab.taxitorg.handlers.GetDriverListHandler;
import com.martianlab.taxitorg.handlers.GetDriverListListener;
import com.martianlab.taxitorg.handlers.RouteHandler;
import com.martianlab.taxitorg.handlers.RouteListener;
import com.martianlab.taxitorg.handlers.SetOrderStatusHandler;
import com.martianlab.taxitorg.handlers.SetOrderStatusListener;
import com.martianlab.taxitorg.service.TaxitorgServicePassenger;
import com.martianlab.taxitorg.service.TaxitorgServicePassenger.ServicePassengerBinder;

import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.provider.Settings.Secure;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RatingBar;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.BroadcastReceiver;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;

/**
 * ����� ��� ����������� �������� ������ ������������. ������������ ��������� � �������� ����� � ������� ����� ����. 
 * � ������ �������� ����������� �� ��������� ������������ ���� � �������� �� �����������
 * 
 * � ��� �������� ����� ������ ��:
 *    - �������� ������ ������� �������
 *    - �������� �������� ������ ( � ���� ������ order_id ����� ������ ) 
 *    - �� Notify bar
 *    
 * @author awacs
 *
 */
public class ScreenSearchOffers extends FragmentActivity 
    implements 
         RouteListener, CreateOrderListener, DownloadImageListener
       , GetDriverListListener, SetOrderStatusListener, GetDriverInfoListener
       , CancelOrderListener
       
{
	
	//gfhjkmlkzwifi933
	private final static String TAG = "ScreenSearchOffers";
	
	public static final int SEARCH_OFFERS = 1;
	public static final int SEARCH_OFFERS_AND_SET_PHONE = 2;
	public static final int DEMONSTRATION = 3;
	
	/**
	 * ��� ������� ���������
	 */
	private boolean isBoundPassengerServer = false;
	private TaxitorgServicePassenger servicePassenger;
	private ServiceConnection servicePassengerConnection; 

	private PopupWindow popUpDrivers; 
	
	private ArrayList<DriverOffer> driverOffers;
	
	private Order currentOrder;
	
	private GoogleMap mMap;
	
	private LocationManager lManager;
	
	private RouteHandler routeHandler;
	
	private Driver currentDriver;
	
	private View offerLayout;
	
	private View additionalDriverInfoView;
	
	/**
	 * ������ � ����������� ��������
	 */
	private TextView driverNameTextView;
	private RatingBar driverRatingBar;
	private TextView driverRatingCountTextView;
	private TextView driverAboutTextView;
	private TextView vehicleBrandTextView;
	private TextView vehicleModelTextView;
	private TextView vehicleMaxPassengersTextView;
	private TextView vehicleYearTextView;
	private TextView driverCommentTextView;
	private ImageView imageFace;
	private ImageView imageCar;
	
	private ImageButton buttonInfoDriver;
	private ImageButton buttonPhoneDriver;
	
	private Button buttonAcceptDriver;
	private Button buttonDeclineDriver;
	
	/**
	 * ���� � �������� ��������/������/����
	 */
	private TableRow offerButtonsLayout;
	
	
    private boolean create_order = false;        
    private boolean show_phone = false;
    private boolean demonstration = false;	
    
	/**
	 * ��������� �������� ��������
	 */
    private ArrayList<LatLng> route;
    
    private DBAdapter db;
    
    private String device_id;
    
    private boolean isRegisteredReceiver = false;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        
    	super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_passenger_search_offers);
        
        // �������� device_id: 
        device_id = Secure.getString(getApplicationContext().getContentResolver(), Secure.ANDROID_ID);
        
        // ���������
        TextView title = (TextView) this.findViewById(R.id.header_title);
        title.setText( getString(R.string.title_screen_passenger_preferences));
        
        // ������ "�����"
        Button backButton = (Button)this.findViewById(R.id.header_backbutton);
        backButton.setOnClickListener(new OnClickListener(){
 			@Override
 			public void onClick(View v) {
 				finish(); 
 			}
        });
        
        // ������ "�������� �����"
        Button cancelOrderButton = (Button)this.findViewById(R.id.header_completebutton);
        cancelOrderButton.setText(this.getString(R.string.button_screen_passenger_search_offers_cancel_order));
        cancelOrderButton.setVisibility(View.VISIBLE);
        cancelOrderButton.setOnClickListener(new OnClickListener(){
 			@Override
 			public void onClick(View v) {
 				closeCurrentOrder(); 
 			}
        });        
        
        
        
        lManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        
        // �������� ���� ������
        db = new DBAdapter(this);
        
        // ��� �������� ������� �� ����� �����
        getWindow().addFlags(LayoutParams.FLAG_KEEP_SCREEN_ON);
        
        long orderId = -1;        
        long rowId = -1;
        
        
        Bundle extras = getIntent().getExtras(); 
        if( extras != null ){
	        
        	if( extras.containsKey("order_id") ){
        		orderId = extras.getLong("order_id");
        	}
        	
        	if( extras.containsKey("row_id") ){
	        	rowId = extras.getLong("row_id");
	        }
        	
        	if( extras.containsKey("create_order") ){
				create_order = extras.getBoolean("create_order"); 
			}
	        if( extras.containsKey("phonenumber") ){
	        	show_phone = extras.getBoolean("phonenumber");
	        }
			if( extras.containsKey("demonstration") ){
	        	demonstration = extras.getBoolean("demonstration");
			} 
			
        }
        
        Log.d(TAG, "rowID = " + rowId + ", orderID = " + orderId);
        if( loadOrder( rowId, orderId ) ) {
            // ������� ����� ��� �����
            setUpMapIfNeeded();
        }
        
        //
        // ������� �����, ���� ������ � ������� order_id
        //
        if( create_order || show_phone || demonstration ){
            // �������� ������� ����� �� ��
        	new CreateOrderHandler(this, rowId).sendOrder( currentOrder, device_id );
        }
        
    }
    
    private boolean loadOrder( long row_id, long order_id ){
    	if( order_id > -1 ){
            try{
            	db.open();
            	currentOrder = db.getOrderByOrderId(order_id);
            } catch(Error e){
            	e.printStackTrace();
            	Log.e(TAG, e.getLocalizedMessage());
            	return false;
            } finally{
            	db.close();
            }    		
    		return true;
    	} else if ( row_id > -1 ) {
            try{
            	db.open();
            	currentOrder = db.getOrderByRowId(row_id);
            } catch(Error e){
            	e.printStackTrace();
            	Log.e(TAG, e.getLocalizedMessage());
            	return false;
            } finally{
            	db.close();
            }    		
    		return true;
    	} else {
    		return false;
    	}
    }
    
    

    
    /**
     * ������������� �����
     */
    private void setUpMapIfNeeded() {
    	
        // �������������� ����� ��� �������������
    	if (mMap == null) { 
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.screen_passenger_search_offers_mapview)).getMap();
        }
    	
        // ��� ���������, ����� �� ��������� ��� �������, �������� ���� ��������� ��������� �������
        if( lManager != null ){
            Location l = null;
            List<String> providers = lManager.getProviders(true);
            for (int i=providers.size()-1; i>=0; i--) {
                l = lManager.getLastKnownLocation(providers.get(i));
                if (l != null) break;
            }
            if (l != null) {
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(l.getLatitude(), l.getLongitude()), GeoUtility.zoomLevel(AppSettings.DEFAULT_RADIUS_FOR_DRIVER_MAP*2)));
            }            
        }    	

        offerLayout = (View) findViewById(R.id.screen_passenger_search_offers_offer_layout);
        offerButtonsLayout = (TableRow) findViewById(R.id.screen_passenger_search_offers_offer_layout_buttons);
        
        driverCommentTextView = (TextView) findViewById(R.id.screen_passenger_search_offers_offer_layout_driver_comment);
        driverCommentTextView.setVisibility(View.GONE);
        
        driverNameTextView = (TextView) findViewById(R.id.screen_passenger_search_offers_driver_nickname);
        driverRatingBar = (RatingBar) findViewById(R.id.screen_passenger_search_offers_driver_rating);
        driverRatingCountTextView = (TextView) findViewById(R.id.screen_passenger_search_offers_driver_rating_count);
        driverAboutTextView = (TextView) findViewById(R.id.screen_passenger_search_offers_driver_about);
    	vehicleBrandTextView = (TextView) findViewById(R.id.screen_passenger_search_offers_vehicle_brand);
    	vehicleModelTextView = (TextView) findViewById(R.id.screen_passenger_search_offers_vehicle_model);
        
    	buttonInfoDriver = (ImageButton) findViewById(R.id.screen_passenger_search_offers_info_driver_button);
    	
    	buttonPhoneDriver = (ImageButton) findViewById(R.id.screen_passenger_search_offers_phone_driver_button);
    	buttonPhoneDriver.setVisibility(View.GONE);    	
    	
    	buttonAcceptDriver = (Button) findViewById(R.id.screen_passenger_search_offers_accept_driver_button);
    	buttonDeclineDriver = (Button)findViewById(R.id.screen_passenger_search_offers_decline_driver_button);
    	
    	additionalDriverInfoView = (View) findViewById(R.id.screen_passenger_search_offers_additional_driver_info_view);
    	additionalDriverInfoView.setVisibility(View.GONE);
    	
    	vehicleYearTextView = (TextView) findViewById(R.id.screen_passenger_search_offers_vehicle_year);
    	vehicleMaxPassengersTextView = (TextView) findViewById(R.id.screen_passenger_search_offers_vehicle_max_passengers);
    	
        imageFace = (ImageView) findViewById(R.id.screen_passenger_search_offers_image1);
        imageCar = (ImageView) findViewById(R.id.screen_passenger_search_offers_image2);
        
        if( currentOrder != null ){
        
			calculateRoute(
					  currentOrder.getStartLatitude()
					, currentOrder.getStartLongitude()
					, currentOrder.getEndLatitude()
					, currentOrder.getEndLongitude()
					, AppSettings.FINE_ROUTE
			);        
        
		    if( currentOrder.getId() > 0 ){

			    // ���� ���� �������������� � ���������, ���������� ��� ����
		    	if( currentOrder.getCurrentStatus() == AppSettings.ORDER_STATUS_PASSENGER_ACCEPT_DRIVER ){
		        	offerLayout.setVisibility(View.VISIBLE);
		        	driverCommentTextView.setVisibility(View.VISIBLE);
			    	buttonAcceptDriver.setVisibility(View.INVISIBLE);		        	
		        	additionalDriverInfoView.setVisibility(View.GONE);
		        	String driverParams = currentOrder.getCurrentDriverParams();
		        	Log.d(TAG, driverParams);
			    }
				
				if( currentOrder.getCurrentStatus() == AppSettings.ORDER_STATUS_OPEN ){
					// ���� ������ �������� - ����������� ����������� ���������
					getDriverList( currentOrder.getId() );
				} else if( currentOrder.getCurrentStatus() > AppSettings.ORDER_STATUS_OPEN ) {
					// ����������� ������ �������� ��������
					getDriverInfo( currentOrder.getCurrentDriverId() );
				}
				 
			    switch( currentOrder.getCurrentStatus() ){
			    	case AppSettings.ORDER_STATUS_DRIVER_DECLINE_ORDER :
			    	    // � ������� ������ �������� ��������� �� ������
				    	offerLayout.setVisibility(View.INVISIBLE);
			        	offerButtonsLayout.setVisibility(View.GONE);
			        	driverCommentTextView.setVisibility(View.VISIBLE);
			    		driverCommentTextView.setText(getString(R.string.text_screen_passenger_search_offers_cancel_order));
				    	showAlertDriverDeclineOrder(currentOrder.getId());			    		
			    		break;
				    case AppSettings.ORDER_STATUS_DRIVER_SAY_DELAY:
				    	// � ������� ������ �������� ������ ��� �������������
				    	offerLayout.setVisibility(View.VISIBLE);
				    	buttonAcceptDriver.setVisibility(View.INVISIBLE);
				    	driverCommentTextView.setVisibility(View.VISIBLE);
				    	Log.d(TAG, currentOrder.getLastComment());
				    	driverCommentTextView.setText( currentOrder.getLastComment() );
				    	break; 
				    case AppSettings.ORDER_STATUS_DRIVER_SAY_ARRIVE:
				    	// ���� � ������� ������ �������� ������ ��� �������
				    	offerLayout.setVisibility(View.VISIBLE);
				    	buttonAcceptDriver.setVisibility(View.INVISIBLE);
				    	driverCommentTextView.setVisibility(View.VISIBLE);
				    	driverCommentTextView.setText(getString(R.string.text_screen_passenger_search_offers_driver_arrive));
				    	break;
			    }			
		    }
        }    
    }   
    
    public void button_screen_passenger_search_offers_back_click(View v){
    	finish();
    }
    
    /**
     * ������ �������� �����
     */
    public void closeCurrentOrder(){
    	if( currentDriver != null ){
    		new SetOrderStatusHandler(this).setStatus(currentOrder.getId(), currentDriver, AppSettings.ORDER_STATUS_CLOSE, "");
    	}
    	closeOrder( currentOrder );
    }
    
	@Override 
	public void onResume() {
		super.onResume();
		setUpMapIfNeeded();
		Log.d(TAG, "onResume");
		
		servicePassengerConnection = new ServiceConnection() {
		    public void onServiceConnected(ComponentName name, IBinder service) {
		        Log.d(TAG, "onServiceConnected");
		    	ServicePassengerBinder servicePassengerBinder = (ServicePassengerBinder) service;
		        servicePassenger = servicePassengerBinder.getService();
		        isBoundPassengerServer = true;
		    }
		    public void onServiceDisconnected(ComponentName name) {
		    	Log.d(TAG, "onServiceDisconnected");
		        isBoundPassengerServer = false;
		      }
	    };		
		
		if( !isBoundPassengerServer ){ 
			Log.d(TAG, "bindService");
			bindService(new Intent(this, TaxitorgServicePassenger.class), servicePassengerConnection, Context.BIND_NOT_FOREGROUND);
		}
		if( !isRegisteredReceiver ){
			registerReceiver(broadcastReceiver, new IntentFilter(AppSettings.BROADCAST_ACTION_PASSENGER));
			isRegisteredReceiver = true;
		}
	}    
	
    @Override
    protected void onPause(){ 
    	Log.d( TAG, "onPause activity " + TAG );
    	if( popUpDrivers != null && popUpDrivers.isShowing() ){
    		popUpDrivers.dismiss(); 
    	}
    	
    	if( isBoundPassengerServer ){
    		isBoundPassengerServer = false;
       		
    		unbindService(servicePassengerConnection);
    	}
   		
    	if( isRegisteredReceiver ){
    		unregisterReceiver(broadcastReceiver);
    		isRegisteredReceiver = false;
    	}
    	
    	super.onPause();
    }
	
	private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String task = intent.getStringExtra("task");
			String orderId = intent.getStringExtra("order_id");
			String results = intent.getStringExtra("results");
			if( task.equals( AppSettings.REQUEST_GET_DRIVER_LIST ) ){
				parseDriverOffers(orderId, results);
				showNextDriverOffer();
			}
			
			if( task.equals( AppSettings.REQUEST_CHECK_ORDER_STATUS ) ){
				String comment = intent.getStringExtra("comment");
				post_check_order_status( orderId, results, comment );
			}
			
		}
	};	
	
    
    /**
     * �������� ��������, ����������� �� ������
     * @param v
     */
    public void screen_passenger_search_offers_decline_driver_button_click(View v){
		new SetOrderStatusHandler(this).setStatus(currentOrder.getId(), currentDriver, AppSettings.ORDER_STATUS_PASSENGER_DECLINE_DRIVER, "");
    }
    
    /**
     * ������� ��������, ����������� �� ������
     * @param v
     */
    public void screen_passenger_search_offers_accept_driver_button_click(View v){
		new SetOrderStatusHandler(this).setStatus(currentOrder.getId(), currentDriver, AppSettings.ORDER_STATUS_PASSENGER_ACCEPT_DRIVER, "");
    }

    /**
     * �������� ���������� � ��������
     */
    public void screen_passenger_search_offers_info_driver_button_click(View v){
    	if( additionalDriverInfoView.getVisibility() == View.GONE ){
    		additionalDriverInfoView.setVisibility(View.VISIBLE);
    	} else {
    		additionalDriverInfoView.setVisibility(View.GONE);
    	}
    }
    
    /**
     * ��������� ���������� ��������
     * ���� ���� ��� ���� � ����� ����������, ����� �� ������
     * ����� ���������� ������� ��������
     * @param driver_id
     */
    private void loadDriverPhotos( long driver_id ){
    	String path = Environment.getExternalStorageDirectory() + AppSettings.DIR_TAXITORG;
		
		if( isDriverPhotoDownloaded( driver_id, AppSettings.POSTFIX_IMAGE_FACE ) ) {
	        BitmapFactory.Options options = new BitmapFactory.Options();
	        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
	        Bitmap bitmapFace = BitmapFactory.decodeFile(path + driver_id + AppSettings.POSTFIX_IMAGE_FACE + AppSettings.IMAGES_EXTENSION, options);
	        imageFace.setImageBitmap(bitmapFace);
		} else {
			new DownloadImageHandler(this).getImage( driver_id, AppSettings.POSTFIX_IMAGE_FACE, path );
		}
		if( isDriverPhotoDownloaded( driver_id, AppSettings.POSTFIX_IMAGE_CAR ) ) {
	        BitmapFactory.Options options = new BitmapFactory.Options();
	        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
	        Bitmap bitmapCar = BitmapFactory.decodeFile(path + driver_id + AppSettings.POSTFIX_IMAGE_CAR + AppSettings.IMAGES_EXTENSION, options);
	        imageCar.setImageBitmap(bitmapCar);
		} else {
			new DownloadImageHandler(this).getImage( driver_id, AppSettings.POSTFIX_IMAGE_CAR, path );
		}    	
    }
    
    
    private void deleteDriverFromOffersList( long driverId ){
    	ArrayList<DriverOffer> newDriverOffers = new ArrayList<DriverOffer>();
		if( driverOffers != null ){
        	for( DriverOffer driverOffer : driverOffers ){
    			if( driverOffer.driver.getId() != driverId ){
    				newDriverOffers.add(driverOffer);
    			}
    		}
		}
		// ��������� ������ ���������    		
		driverOffers = new ArrayList<DriverOffer>();
		driverOffers = newDriverOffers;
    }
    
    private void post_check_order_status( String _order_id, String _status, String _comment ){

		long order_id = Long.parseLong(_order_id);
		int status = Integer.parseInt(_status);
		
	    // TODO: ����� ���� � ����������� �� �������������� ������, ������ ������������ ������
		
    	switch( status ){
    		case AppSettings.ORDER_STATUS_DRIVER_DECLINE_ORDER :
				// �������� �������� ������
    			showAlertDriverDeclineOrder(order_id);
				break;
			case AppSettings.ORDER_STATUS_DRIVER_SAY_DELAY :
				// �������� ������ ��� ������������� 
				Log.d(TAG, "ORDER_STATUS_DRIVER_SAY_DELAY");				
				driverCommentTextView.setText("�������� �������������");
				break;
				
			case AppSettings.ORDER_STATUS_DRIVER_SAY_ARRIVE :
				// �������� ������ ��� �������
				Log.d(TAG, "ORDER_STATUS_DRIVER_SAY_ARRIVE");
				driverCommentTextView.setText("�������� �������");
				break;
    	}
    }
    
    /**
     * �������� ���������� ���� "�������� ��������� �� ������. ��������� ����� �����������"
     */
    private void showAlertDriverDeclineOrder(long _order_id){
    	
    	final long order_id = _order_id;
    	
    	AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    builder.setMessage( getString(R.string.screen_passenger_search_offers_dialog_driver_cancel_offer) )
	        .setCancelable(false)
	        .setPositiveButton( getString(R.string.button_ok), new DialogInterface.OnClickListener() {
	            public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
	         	   offerLayout.setVisibility(View.GONE);
	         	   servicePassenger.setStatus(order_id, AppSettings.ORDER_STATUS_OPEN, 0, "");
	         	   dialog.dismiss();
	            }
	        });
	    
	    AlertDialog alert = builder.create();
	    alert.show();    	
    }
    
    /**
     * �������� ���� �� ������� ���������
     * @param drivers
     */
    private void parseDriverOffers(String orderId, String drivers){ 
    	
    	Log.d(TAG, "### parseDriverOffers() ###");
    	Log.d(TAG, drivers); 
    	
		long order_id = Long.parseLong(orderId);

    	driverOffers = new ArrayList<DriverOffer>();
		
		if( currentOrder.getId() == order_id ){
	    	// ��������� ����������� �� ���������
			JSONArray driversArray = new JSONArray();
			try {
				driversArray = new JSONArray(drivers);
			} catch (JSONException e) {
				Log.e(TAG, e.getLocalizedMessage());
				e.printStackTrace();
			}				
			
			Log.d(TAG, "drivers.length()="+driversArray.length());
			for (int i = 0; i < driversArray.length(); i++) {
				DriverOffer driverOffer = null;
				try{
					driverOffer = new DriverOffer(driversArray.getJSONArray(i));
				} catch (JSONException e){
					Log.e(TAG, e.getLocalizedMessage());
					e.printStackTrace();
				}
				if( driverOffer != null ){
					driverOffers.add( driverOffer );
				}
			}
		}
    }
    
    /**
     * �������� ��������� �����
     */
    private void showNextDriverOffer(){ 
    	Log.d(TAG, "### showNextDriverOffer() ###");
    	if( driverOffers != null && driverOffers.size()>0 ){
    		DriverOffer offer = driverOffers.get(0);
    		currentDriver = offer.driver;
    		
    		if( !offer.getComment().equals("") ){
    			driverCommentTextView.setText( offer.getComment() );
    			driverCommentTextView.setVisibility(View.VISIBLE);
    		}
    		drawDriverInfo( currentDriver );
        	offerLayout.setVisibility(View.VISIBLE);
        	offerButtonsLayout.setVisibility(View.VISIBLE);
    	} else {
    		offerLayout.setVisibility(View.GONE);
    	}
    }
    
    private void drawDriverInfo( Driver driver ){
        driverNameTextView.setText( driver.getNickname() );
    	vehicleYearTextView.setText( String.valueOf( driver.getYear() ) );
    	vehicleMaxPassengersTextView.setText( String.valueOf( driver.getMaxPassenger() ) );
        vehicleBrandTextView.setText( driver.getBrand() );  
    	vehicleModelTextView.setText( driver.getModel() );
    	driverRatingBar.setRating( (float) driver.getRating() );
    	driverRatingCountTextView.setText( new StringBuilder().append(" (").append( driver.getRatingCount() ).append(") ").toString());
		driverAboutTextView.setText( driver.getAnnotation() );
    	loadDriverPhotos( driver.getId() );
    	 
    	final String phonenumber = driver.getPhone();
    	Log.d(TAG, phonenumber);
    	if( driver.isShowPhone() && !phonenumber.equals("") ){
    		buttonPhoneDriver.setVisibility(View.VISIBLE);
    		buttonPhoneDriver.setOnClickListener( new OnClickListener(){
    			@Override
				public void onClick(View v) {
    				Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+phonenumber));
    				startActivity(intent);	    				
				}
    		});
    	}
    	
    	buttonAcceptDriver.setEnabled( true );
    	buttonDeclineDriver.setEnabled( true );
    	
    }
    

    
    /**
     * ������ ������ � ����������� �� mapView
     * @param latStart
     * @param lonStart
     * @param latEnd
     * @param lonEnd
     * @param accuracy
     */
    private void calculateRoute( double latStart, double lonStart, double latEnd, double lonEnd, int accuracy  ){
	    Log.d(TAG, "calculateRoute:" + latStart + "x" + lonStart + " " + latEnd + "x" + lonEnd);
    	routeHandler = new RouteHandler( this );
	    routeHandler.calculateRoute(latStart, lonStart, latEnd, lonEnd, accuracy);
    }    
    
    
	@Override
	public void onRouteCalcBegin() {
		// TODO �������� �����������
		
	}    
	
	
	private void drawRoute(){
		mMap.clear();
		mMap.addPolyline((new PolylineOptions().color(Color.BLUE).width(10)).addAll(route));		
	}
	
	@Override
	public void onBoundsCalculated(LatLng southwest, LatLng northeast) {
		LatLngBounds bounds = new LatLngBounds.Builder().include(southwest).include(northeast).build(); 
		try{
			mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 50));
		} catch( Exception e ){
			Log.e(TAG, e.getLocalizedMessage());
			e.printStackTrace();
		}
	}

	@Override
	public void onRouteCalcError(String error_msg) {
		new ErrorAlert(this).showErrorDialog(this.getString(R.string.offline_title), error_msg);
	}	
	
	@Override
	public void onRouteCompleted(ArrayList<LatLng> r, long distance,
			long duration) {
		route = r;
		/* TODO: ��������� ��������� � ����. AppSettings.currentOrder.setRoute(route); */
		drawRoute();
	}
	
	// ===============================================================
	//                      Create order
	// ===============================================================

	
	/**
	 * ������ ���������� ������
	 */
	@Override
	public void OnCreateOrderError(String errorMsg) {
		new ErrorAlert(this).showErrorDialog(this.getString(R.string.offline_title), this.getString(R.string.offline_description));
	}

    /**
     * ������ ����� �� ������� � ���������� ������
     * @param _orderId - ������������� ������
     */
	@Override
	public void onCreateOrderComplete(long _orderId, long row_id) {
    	Log.d(TAG, "post_send_order, OrderId=" + _orderId);
		
    	currentOrder.setId(_orderId);
    	
    	// ��������� ������������� � ���� ������
    	storeOrderId(row_id, _orderId);
    	
    	// �������� � ������ ����� �����
    	ActiveOrder order = new ActiveOrder(_orderId, AppSettings.ORDER_STATUS_OPEN, currentOrder.getAddressStart(), currentOrder.getAddressEnd(), currentOrder.getVoyageTime(), 0);
    	servicePassenger.addOrder( order );
    	
    	// ������ "���� ������ �������"
		AlertDialog.Builder dialog = new AlertDialog.Builder(this);
		dialog.setTitle( getString( R.string.screen_passenger_search_offers_create_order_dialog_title ) );
		dialog.setMessage( getString( R.string.screen_passenger_search_offers_create_order_dialog_description ) );
		// ������ ��� ������
		dialog.setPositiveButton( getString(R.string.screen_passenger_search_offers_create_order_dialog_close_button), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
			    Intent intent = new Intent();
			    intent.setClass(ScreenSearchOffers.this, ScreenPassengerMain.class);
			    startActivity(intent);
				finish();
			}
		});
		dialog.setNegativeButton(getString(R.string.button_cancel), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
			}
		});
		dialog.setCancelable(true);
		dialog.show();    	    	
	}
	
	private void storeOrderId( long row_id, long order_id ){
		
		try{
			db.open();
			db.updateOrderId(row_id, order_id);
			db.close();
		} catch( Error e ){
			Toast toast = Toast.makeText(this, e.toString(), Toast.LENGTH_LONG);
			toast.show(); 
		}
	}
	
	@Override
	public void onDownloadImageComplete(int imageId, Bitmap data) {
		Log.d(TAG, "onDownloadImageComplete() " + imageId);
		switch( imageId ){
		   case AppSettings.POSTFIX_IMAGE_FACE:
			   imageFace.setImageBitmap(data);
			   break;
		   case AppSettings.POSTFIX_IMAGE_CAR:
			   imageCar.setImageBitmap(data);
			   break;
		}
	}

	@Override
	public void onDownloadImageError(String error_msg) {
		// TODO Auto-generated method stub
		
	}
	
	// ======================================================
	// GetDriverList
	// ======================================================
	
	private void getDriverList( long orderId ){
		new GetDriverListHandler(this).GetDriverList( orderId );
	}
	
	@Override
	public void OnCompleteGetDriverList(long orderId, JSONArray list) {
		Log.d(TAG, "### OnCompleteGetDriverList() ###");
		parseDriverOffers(String.valueOf(orderId), list.toString());
		showNextDriverOffer();
	}

	@Override
	public void OnErrorGetDriverList(long orderId, String error_msg) {
		Log.e(TAG, "OrderID = " + orderId + ". " + error_msg);
	}

	
	
	// ======================================================
	//
	// ======================================================	
	
	@Override
	public void OnSetOrderStatusComplete(int order_status, Driver driver, long order_id, String comment) {
    	Log.i(TAG, "post_set_order_status, DriverId=" + driver.getId() + ", order_status="+order_status + ", order_id="+order_id );
    	
    	// �������� ������� �� ��������� ���������
    	servicePassenger.setStatus(order_id, order_status, driver.getId(), comment);
    	 
    	// ����� � �������� ����������� ��������
    	if( order_status == AppSettings.ORDER_STATUS_PASSENGER_ACCEPT_DRIVER ){
    		currentOrder.setCurrentDriverId(driver.getId(), driver.getParams());
    		// ��������� �������� � ����
    		try{
    			db.open();
    			db.setDriverId(order_id, driver.getId(), driver.getParams());
    		} catch ( Error e ) {
    			e.printStackTrace();
    		} finally {
    			db.close();
    		}
        	// �������� ����� ���������� ��������
        	offerLayout.setVisibility(View.VISIBLE);
        	buttonAcceptDriver.setVisibility(View.INVISIBLE);        	
    	}
    	
    	// ����� �� ������ �� ����������� ��������
    	if( order_status == AppSettings.ORDER_STATUS_PASSENGER_DECLINE_DRIVER ){
        	offerLayout.setVisibility(View.GONE);
        	offerButtonsLayout.setVisibility(View.GONE);    		
    		deleteDriverFromOffersList(driver.getId());
    		showNextDriverOffer();
    	}
    	
    	// ������ ����� � �������� ������
    	if( order_status == AppSettings.ORDER_STATUS_CLOSE ){
    		closeOrder(currentOrder);
    	}
    	
	}

	@Override
	public void OnSetOrderStatusError(long order_id, String error_msg) {
		if( currentOrder.getId() == order_id ){
			new ErrorAlert(this).showErrorDialog("Error", error_msg); 
		}
	}

	// ======================================================
	// ��������� ���������� � ��������
	// ======================================================		
	
	private void getDriverInfo( long driverId ){
    	buttonAcceptDriver.setEnabled( false );
    	buttonDeclineDriver.setEnabled( false );
		new GetDriverInfoHandler(this).getDriverInfo( driverId );
	}
	
    private boolean isDriverPhotoDownloaded( long driver_id, int type ){
    	String path = Environment.getExternalStorageDirectory() + AppSettings.DIR_TAXITORG;
    	String filename = String.valueOf(driver_id) + String.valueOf(type) + AppSettings.IMAGES_EXTENSION;
    	Log.d(TAG, path + filename);
    	
    	File file =  new File( path, filename );
    	return file.exists();
    }			

	@Override
	public void OnBeginGetDriverInfo(long driver_id) {
		loadDriverPhotos( driver_id ); 
	}
    
	@Override 
	public void OnEndGetDriverInfo(Driver driver) {
		currentDriver = driver;
		if (currentOrder.getCurrentDriverId() == driver.getId()){
	        drawDriverInfo( driver );
		}
	}

	@Override
	public void OnErrorGetDriverInfo(String errorMsg) {
		// TODO Auto-generated method stub
		
	}
	
	// =============================================================
	//  Cancel order
	// =============================================================

    private void closeOrder( Order order ){
    	new CancelOrderHandler(this).cancelOrder(order);
    }	
	
	@Override
	public void onCancelOrderCompleted(Order order) {
    	
		// ��������� ����� � ���� ������
    	try{
        	db.open();
        	db.closeOrder(order.getId());
        } catch(Error e){
        	e.printStackTrace();
        	Log.e(TAG, e.getLocalizedMessage());
        } finally{
        	db.close();
        }
        
        // ������� ����� � �������
    	servicePassenger.deleteOrder( order.getId() );

    	// ��������� ������������� �������
    	if( isBoundPassengerServer ){
    		isBoundPassengerServer = false;
    		unbindService(servicePassengerConnection); 
    	}    	
    	
    	if( isRegisteredReceiver ){
    		unregisterReceiver(broadcastReceiver);
    		isRegisteredReceiver = false;
    	}
    	
    	// ��������� ��������
    	finish();
    	
	}

	@Override
	public void onCancelOrderError(String error_msg) {
		// TODO Auto-generated method stub
		
	}
	
}
