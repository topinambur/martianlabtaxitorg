package com.martianlab.taxitorg.common;

import java.util.ArrayList;

import com.martianlab.taxitorg.AppSettings;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class DBAdapter {
    
	private final static String TAG = "DBAdapter";
	
	private Context context;
	
	private static final String DATABASE_TABLE_ADDRESSES = "addresses";
	private static final String DATABASE_TABLE_PASSENGER_ORDERS = "orders";
	
    public static final String KEY_ADDRESSES_ROWID = "_id";
    public static final String KEY_ADDRESSES_ADDRESS = "address";
    public static final String KEY_ADDRESSES_LATITUDE = "latitude";
    public static final String KEY_ADDRESSES_LONGITUDE = "longitude";
    public static final String KEY_ADDRESSES_FRECUENCY = "frequency";

    public static final String KEY_PASSENGER_ORDERS_ROWID = "_id";
    public static final String KEY_PASSENGER_ORDERS_ORDERID = "order_id";
    public static final String KEY_PASSENGER_ORDERS_ADDRESS_START = "address_start";
    public static final String KEY_PASSENGER_ORDERS_ADDRESS_END = "address_end";
    public static final String KEY_PASSENGER_ORDERS_START_LAT = "start_lat";
    public static final String KEY_PASSENGER_ORDERS_START_LON = "start_lon";
    public static final String KEY_PASSENGER_ORDERS_END_LAT = "end_lat";
    public static final String KEY_PASSENGER_ORDERS_END_LON = "end_lon";
    public static final String KEY_PASSENGER_ORDERS_PRICE = "price";
    public static final String KEY_PASSENGER_ORDERS_CURRENCY = "currency";
    public static final String KEY_PASSENGER_ORDERS_PARAMS = "params";
    public static final String KEY_PASSENGER_ORDERS_DRIVERPARAMS = "driver_params";
    public static final String KEY_PASSENGER_ORDERS_VOYAGETIME = "voyage_time";
    public static final String KEY_PASSENGER_ORDERS_DRIVERID = "driver_id";
    public static final String KEY_PASSENGER_ORDERS_CURRENT_STATUS = "current_status";
    public static final String KEY_PASSENGER_ORDERS_STATUS = "status";
	public static final String KEY_PASSENGER_ORDERS_COMMENT = "comment";
	public static final String KEY_PASSENGER_ORDERS_DRIVER_RATED = "driver_rated";
    
	private DBHelper databaseHelper;
	private SQLiteDatabase db;
	
	public DBAdapter(Context ctx){
		this.context = ctx;
		
	}
	
    //---opens the database---
    public DBAdapter open() throws SQLException {
    	if( databaseHelper == null ) {
    		databaseHelper = new DBHelper(context);
    	}

    	try{
    		// A reference to the database can be obtained after initialization.
    		db = databaseHelper.getWritableDatabase();
    	} catch( Exception ex ){
        	ex.printStackTrace();
        }
        return this;
    }	
	
    //---closes the database---    
    public void close() {
    	if(databaseHelper != null) {
    		databaseHelper.close();
    		databaseHelper = null;
    	}
    }    
    
    public Order getOrderByOrderId( long order_id){
    	String query = 
  		  " SELECT * "
  		+ " FROM " + DATABASE_TABLE_PASSENGER_ORDERS
  		+ " WHERE " + KEY_PASSENGER_ORDERS_ORDERID + " = " + order_id;
    	
    	Cursor c = db.rawQuery(query, null);
    	
    	Order res = null;
    	if( c.moveToFirst() ) {
    		res = loadOrder(c);
    	}
    	
    	c.close();
    	
    	return res;
    }

    public Order getOrderByRowId( long row_id){
    	String query = 
  		  " SELECT * "
  		+ " FROM " + DATABASE_TABLE_PASSENGER_ORDERS
  		+ " WHERE " + KEY_PASSENGER_ORDERS_ROWID + " = " + row_id;
    	
    	Cursor c = db.rawQuery(query, null);
    	
    	Order res = null;
    	if( c.moveToFirst() ) {
    		res = loadOrder(c);
    	}
    	
    	c.close();
    	
    	return res;
    }    
    
    
    private Order loadOrder( Cursor c ){
    	Order res = new Order();
		long id = c.getLong( c.getColumnIndex( KEY_PASSENGER_ORDERS_ORDERID ) );
		long driver_id = c.getLong( c.getColumnIndex( KEY_PASSENGER_ORDERS_DRIVERID ) );
		double start_lat = c.getDouble( c.getColumnIndex( KEY_PASSENGER_ORDERS_START_LAT ) ); 
		double start_lon = c.getDouble( c.getColumnIndex( KEY_PASSENGER_ORDERS_START_LON ) );
		double end_lat = c.getDouble( c.getColumnIndex( KEY_PASSENGER_ORDERS_END_LAT ) ); 
		double end_lon = c.getDouble( c.getColumnIndex( KEY_PASSENGER_ORDERS_END_LON ) );
		String address_start = c.getString( c.getColumnIndex( KEY_PASSENGER_ORDERS_ADDRESS_START ) );
		String address_end = c.getString( c.getColumnIndex( KEY_PASSENGER_ORDERS_ADDRESS_END ) );
		String params = c.getString( c.getColumnIndex( KEY_PASSENGER_ORDERS_PARAMS ) );
		float price = c.getFloat( c.getColumnIndex( KEY_PASSENGER_ORDERS_PRICE ) );
		String currency = c.getString( c.getColumnIndex( KEY_PASSENGER_ORDERS_CURRENCY ) );
		long voyageTime = c.getLong( c.getColumnIndex( KEY_PASSENGER_ORDERS_VOYAGETIME ) );
		int status = c.getInt( c.getColumnIndex( KEY_PASSENGER_ORDERS_STATUS ) );
		int current_status = c.getInt( c.getColumnIndex( KEY_PASSENGER_ORDERS_CURRENT_STATUS ) );
		String last_comment = c.getString( c.getColumnIndex( KEY_PASSENGER_ORDERS_COMMENT ) );
		
		// set order
		res = new Order();
		res.setId(id);
		res.setCurrentDriverId(driver_id, params);
		res.setStartPoint(start_lat, start_lon);
		res.setEndPoint(end_lat, end_lon);
		res.setStartAddress(address_start);
		res.setEndAddress(address_end);
		res.setPrice(price);
		res.setCurrency(currency);
		res.setVoyageTime(voyageTime);
		res.setStatus(status);
		res.setCurrentStatus(current_status);
		res.setParams(params);
		res.setLastComment(last_comment);
		return res;
    }
    
    public boolean insert_address(String address, double latitude, double longitude){
    	
    	/**
    	 * ���� �� ������ ����� � ��
    	 */
    	String query =  
    	  " SELECT * " 
    	+ " FROM " + DATABASE_TABLE_ADDRESSES
    	+ " WHERE lower(" + KEY_ADDRESSES_ADDRESS + ") LIKE lower('" + address + "')";

    	Cursor c = db.rawQuery(query, null);
    	long addrId = -1;
    	if( c.moveToFirst() ) {
    		addrId = c.getLong( c.getColumnIndex(KEY_ADDRESSES_ROWID) );
    	}
    	c.close();
    	
    	Log.d(TAG, "addrId = " + addrId);
    	
    	if( addrId == -1 ){
	        Log.d(TAG, "table="+DATABASE_TABLE_ADDRESSES+", key="+KEY_ADDRESSES_ADDRESS );
    		ContentValues values = new ContentValues();
	        values.put(KEY_ADDRESSES_ADDRESS, address);
	        values.put(KEY_ADDRESSES_LATITUDE, latitude);
	        values.put(KEY_ADDRESSES_LONGITUDE, longitude);
	        db.insert(DATABASE_TABLE_ADDRESSES, null, values);
    	} else {
    		increaseAddressFrequency( addrId );
    	}
        return true;
    }

    /**
     * �������� ���������� ������������ ��������
     * @return
     */
    public Cursor getUnratedDriver(){
    	String query = 
    		  " SELECT * "
    		+ " FROM " + DATABASE_TABLE_PASSENGER_ORDERS
    		+ " WHERE " + KEY_PASSENGER_ORDERS_DRIVERID + " > 0"        // ���� ������������� ��������
    		+ "   AND " + KEY_PASSENGER_ORDERS_DRIVER_RATED + " = 0 "   // �������� ���� �� ������ 
    		+ "   AND " + KEY_PASSENGER_ORDERS_CURRENT_STATUS + " > 1 " // ������ ������ ���������
    		+ "   AND " + KEY_PASSENGER_ORDERS_STATUS + " = 0 ";        // ������ �������

    	return db.rawQuery(query, null);
    }
    
    /**
     * �������� �������� ��� ����������
     * @param driver_id
     */
    public void rateDriver( long driver_id, long order_id ){
       ContentValues values = new ContentValues();
  	   values.put(KEY_PASSENGER_ORDERS_DRIVER_RATED, 1);
 	   int res = db.update(DATABASE_TABLE_PASSENGER_ORDERS, values, "driver_id=? AND order_id=?", new String[]{String.valueOf(driver_id), String.valueOf(order_id)});
       Log.d(TAG, "Row affected:" + res);	     	
    }
    
    public long saveOrder( Order order ){
    	ContentValues values = new ContentValues();
        values.put(KEY_PASSENGER_ORDERS_ADDRESS_START, order.getAddressStart());
        values.put(KEY_PASSENGER_ORDERS_ADDRESS_END, order.getAddressEnd());
        values.put(KEY_PASSENGER_ORDERS_START_LAT, order.getStartLatitude());
        values.put(KEY_PASSENGER_ORDERS_START_LON, order.getStartLongitude());
        values.put(KEY_PASSENGER_ORDERS_END_LAT, order.getEndLatitude());
        values.put(KEY_PASSENGER_ORDERS_END_LON, order.getEndLongitude());
        values.put(KEY_PASSENGER_ORDERS_PRICE, order.getPrice());
        values.put(KEY_PASSENGER_ORDERS_CURRENCY, order.getCurrency());
        values.put(KEY_PASSENGER_ORDERS_PARAMS, order.getParams());
 	    long voyageTime = AppSettings.currentOrder.getVoyageTime();
	    if( voyageTime == 0 ){
		    voyageTime = System.currentTimeMillis()/1000;
	    }        
        values.put(KEY_PASSENGER_ORDERS_VOYAGETIME, voyageTime);
        values.put(KEY_PASSENGER_ORDERS_STATUS, 1);
        return db.insert(DATABASE_TABLE_PASSENGER_ORDERS, null, values);
    }
    
    /**
     * �������� ���������� �������� ���������������� �������
     * @return
     */
    public int getOrdersCount(){
    	String query = 
    		  " SELECT COUNT(*) "
    		+ " FROM " + DATABASE_TABLE_PASSENGER_ORDERS
    		+ " WHERE " + KEY_PASSENGER_ORDERS_STATUS + " = 1";
    	
    	Cursor c = db.rawQuery(query, null);
    	int res = 0;
    	if( c.moveToFirst() ) {
    		res = c.getInt( 0 );
    	}
    	c.close();
    	return res;
    }
    
    /**
     * ������� ������������ �����
     * @return
     */
    public void closeOrder( long order_id ){
   	   ContentValues values = new ContentValues();
 	   values.put(KEY_PASSENGER_ORDERS_STATUS, 0);
 	   int res = db.update(DATABASE_TABLE_PASSENGER_ORDERS, values, "order_id=?", new String[]{String.valueOf(order_id)});
        Log.d(TAG, "Row affected:" + res);	     	
     	
    }

    public void setOrderStatus( long order_id, int status, String comment ){
  	   ContentValues values = new ContentValues();
 	   values.put(KEY_PASSENGER_ORDERS_CURRENT_STATUS, status);
 	   if( !comment.equals("") ){
 		  values.put(KEY_PASSENGER_ORDERS_COMMENT, comment);
 	   }
 	   int res = db.update(DATABASE_TABLE_PASSENGER_ORDERS, values, "order_id=?", new String[]{String.valueOf(order_id)});
       Log.d(TAG, "Row affected:" + res);	     	
     }    
    
    public void setDriverId( long order_id, long driver_id, String driver_params ){
   	   ContentValues values = new ContentValues();
 	   values.put(KEY_PASSENGER_ORDERS_DRIVERID, driver_id);
 	   values.put(KEY_PASSENGER_ORDERS_DRIVERPARAMS, driver_params);
 	   int res = db.update(DATABASE_TABLE_PASSENGER_ORDERS, values, "order_id=?", new String[]{String.valueOf(order_id)});
 	   Log.d(TAG, "Row affected:" + res);
    }
    
    public int getOrderStatus( long order_id ){
    	
    	int res = -1;
    	
    	String query = 
  		  " SELECT *"
  		+ " FROM " + DATABASE_TABLE_PASSENGER_ORDERS
  		+ " WHERE " + KEY_PASSENGER_ORDERS_ORDERID + " = " + order_id;
    	
    	Cursor c = db.rawQuery(query, null);
    	if( c != null ){
    		c.moveToFirst();
    		res = c.getInt(c.getColumnIndex(KEY_PASSENGER_ORDERS_CURRENT_STATUS));
    	}
    	c.close();
    	
    	return res;
    	
    }
    
    public ArrayList<ActiveOrder> getOpenPassengerOrders(){
    	
    	long currentTime = System.currentTimeMillis()/1000;
    	
    	ArrayList<ActiveOrder> res = new ArrayList<ActiveOrder>();;
    	
    	String query =
    			" SELECT * "
    	      + " FROM " + DATABASE_TABLE_PASSENGER_ORDERS
    		  + " WHERE " + KEY_PASSENGER_ORDERS_STATUS + " = 1 ";
    	
    	Log.d(TAG, query);
    	
    	Cursor c = db.rawQuery(query, null);
    	
    	if( c != null ) {
    		while( c.moveToNext() ){
    			long voyageTime = c.getLong(c.getColumnIndex(DBAdapter.KEY_PASSENGER_ORDERS_VOYAGETIME));
				long order_id = c.getLong(c.getColumnIndex(DBAdapter.KEY_PASSENGER_ORDERS_ORDERID));
				long driver_id = c.getLong(c.getColumnIndex(DBAdapter.KEY_PASSENGER_ORDERS_DRIVERID));
				String address_start = c.getString(c.getColumnIndex(DBAdapter.KEY_PASSENGER_ORDERS_ADDRESS_START));
				String address_end = c.getString(c.getColumnIndex(DBAdapter.KEY_PASSENGER_ORDERS_ADDRESS_END));
				int current_status = c.getInt(c.getColumnIndex(DBAdapter.KEY_PASSENGER_ORDERS_CURRENT_STATUS));
    			if( voyageTime + AppSettings.DEFAULT_ORDER_LIFETIME < currentTime  ){
    				Log.d(TAG, "Close order " + order_id + " by timeout");
    				closeOrder(order_id);
    			} else {
    				res.add( new ActiveOrder(order_id, current_status, address_start, address_end, voyageTime, driver_id) );
    			}
    		}
    	}
    	
    	c.close();
    	
    	return res;
    }

    /**
     * ����� ������ ������������ �� ������� �� ������ � ������������� � ������� ����������� ����������� ������ 
     * @return
     */
    public Cursor getLatestAddresses(){
		String query = 
			  " SELECT *"
			+ " FROM " + DATABASE_TABLE_ADDRESSES 
			+ " ORDER BY " + KEY_ADDRESSES_FRECUENCY + " DESC, " + KEY_ADDRESSES_ROWID + " ASC";
		
		Log.d(TAG, query);
		return db.rawQuery(query, null);
    }

    /**
     * �������� order_id, ���������� �� �������
     */
    public void updateOrderId( long _id, long order_id ){
	   ContentValues values = new ContentValues();
	   values.put(KEY_PASSENGER_ORDERS_ORDERID, order_id);
	   int res = db.update(DATABASE_TABLE_PASSENGER_ORDERS, values, "_id=?", new String[]{String.valueOf(_id)});
       Log.d(TAG, "Row affected:" + res);	 
    }


    
    public void increaseAddressFrequency( long address_id ){
    	String query = 
    		"UPDATE " + DATABASE_TABLE_ADDRESSES 
    		   + " SET " + KEY_ADDRESSES_FRECUENCY + " = " + KEY_ADDRESSES_FRECUENCY + " + 1"
     	       + " WHERE " + KEY_ADDRESSES_ROWID + " = " + address_id; 
    	Log.d(TAG, query);
    	
    	db.execSQL(query);
    }    
    
}
