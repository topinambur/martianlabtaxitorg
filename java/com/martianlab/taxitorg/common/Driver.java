package com.martianlab.taxitorg.common;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import org.json.JSONArray;
import org.json.JSONException;

import android.graphics.Bitmap;
import android.util.Log;

public class Driver {
	
	private static final String TAG = "Driver";
	
	private long id;
	private String login;
	private String password;
	private String nickname = "";

	private String brand = "";
	private String model = "";
	private int year;

	private int maxPassenger = 3;
	
	private long photo_primary;
	private long photo_secondary;
	
	private boolean isSmoke;
	private boolean isChildSeat;
	private boolean isBigBaggage;
	private boolean isConditioner;
	private boolean showPhone;
	
	private String phonenumber;
	
	private Bitmap bitmap1;
	private Bitmap bitmap2;
	
	private String annotation;
	
	private double rating;
	private long rating_count;
	
	public Driver(){
	}

	public Driver(String _login, String _password){
		this.login = _login;
		try {
			this.password = getSha256(_password);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) { 
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.phonenumber = "";
		
	}
	
	
	public Driver(String _login, String _password, String _phone){
		this.login = _login;
		try {
			this.password = getSha256(_password);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.phonenumber = _phone;
	}
	
	public Driver( JSONArray params ) throws JSONException{
		if( params != null ){
			try {
				id = params.getLong(0);
				rating = params.getInt(1);
				phonenumber = params.getString(2); 
				setParams( params.getString(3) );
				rating_count = params.getLong(4);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	public void setParams( String value ) {
		
		try {
			
			String additionalParamsStr = value.replace("\\", "");
			JSONArray additional_params = new JSONArray( additionalParamsStr );
			
			nickname = "N/A";
			if( !additional_params.isNull(0) ){
				nickname = additional_params.getString(0);
			}				
			
			brand = "N/A";
			if( !additional_params.isNull(1) ){
				brand = additional_params.getString(1);
			}				
	
			model = "N/A";
			if( !additional_params.isNull(2) ){
				model = additional_params.getString(2);
			}		
			
			year = 1970;
			if( !additional_params.isNull(3) ){
				year = additional_params.getInt(3);
			}				
	
			maxPassenger = 1;
			if( !additional_params.isNull(4) ){
				maxPassenger = additional_params.getInt(4);
			}				 
	
		    annotation = "";
			if( !additional_params.isNull(5) ){
				annotation = additional_params.getString(5);
			}
			
			showPhone = false;
			if( !additional_params.isNull(6) ){
				showPhone = Boolean.parseBoolean( additional_params.getString(6) );
			}
		} catch (JSONException e) {
			e.printStackTrace();
			Log.e(TAG, e.getLocalizedMessage());
		}
		
	}
	
	public void setId( long _id ){
		this.id = _id;
	}

	public long getId(){
		return id;
	}
	
	public void setNickname(String value){
		nickname = value;
	}
	
	public String getNickname(){
		return nickname;
	}
	
	public void setBrand(String value){
		brand = value;
	}
	
	public String getBrand(){
		return brand;
	}
	
	public void setModel(String value){
		model = value;
	}
	
	public String getModel(){
		return model;
	}	

	public void setYear( int value){
		year = value;
	}
	
	public int getYear(){
		return year;
	}
	
	public void setMaxPassenger( int value ){
		this.maxPassenger = value;
	}
	
	public int getMaxPassenger(){
		return maxPassenger;
	}
	
	public void setImage1( Bitmap bitmap ){
		this.bitmap1 = bitmap;
	}
	
	/**
	 * ����� ����������� ����������� ��������
	 * @return
	 */
	public Bitmap getImage1(){
		return this.bitmap1;
	}

	/**
	 * ����� ����������� ����������� ����������
	 * @return
	 */
	public Bitmap getImage2(){
		return this.bitmap2;
	}
	

	public void setImage2( Bitmap bitmap ){
		this.bitmap2 = bitmap;
	}
	
	
	
	public String getLogin(){
		return login;
	}
	
	public String getPassword(){
		return password;
	}
	
	public void setPhone( String value ){
		this.phonenumber = value;
	}
	
	public String getPhone(){
		return phonenumber;
	}	

	public void setAnnotation( String value){
		annotation = value;
	}
	
	
	public String getAnnotation(){
		return annotation;
	}
	
	public void setRating( double value ){
		rating = value;
	}
	
	public double getRating(){
		return rating;
	}
	
	public void setRatingCount( long value ){
		rating_count = value;
	}
	
	public long getRatingCount(){
		return rating_count;
	}
	
	public boolean isShowPhone(){
		return showPhone;
	}
	
	public void setShowPhone( boolean value ){
		showPhone = value;
	}
	
    /**
     * ��������� SHA256 �� ������ ��������
     *     author: awacs
     * @throws NoSuchAlgorithmException 
     * @throws UnsupportedEncodingException 
     */
    public String getSha256(String str) throws NoSuchAlgorithmException, UnsupportedEncodingException{
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(str.getBytes("UTF-8"));
        byte[] hash = md.digest();
        BigInteger bigInt = new BigInteger(1, hash);
        return bigInt.toString(16);
    }	
	
	public String getParams(){
		Log.d(TAG, "getParams");
		String[] data = {
				  nickname
				, brand
				, model
				, String.valueOf(year)
				, String.valueOf(maxPassenger)
				, annotation
				, String.valueOf(showPhone)
		};
		JSONArray params = new JSONArray(Arrays.asList(data));
		Log.d(TAG, params.toString());
		return params.toString();
	}
	
}
