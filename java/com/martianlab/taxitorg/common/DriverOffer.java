package com.martianlab.taxitorg.common;

import org.json.JSONArray;
import org.json.JSONException;

import android.util.Log;

/**
 * Предложение водителя
 * @author awacs
 *
 */
public class DriverOffer {
	
	private static final String TAG = "DriverOffer";
	
	public Driver driver;
	
	private int offer_status;
	private String driver_comment;
	 
	public DriverOffer( JSONArray driverOfferStr ) throws JSONException{
	    driver = new Driver();
	    driver.setId( driverOfferStr.getInt(0) );
	    driver.setRating( driverOfferStr.getInt(4) );
	    driver.setParams( driverOfferStr.getString(2) );
	    offer_status = driverOfferStr.getInt(1);    
    	driver_comment = driverOfferStr.getString(3);
    	
    	if( !driverOfferStr.isNull(5)){
    		driver.setPhone( driverOfferStr.getString(5) );
    	}
	}
	
	public int getStatus(){
		return offer_status;
	}
	
	public String getComment(){
		return driver_comment; 
	}
	
}
