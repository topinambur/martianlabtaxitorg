package com.martianlab.taxitorg.common;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import android.util.Log;

/**
 * ��������� ��� �������� ��������� ������ �������.
 * @author awacs
 *
 */

public class ActiveOrder {
	
	public static final String TAG = "ActiveOrder";
	
	public long order_id;
	public long driver_id;
	public String address_start;
	public String address_end;
	public int current_status;
	public long last_update;
	public long voyage_time;
	
	public ArrayList<DriverOffer> driverOffers; 
	
	/**
	 * ����������� ������
	 * @param _id
	 * @param _status
	 * @param _address_start
	 * @param _address_end
	 * @param _voyage_time
	 * @param _driver_id
	 */
	public ActiveOrder( long _id, int _status, String _address_start, String _address_end, long _voyage_time, long _driver_id ){
		order_id = _id;
		driver_id = _driver_id;
		current_status = _status;
		address_start = _address_start;
		address_end = _address_end;
		last_update = System.currentTimeMillis()/1000;
		voyage_time = _voyage_time;
		driverOffers = new ArrayList<DriverOffer>();
	}

	/**
	 * ��������� ����������� �� ���������
	 * @param drivers
	 */
	public int setDriverOffers(String drivers){
		driverOffers = new ArrayList<DriverOffer>(); 
    	try {
    		Log.d(TAG, drivers);
			JSONArray driversArray = new JSONArray(drivers);
			for (int i = 0; i < driversArray.length(); i++) {
				DriverOffer driverOffer = new DriverOffer(driversArray.getJSONArray(i));
				driverOffers.add( driverOffer );
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return driverOffers.size();
	}
	
	public void setCurrentStatus(int status){
		this.current_status = status;
	}
	
}
