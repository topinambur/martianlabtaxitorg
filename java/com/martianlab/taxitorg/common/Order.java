package com.martianlab.taxitorg.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONException;

import com.google.android.gms.maps.model.LatLng;

import android.util.Log;

/**
 * ����� ��� �������� �������� ������
 * @author awacs
 *
 */
public class Order implements Serializable {
	
	private final static String TAG = "Order";
	
	private long id;
	
	/**
	 * �������������� ���������� ������ ����������� � ����������
	 */
	private double latitudeStart = 0.0d;
	private double latitudeEnd = 0.0d;
	private double longitudeStart = 0.0d;
	private double longitudeEnd = 0.0d; 
	 
	/**
	 * ������������/����� ������� ���������� � �����������
	 */
	private String addressStart = "";
	private String addressEnd = ""; 
	
	/**
	 * ��������� � ���������
	 */
	private boolean meetWithNameplate = false; 
	
	/**
	 * ����� �����/������
	 */
	private String flightNumber = "";       
	
	/**
	 * ����� �� ��������
	 */
	private String namePlateText = ""; 

	/**
	 * ����� ����������
	 */
	private String vehicleClass = "";
	
	/**
	 * � ���� ������
	 */
	private boolean isPassengerSmoke = false;
	
	/**
	 * �������� ����� ������
	 */
	private boolean isDriverSmoke = false;
	
	/**
	 * ������ ���� �����������
	 */
	private boolean isConditioner = false;
		
	/**
	 * ������ ���� ������� ������
	 */
	private boolean isChildSeat = false;
	
	/**
	 * ����� �����
	 */
	private boolean isBaggage = false;
	  
	/**
	 * ������ "������� ��������"
	 */
	private boolean isSoberDriver = false;
	
	
	/**
	 * ��� ������
	 */
	private boolean isMyMusic = false;
	
	/**
	 * ��������� �������
	 */
	private float price = 0.0f;
	
	/**
	 * ������ ������
	 */
	private String currency = "eur";

	/**
	 * ��������� � ������
	 */
	private String annotation = "";
	
	/**
	 * �������������� ���� �������, 0 - ������
	 */
	private long voyageTime = 0;
	
	/**
	 * ������ ������
	 */
	private int status = 0;
	
	/**
	 * ������� ������ ������
	 */
	private int currentStatus = 0;
	
	/**
	 * ������� ������������� ��������
	 */
	private long currentDriverId;
	
	/**
	 * 
	 */
	private String currentDriverParams;
	
	/**
	 * ��������� ������� �� ������
	 */
	private String lastComment = "";
	
	
	/**
	 * ���������� ����� ���������
	 */
	private String phonenumber = "";
	
	/**
	 * 
	 */
	private boolean _isRouteCalculated = false;
	private ArrayList<LatLng> route;
	
	public long getId(){
		return id;
	}
	
	public Order(){
		this.id = 0;
		Log.d(TAG, "Create order()");
	}
	
	public Order( JSONArray params ) throws JSONException{
		Log.d(TAG, "Order( JSONArray params )");
		id = params.getLong(0);
		latitudeStart = params.getDouble(1);
		longitudeStart = params.getDouble(2);  
		latitudeEnd = params.getDouble(3); 
		longitudeEnd = params.getDouble(4);
		voyageTime = params.getLong(5);
		price = (float)params.getDouble(6);
		currency = params.getString(7); 
		
		String additionalParamsStr = params.getString(8);
		additionalParamsStr = additionalParamsStr.replace("\\", "");
		
		setAdditionalParams(additionalParamsStr );
		
		status = 0;
		if( !params.isNull(9) ){
			status = params.getInt(9);
		}
		
	} 
	
	/**
	 * ������ �������������� ���������
	 * @param additionalParamsStr
	 * @throws JSONException 
	 */
	private void setAdditionalParams( String additionalParamsStr ) throws JSONException{

		JSONArray additional_params = new JSONArray( additionalParamsStr );

		
		addressStart = "Unknown";
		if( !additional_params.isNull(0) ){
			addressStart = additional_params.getString(0);
		}
		Log.d(TAG, "addressStart " + addressStart);
		
		addressEnd = "Unknown";
		if( !additional_params.isNull(1) ){
			addressEnd = additional_params.getString(1);
		}
		Log.d(TAG, "addressEnd " + addressEnd);
		
		meetWithNameplate = false;
		if( !additional_params.isNull(2) ){
			meetWithNameplate = Boolean.parseBoolean( additional_params.getString(2) );
		}
		Log.d(TAG, "meetWithNameplate " + meetWithNameplate);
		
		flightNumber = "";
		if( !additional_params.isNull(3) ){
			flightNumber = additional_params.getString(3);
		}
		Log.d(TAG, "flightNumber " + flightNumber);
		
		namePlateText = "";
		if( !additional_params.isNull(4) ){
			namePlateText = additional_params.getString(4);
		}
		Log.d(TAG, "namePlateText " + namePlateText);
		
		vehicleClass = "";
		if( !additional_params.isNull(5) ){
			vehicleClass = additional_params.getString(5);
		}
		Log.d(TAG, "vehicleClass " + vehicleClass);
		
		isPassengerSmoke = false;
		if( !additional_params.isNull(6) ){
			isPassengerSmoke = Boolean.parseBoolean( additional_params.getString(6) );
		}
		Log.d(TAG, "isPassengerSmoke " + isPassengerSmoke);
		
		isDriverSmoke = false;
		if( !additional_params.isNull(7) ){
			isDriverSmoke = Boolean.parseBoolean( additional_params.getString(7) );
		}		
		Log.d(TAG, "isDriverSmoke " + isDriverSmoke);

		isConditioner = false;
		if( !additional_params.isNull(8) ){
			isConditioner = Boolean.parseBoolean( additional_params.getString(8) );
		}		
		Log.d(TAG, "isConditioner " + isConditioner);
		
		isChildSeat = false;
		if( !additional_params.isNull(9) ){
			isChildSeat = Boolean.parseBoolean( additional_params.getString(9) );
		}		
		Log.d(TAG, "isChildSeat " + isChildSeat);
		
		isBaggage = false;
		if( !additional_params.isNull(10) ){
			isBaggage = Boolean.parseBoolean( additional_params.getString(10) );
		}		
		Log.d(TAG, "isBaggage " + isBaggage);
		
		isSoberDriver = false;
		if( !additional_params.isNull(11) ){
			isSoberDriver = Boolean.parseBoolean( additional_params.getString(11) );
		}		
		Log.d(TAG, "isSoberDriver " + isSoberDriver);
		
		isMyMusic = false;
		if( !additional_params.isNull(12) ){
			isMyMusic = Boolean.parseBoolean( additional_params.getString(12) );
		}		
		Log.d(TAG, "isMyMusic " + isMyMusic);
		
		annotation = "";
		if( !additional_params.isNull(13) ){
			annotation = additional_params.getString(13);
		}		
		Log.d(TAG, "annotation " + annotation);

		phonenumber = "";
		if( !additional_params.isNull(14) ){
			phonenumber = additional_params.getString(14);
		}		
		Log.d(TAG, "phonenumber " + phonenumber);		
		
		if( !additional_params.isNull(15) ){
			Log.d(TAG, "undefined parameter " + additional_params.getString(14));
		}		
	}
	
	public String checkOrder(){
		if( this.addressStart.equalsIgnoreCase("")  ){
			return "addressStart";
		}
		if( this.addressEnd.equalsIgnoreCase("")  ){
			return "addressEnd";
		}
		if( this.price <= 0 ){
			return "price";
		}
		return "";
	}
	
	public void setId( long order_id ){
		id = order_id;
	}
	
	public void setStartPoint(double lat, double lon){
		latitudeStart = lat;
		longitudeStart = lon;
	}

	public void setStartAddress(String address){
		Log.d(TAG, "setStartAddress: " + address);
		addressStart = address;
	}
	
	
	public void setEndPoint(double lat, double lon){
		latitudeEnd = lat;
		longitudeEnd = lon;
	}	
	
	public double getStartLatitude(){
		return latitudeStart;
	}

	public double getStartLongitude(){
		return longitudeStart;
	}

	public double getEndLatitude(){
		return latitudeEnd;
	}

	public double getEndLongitude(){
		return longitudeEnd;
	}	
	
	
	public String getAddressStart(){
		return addressStart;
	}
	
	public String getAddressEnd(){
		return addressEnd;
	}
	
	public void setEndAddress(String address){
		Log.d(TAG, "setEndAddress: " + address);
		addressEnd = address;
	}
	
	public String getAnnotation(){
		return annotation;
	}
	
	public String getNamePlateText(){
		return this.namePlateText;
	}
	
	public boolean getMeetWithNameplate(){
		return this.meetWithNameplate;
	}
	
	public String getFlightNumber(){
		return this.flightNumber;
	}
	
	public boolean isAeroportMeeting(){
		if( meetWithNameplate ){
			return true;
		} else if( !flightNumber.equals("") ){
			return true;
		} else if( !namePlateText.equals("") ){
			return true;
		} else {
			return false;
		}
	}
	
 	
	public String getVehicleClass(){
		return vehicleClass;
	}
	
	public void setAirportPreferences(boolean _meetWithNameplate, String _flightNumber, String _namePlateText){
		meetWithNameplate = _meetWithNameplate;
		flightNumber = _flightNumber;
		namePlateText = _namePlateText;
		
		Log.d(TAG, "setAirportPreferences" + meetWithNameplate + flightNumber + namePlateText);
	}
	
	public void setTripPreferences(String _vehicleClass, boolean _isPassengerSmoke, boolean _isDriverSmoke, boolean _isConditioner, boolean _isBaggage, boolean _isChildSeat, boolean _isSoberDriver, boolean _isMyMusic, String _annotation){
		vehicleClass = _vehicleClass;
		isPassengerSmoke = _isPassengerSmoke;
		isDriverSmoke = _isDriverSmoke;
		isConditioner = _isConditioner;
		isBaggage = _isBaggage;
		isChildSeat = _isChildSeat;
		isSoberDriver = _isSoberDriver;
		isMyMusic = _isMyMusic;
		annotation = _annotation;
	}
	
	public boolean getIsPassengerSmoke(){
		return isPassengerSmoke;
	}
	public boolean getIsDriverSmoke(){
		return isDriverSmoke;
	}	
	public boolean getIsConditioner(){
		return isConditioner;
	}
	public boolean getIsBaggage(){
		return isBaggage;
	}	
	public boolean getIsChildSeat(){
		return isChildSeat;
	}	
	public boolean getIsSoberDriver(){
		return isSoberDriver;
	}
	public boolean getIsMyMusic(){
		return isMyMusic;
	}	
	
	public void setVoyageTime( long time ){
		voyageTime = time;
	}
	
	public long getVoyageTime(){
		return voyageTime;
	}
	
	public void setPrice( float _price){
		Log.d(TAG, "setPrice " + _price);
		price = _price;
	}
	
	public void setCurrency( String _currency  ){
		currency = _currency;		
	}

	public String getPrice(){
		return String.valueOf(price);
	}

	public String getCurrency(){
		return String.valueOf(currency);
	}
	
	
	/**
	 * @return
	 */
	public String getParams(){
		JSONArray params = getParamsAsJSONArray();
		return params.toString();
	}
	
	public JSONArray getParamsAsJSONArray(){
		String[] data = {
				  addressStart
				, addressEnd
				, String.valueOf(meetWithNameplate)
				, flightNumber
				, namePlateText
				, vehicleClass
				, String.valueOf(isPassengerSmoke)
				, String.valueOf(isDriverSmoke)
				, String.valueOf(isConditioner)
				, String.valueOf(isChildSeat)
				, String.valueOf(isBaggage)
				, String.valueOf(isSoberDriver)
				, String.valueOf(isMyMusic)
				, annotation
				, phonenumber
		};
		return new JSONArray(Arrays.asList(data));		
	}
	
	/**
	 * @param value
	 */
	public void setParams(String value){
		try {
			setAdditionalParams( value );
		} catch (JSONException e) {
			e.printStackTrace();
			Log.e(TAG, e.getLocalizedMessage());
		}
	}
	
	public void setRoute( ArrayList<LatLng> r ){
		this.route = r;
		this._isRouteCalculated = true;
	}
	
	public boolean isRouteCalculated(){
		return _isRouteCalculated;
	}
	
	public ArrayList<LatLng> getRoute(){
		return route;
	}
	
	public void setCurrentDriverId( long value, String driver_params ){
		currentDriverId = value;
		currentDriverParams = driver_params;
	}
	
	public long getCurrentDriverId(){
		return currentDriverId;
	}	
	
	public void setStatus( int value ){
		status = value;
	}
	
	public int getStatus(){
		return status;
	}
	
	public void setCurrentStatus( int value ){
		currentStatus = value;
	}
	
	public int getCurrentStatus(){
		return currentStatus;
	}
	
	public String getCurrentDriverParams(){
		return this.currentDriverParams;
	}
	
	public String getLastComment(){
		return lastComment;
	}
	
	public void setLastComment(String value){
		if( value != null && !value.equals("") ){
			this.lastComment = value;
		}
	}
	
	public void setPhonenumber(String value){
		phonenumber = value; 
	}
	
	public String getPhonenumber(){
		return phonenumber;
	}
	
}
