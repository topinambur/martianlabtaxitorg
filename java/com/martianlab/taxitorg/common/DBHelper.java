package com.martianlab.taxitorg.common;

import com.martianlab.taxitorg.AppSettings;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBHelper extends SQLiteOpenHelper {
	
	private static final String TAG = "DBHelper";
	
	private Context context;
	
    private static final String DATABASE_CREATE_ADDRESSES =
        " CREATE TABLE addresses ( "
        + "  _id INTEGER PRIMARY KEY AUTOINCREMENT "
        + ", address TEXT NOT NULL "
        + ", latitude NUMBER NOT NULL "
        + ", longitude NUMBER NOT NULL "
        + ", frequency INTEGER NOT NULL DEFAULT 0"
        + " ); ";	
	
    private static final String DATABASE_CREATE_PASSENGER_ORDERS = 
    	" CREATE TABLE orders ( "
    	+ " _id INTEGER PRIMARY KEY AUTOINCREMENT "
    	+ ", order_id INTEGER "
    	+ ", driver_id INTEGER DEFAULT -1"
    	+ ", address_start TEXT "
    	+ ", address_end TEXT "
    	+ ", start_lat NUMBER "
    	+ ", start_lon NUMBER "
    	+ ", end_lat NUMBER "
    	+ ", end_lon NUMBER "
    	+ ", price NUMBER DEFAULT 0"
    	+ ", currency TEXT "
    	+ ", params TEXT "
    	+ ", driver_params TEXT "
    	+ ", voyage_time NUMBER "
    	+ ", status INTEGER default 1"
    	+ ", current_status INTEGER default 1"
    	+ ", comment TEXT"
    	+ ", driver_rated INTEGER default 0"
    	+ " ); ";
    
	public DBHelper(Context ctx) {
		super(ctx, AppSettings.DATABASE_NAME, null, AppSettings.DATABASE_VERSION);
		this.context = ctx;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		Log.w(TAG, "Creating database");
		db.execSQL(DATABASE_CREATE_ADDRESSES);
		db.execSQL(DATABASE_CREATE_PASSENGER_ORDERS);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(TAG, "Upgrading database from version " + oldVersion 
                + " to "
                + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS orders");
        db.execSQL("DROP TABLE IF EXISTS addresses");
        onCreate(db);	
    }

}
