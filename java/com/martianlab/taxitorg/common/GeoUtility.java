package com.martianlab.taxitorg.common;

import android.util.Log;

public class GeoUtility {

	public static byte zoomLevel (double distance){
	    byte zoom=1;
	    double E = 40075;
	    Log.i("Astrology", "result: "+ (Math.log(E/distance)/Math.log(2)+1));
	    zoom = (byte) Math.round(Math.log(E/distance)/Math.log(2)+1);
	    // to avoid exeptions
	    if (zoom>21) zoom=21;
	    if (zoom<1) zoom =1;

	    return zoom;
	}		
	
}
