package com.martianlab.taxitorg;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;
import android.app.Activity;

/**
 * �������� ��� ����������� ��������� ���������� � ����������
 * �������� ������������� �������� � ���� content[ about, faq, license] � ���������� ��������������� �����
 * @author awacs
 *
 */
public class ScreenMainInfoActivity extends Activity {

	private String content_type = "";
	
	private WebView content_body;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_screen_main_info);
		
 	    Bundle extras = getIntent().getExtras();
 	    if( extras != null ){
 		    if( extras.containsKey( "content" ) ){
 			    content_type = extras.getString( "content" );
 		    }
 	    }		
		
        // ���������
        TextView title = (TextView) this.findViewById(R.id.header_title);
        
        Button backButton = (Button)this.findViewById(R.id.header_backbutton);
        backButton.setOnClickListener(new OnClickListener(){
 			@Override
 			public void onClick(View v) {
 				finish();
 			}
        }); 	    
 	    
        // ���� ������
        content_body = (WebView) this.findViewById(R.id.screen_main_info_text);
        String content;
        
 	    if( content_type.equalsIgnoreCase("about") ){
 	    	title.setText(getString(R.string.title_activity_main_info_about));
 	    	content = getString(R.string.text_activity_main_info_about);
 	    } else if( content_type.equalsIgnoreCase("faq") ){
 	    	title.setText(getString(R.string.title_activity_main_info_faq));
 	    	content = getString(R.string.text_activity_main_info_faq);
 	    } else if( content_type.equalsIgnoreCase("license") ){
 	    	title.setText(getString(R.string.title_activity_main_info_license));
 	    	content = getString(R.string.text_activity_main_info_license);
 	    } else {
 	    	title.setText(getString(R.string.title_activity_main_info_about));
 	    	content = getString(R.string.text_activity_main_info_about);
 	    }
	    content_body.loadData( "<html><body>"+content+"</body></html>", "text/html; charset=UTF-8", "utf-8");
 	    
	}

}
