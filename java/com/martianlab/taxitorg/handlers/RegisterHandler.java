package com.martianlab.taxitorg.handlers;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.martianlab.taxitorg.driver.ScreenDriverRegister;

import android.os.AsyncTask;
import android.util.Log;

/**
 * Register driver handler
 * @author awacs
 *
 */
public class RegisterHandler extends AsyncTask<String, Void, String> {

	private static final String TAG = "RegisterHandle";
	
	private final HttpClient client = new DefaultHttpClient();
	private String content;
	private String status;
	private boolean error;
	private String error_msg;
	private long driverId;
	
	private RegisterListener listener;
	
	public RegisterHandler( RegisterListener l ){
		listener = l;
		driverId = 0;
		error = false;
		error_msg = "";
	}
	
	public boolean isError(){
		return error;
	}
	
	public String getErrorMessage(){
		return error_msg;
	}
	
	@Override
	protected String doInBackground(String... urls) {
		
    	try {
            Log.d(TAG, urls[0]);
            HttpPost httppost = new HttpPost(urls[0]);
            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            content = client.execute( httppost, responseHandler );
        } catch (ClientProtocolException e) {
        	Log.e(TAG, "ClientProtocolException");
        	e.printStackTrace();
        	error = true;
        	cancel(true);
         } catch (IOException e) {
        	Log.e(TAG, "IOException");
        	e.printStackTrace();
        	error = true;
        	cancel(true);
         }
		return content;
	}
	
	protected void onPostExecute(String content) {
		JSONArray result = null;
		driverId = 0;
		if(!error){
	    	try {
        		Log.d(TAG, content);
        		JSONObject jsonObj = new JSONObject(content);
        		status = jsonObj.getString("status");
	        	if( status.equals("ok") ){
	        		if( !jsonObj.isNull("driver_id") ){
	        			driverId = jsonObj.getLong("driver_id");
	        			listener.OnCompleteRegister(driverId);
	        		} else {
	        			listener.OnErrorRegister("Unknown error");
	        		}
	        	} else {
	        		error = true;
	        		error_msg = "Unknown error";
	        		if( !jsonObj.isNull("error_message") ){
	        		    error_msg = jsonObj.getString("error_message");
	        		}
	        		listener.OnErrorRegister(error_msg);
	            }
        	} catch (JSONException e) {
				error = true;
				listener.OnErrorRegister("Error in parsing JSON from server");
        		e.printStackTrace();
			}
	    	
	    } else {
	    	listener.OnErrorRegister("Offline");
	    }
		
	}

}