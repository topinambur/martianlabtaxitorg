package com.martianlab.taxitorg.handlers;

import org.json.JSONArray;

public interface OrdersInRadiusListener {
	void OnGetOrdersInRadius();
	void OnReceiveOrdersInRadius( JSONArray result );
	void OnErrorGetOrdersInRadius( String error_msg );
}
