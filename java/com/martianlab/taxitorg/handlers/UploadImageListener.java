package com.martianlab.taxitorg.handlers;

public interface UploadImageListener {
   void OnBeginUpload();
   void OnProgressUpload(int progress);
   void OnEndUpload();
   void OnErrorUpload(String errorMsg);
}
