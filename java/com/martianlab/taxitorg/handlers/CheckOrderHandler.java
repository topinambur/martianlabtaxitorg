package com.martianlab.taxitorg.handlers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.martianlab.taxitorg.AppSettings;
import com.martianlab.taxitorg.service.TaxitorgServicePassenger;

import android.os.AsyncTask;
import android.util.Log;

/**
 * Check order task class
 * @author awacs
 *
 */
public class CheckOrderHandler extends AsyncTask<String, Void, String> {
	
	private static final String TAG = "CheckOrderHandler";
	
	private final HttpClient client = new DefaultHttpClient();
	
	private long orderId;
	private long driverId;
	
	private String content;
	private String status;
	private boolean error = false;
	private String error_msg = "";
	
	private CheckOrderListener listener;
	
	public CheckOrderHandler( CheckOrderListener l ){
		orderId = 0;
		driverId = 0;
		this.listener = l;
	}
	
	public void checkOrder( long driver_id, long order_id ){
    	this.orderId = order_id;
    	this.driverId = driver_id;
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
	    nameValuePairs.add(new BasicNameValuePair("request", AppSettings.REQUEST_CHECK_ORDER_STATUS));
	    nameValuePairs.add(new BasicNameValuePair("order_id", String.valueOf(orderId)));
	    nameValuePairs.add(new BasicNameValuePair("driver_id", String.valueOf(driverId)));
	    String paramString = URLEncodedUtils.format(nameValuePairs, "utf-8");
	    execute( AppSettings.API_TAXITORG_DOMAIN + "?" + paramString );
	    
	    
	    
	}
	
	@Override
	protected String doInBackground(String... urls) {
		
		Log.d(TAG, "PassengerCheckOrderHandler::doInBackground");
		
    	try {
            Log.d(TAG, TAG+"::"+urls[0]);
            HttpPost httppost = new HttpPost(urls[0]);
            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            content = client.execute( httppost, responseHandler );
        } catch (ClientProtocolException e) {
        	Log.e(TAG, e.getMessage());
        	error = true;
        	error_msg = e.getMessage();
        	cancel(true);
         } catch (IOException e) {
         	Log.e(TAG, e.getMessage());
        	error_msg = e.getMessage();
        	e.printStackTrace();
        	error = true;
        	cancel(true);
         }
		return content;
	}
	
    /**
     * ������ �� ����� ����������
     */
	@Override
    protected void onCancelled() {
        if( error ){
        	listener.OnErrorCheckOrder(orderId, error_msg);
        }
	}	
	
	@Override
	protected void onPostExecute(String content) {
		
    	try {
    		Log.d(TAG, content);
    		JSONObject jsonObj = new JSONObject(content);
    		status = jsonObj.getString("status");
        	if( status.equals("ok") ){
        		JSONArray results = jsonObj.getJSONArray("results");
        		int value = results.getInt(0);
        		String comment = results.getString(1);
        		listener.OnCompleteCheckOrder(orderId, value, comment);
        	} else {
        		error = true;
        		error_msg = "Unknown error";
        		if( !jsonObj.isNull("error_message") ){
        		    error_msg = jsonObj.getString("error_message");
        		}
        		listener.OnErrorCheckOrder(orderId, error_msg);
            }
    	} catch (JSONException e) {
    		error = true;
    		error_msg = "JSONException";
    		listener.OnErrorCheckOrder(orderId, error_msg);
			e.printStackTrace();
		}
	    	
	}

}