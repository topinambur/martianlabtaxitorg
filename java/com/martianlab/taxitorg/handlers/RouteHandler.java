package com.martianlab.taxitorg.handlers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.maps.model.LatLng;
import com.martianlab.taxitorg.AppSettings;

import android.os.AsyncTask;
import android.util.Log;


/**
 * ����� ���������� ��������� ������, ���� ������ mapView, �������������� �� ��� 
 * @author awacs
 *
 */
public class RouteHandler extends AsyncTask<String, Void, String> {
	
	private final static String TAG = "RouteHandler";
	
	private final HttpClient client = new DefaultHttpClient();
	private String content;
	private boolean error = false; 
	private String error_msg = "";
	
	private ArrayList<LatLng> polyline;
	
	private int accuracyRoute = 1;
	
	private long distance;
	private long duration;
	
	private RouteListener listener;
	
	public RouteHandler( RouteListener l ){
		this.listener = l;
	}
	
	public double getDistance(){
		Log.d(TAG, "distance = " + distance + "m");
		return distance/1000;
	}
	
	public void calculateRoute( double latStart, double lonStart, double latEnd, double lonEnd, int accuracy ){
		
		this.accuracyRoute = accuracy;
		
	    StringBuilder origin = new StringBuilder();
	    origin.append( Double.toString(latStart));
	    origin.append(",");
	    origin.append( Double.toString(lonStart));	    

	    StringBuilder destination = new StringBuilder();
	    destination.append( Double.toString(latEnd));
	    destination.append(",");
	    destination.append( Double.toString(lonEnd));	    
	    
    	List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
	    nameValuePairs.add(new BasicNameValuePair("origin", origin.toString()));
	    nameValuePairs.add(new BasicNameValuePair("destination", destination.toString() ));
	    nameValuePairs.add(new BasicNameValuePair("sensor", "false"));
	    String paramString = URLEncodedUtils.format(nameValuePairs, "utf-8");
	    execute( "http://maps.google.com/maps/api/directions/json" + "?" + paramString );	    
	    
	}
	

	public boolean isError(){
		return error;
	}
	
	public String getErrorMsg(){
		return error_msg;
	}
	
    @Override
	protected String doInBackground(String... urls) {
		
		Log.d(TAG, "RouteHandler::doInBackground");

    	try {
    		Log.v(TAG, urls[0]);
            HttpPost httppost = new HttpPost(urls[0]);
            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            content = client.execute( httppost, responseHandler );
        } catch (ClientProtocolException e) {
        	Log.d(TAG, "GetRouteHandler::ClientProtocolException");
        	e.printStackTrace();
        	error = true;
        	cancel(true);
         } catch (IOException e) {
        	Log.d(TAG, "GetRouteHandler::IOException");
        	e.printStackTrace();
        	error = true;
        	cancel(true);
         }
		return content;
	}
	
	protected void onPostExecute(String content) {
		if (error) {
	        error_msg = "Offline";
	    } else {
	    	try {
				JSONObject response = new JSONObject(content);
				String status = response.getString("status");
				Log.v(TAG, content.replace("\n", ""));
				if( status.equalsIgnoreCase("OK") ){
					polyline = new ArrayList<LatLng>();
				
					JSONArray routesArray = response.getJSONArray("routes");
					JSONObject route = routesArray.getJSONObject(0);
					// ������ � ����������� �� ������� ��������
					JSONArray legs = route.getJSONArray("legs");
					JSONObject leg = legs.getJSONObject(0);	
					
					JSONObject distanceObj = leg.getJSONObject("distance");
					distance = distanceObj.getLong("value");
					
					JSONObject durationObj = leg.getJSONObject("duration");
					duration = durationObj.getLong("value");
					
					// �������� ��� ��������� ��������������� ���� ��� ��������.
					JSONObject bounds = route.getJSONObject("bounds");
					JSONObject bounds_southwest = bounds.getJSONObject("southwest"); 
					JSONObject bounds_northeast = bounds.getJSONObject("northeast");
					
					double maxLat = bounds_northeast.getDouble("lat");
					double maxLon = bounds_northeast.getDouble("lng");
					double minLat = bounds_southwest.getDouble("lat"); 
					double minLon = bounds_southwest.getDouble("lng");
					
					listener.onBoundsCalculated(new LatLng(minLat, minLon), new LatLng(maxLat, maxLon));
					

					JSONArray steps = leg.getJSONArray("steps");
					for( int i=0; i<steps.length(); i++ ){
						JSONObject step = steps.getJSONObject(i);
						JSONObject start_location = step.getJSONObject("start_location");
						JSONObject end_location = step.getJSONObject("end_location");

						double latitudeStart = start_location.getDouble("lat");
						double longitudeStart = start_location.getDouble("lng");
						double latitudeEnd = end_location.getDouble("lat");
						double longitudeEnd = end_location.getDouble("lng");
						LatLng startGeoPoint = new LatLng(latitudeStart,longitudeStart);
						LatLng endGeoPoint = new LatLng(latitudeEnd,longitudeEnd);
						JSONObject polylineObject = step.getJSONObject("polyline");
						if( accuracyRoute == AppSettings.FINE_ROUTE ){
							List<LatLng> points = decodePoly(polylineObject.getString("points"));
							polyline.addAll(points);
						} else {
							polyline.add(startGeoPoint);
							polyline.add(endGeoPoint);
						}
					} 
					
					listener.onRouteCompleted( polyline, distance, duration );
					
				} else if( status.equalsIgnoreCase("NOT_FOUND")){
					// �� ������� ���� ��� ����� �������� ����� (�������� �����, ������ ���������� ��� ������� �����) �������������� ����������.
					listener.onRouteCalcError(status);
				} else if( status.equalsIgnoreCase("ZERO_RESULTS")){
					// ����� �������� ������ � ������� ���������� �� ������� �� ������ ��������.
					listener.onRouteCalcError(status);
				} else if( status.equalsIgnoreCase("MAX_WAYPOINTS_EXCEEDED")){
					// � ������� ������ ������� ����� waypoints. ������������ ���������� waypoints ����� 8 ���� �������� ����� � ����� ����������. ( (������������ Google Maps Premier ����� ��������� ������� � ����������� ������� ����� �� 23.)
					listener.onRouteCalcError(status);
				} else if( status.equalsIgnoreCase("INVALID_REQUEST")){
					// ������ ����������
					listener.onRouteCalcError(status);
				}else if( status.equalsIgnoreCase("OVER_QUERY_LIMIT")){
					// ������ �������� ������� ����� �������� �� ������ ���������� � ����������� ������ �������.
					listener.onRouteCalcError(status);
				}else if( status.equalsIgnoreCase("REQUEST_DENIED")){
					// ������ Directions ��������� ������ ������ ����������.
					listener.onRouteCalcError(status);
				}else if( status.equalsIgnoreCase("UNKNOWN_ERROR")){
					// ��������� ������� �������� ���������� ��-�� ������ �������. ��� ��������� ������� ������ ����� ���� ������� ��������
					listener.onRouteCalcError(status);
				}else{
					listener.onRouteCalcError(status);
				}
			} catch (JSONException e) {
				Log.e(TAG, e.getMessage());
				listener.onRouteCalcError( e.getMessage() );
				e.printStackTrace();
			}
	    }
		
	} // end postExecute
	
	/**
	 * ���������� ��������� �� ���������� ������ ������
	 * @param encoded
	 * @return
	 */
	private List<LatLng> decodePoly(String encoded) {

	    List<LatLng> poly = new ArrayList<LatLng>();
	    int index = 0, len = encoded.length();
	    int lat = 0, lng = 0;

	    while (index < len) {
	        int b, shift = 0, result = 0;
	        do {
	            b = encoded.charAt(index++) - 63;
	            result |= (b & 0x1f) << shift;
	            shift += 5;
	        } while (b >= 0x20);
	        int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
	        lat += dlat;

	        shift = 0;
	        result = 0;
	        do {
	            b = encoded.charAt(index++) - 63;
	            result |= (b & 0x1f) << shift;
	            shift += 5;
	        } while (b >= 0x20);
	        int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
	        lng += dlng;

	        LatLng p = new LatLng( lat/1E5, lng/1E5);
	        poly.add(p);
	    }

	    return poly;
	} // end decodePoly   	
	
} // end class	