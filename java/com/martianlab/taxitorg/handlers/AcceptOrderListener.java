package com.martianlab.taxitorg.handlers;

public interface AcceptOrderListener{
    void onAcceptCompleted();
    void onAcceptError(String error_msg);
}
