package com.martianlab.taxitorg.handlers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.martianlab.taxitorg.AppSettings;

import android.os.AsyncTask;
import android.util.Log;

/**
 * Check orders task class
 * @author awacs
 *
 */
public class GetOrdersHandler extends AsyncTask<String, Void, String> {
	
	private static final String TAG = "GetPrdersHandler";
	
	private final HttpClient client = new DefaultHttpClient();
	private String content;
	private String status;
	private boolean error = false;
	private String error_msg = "";
	
	private GetOrdersListener listener;
	
	public GetOrdersHandler( GetOrdersListener l ){
		listener = l;
	}
	
	public void getOrders( long driverId ){
    	List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("request", AppSettings.REQUEST_GET_ORDERS_BY_DRIVER_ID));
		nameValuePairs.add(new BasicNameValuePair("driver_id", String.valueOf(driverId)));
		nameValuePairs.add(new BasicNameValuePair("statuses", "1,2,3,5,6,7"));
		String paramString = URLEncodedUtils.format(nameValuePairs, "utf-8");
		execute( AppSettings.API_TAXITORG_DOMAIN + "?" + paramString );
	}
	
	@Override
	protected String doInBackground(String... urls) {
		
    	try {
            Log.d(TAG, "GetOrdersHandler::"+urls[0]);
            HttpPost httppost = new HttpPost(urls[0]);
            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            content = client.execute( httppost, responseHandler );
        } catch (ClientProtocolException e) {
        	Log.d(TAG, "GetOrdersHandler::ClientProtocolException");
        	e.printStackTrace();
        	error = true;
        	cancel(true);
         } catch (IOException e) {
        	Log.d(TAG, TAG+"::IOException");
        	e.printStackTrace();
        	error = true;
        	cancel(true);
         }
		return content;
	}
	
	protected void onPostExecute(String content) {
		String result = null;
		if(!error){
	    	try {
        		Log.d(TAG, content);
        		JSONObject jsonObj = new JSONObject(content);
        		status = jsonObj.getString("status");
	        	if( status.equals("ok") ){
	        		result = jsonObj.getString("results");
	        		listener.OnCompleteGetOrdersListener(result);
	        	} else {
	        		error = true;
	        		error_msg = "Unknown error";
	        		if( !jsonObj.isNull("error_message") ){
	        		    error_msg = jsonObj.getString("error_message");
	        		}
	        		listener.OnErrorGetOrdersListener(error_msg);
	            }
        	} catch (JSONException e) {
				error = true;
				error_msg = "Error in parsing JSON from server";
        		listener.OnErrorGetOrdersListener(error_msg);
				e.printStackTrace();
			}
	    } else {
	    	listener.OnErrorGetOrdersListener("Offline");
	    }
		
	}

}