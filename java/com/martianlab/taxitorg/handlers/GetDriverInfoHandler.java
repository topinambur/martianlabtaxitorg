package com.martianlab.taxitorg.handlers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.martianlab.taxitorg.AppSettings;
import com.martianlab.taxitorg.common.Driver;

import android.os.AsyncTask;
import android.util.Log;

/**
 * �������� ���������� � �������� �� ��� id
 * @author awacs
 *
 */
public class GetDriverInfoHandler extends AsyncTask<String, Void, String> {
	
	private static final String TAG = "GetDriverInfoHandler";
	
	private final HttpClient client = new DefaultHttpClient();
	private String content;
	private String status;
	private boolean error = false;
	private String error_msg = "";
	
	private GetDriverInfoListener listener;
	
	public GetDriverInfoHandler(GetDriverInfoListener l){
		listener = l;
	}
	
	public void getDriverInfo( long driverId ){
    	listener.OnBeginGetDriverInfo( driverId );
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
	    nameValuePairs.add(new BasicNameValuePair("request", AppSettings.REQUEST_GET_DRIVER_INFO));
	    nameValuePairs.add(new BasicNameValuePair("driver_id", String.valueOf(driverId)));
	    String paramString = URLEncodedUtils.format(nameValuePairs, "utf-8");
	    execute( AppSettings.API_TAXITORG_DOMAIN + "?" + paramString );		
	}
	
	@Override
	protected String doInBackground(String... urls) {
		
    	try {
            Log.d(TAG, urls[0]);
            HttpPost httppost = new HttpPost(urls[0]);
            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            content = client.execute( httppost, responseHandler );
        } catch (ClientProtocolException e) {
        	Log.e(TAG, TAG+"::ClientProtocolException");
        	e.printStackTrace();
        	error = true;
        	error_msg = "ClientProtocolException";
        	cancel(true);
         } catch (IOException e) {
        	Log.e(TAG, TAG+"::IOException");
        	error_msg = "IOException";
        	e.printStackTrace();
        	error = true;
        	cancel(true);
         }
		return content;
	}
	
	@Override
	protected void onCancelled(){
		if(error) {
	        listener.OnErrorGetDriverInfo(error_msg);
	    }		
	}
	
	@Override
	protected void onPostExecute(String content) {
    	try {
    		Log.d(TAG, content);
    		JSONObject jsonObj = new JSONObject(content);
    		status = jsonObj.getString("status");
        	if( status.equals("ok") ){
        		Driver driver = new Driver( jsonObj.getJSONArray("results") );
        		listener.OnEndGetDriverInfo(driver);
        	} else {
        		error_msg = "Unknown error";
        		if( !jsonObj.isNull("error_message") ){
        		    error_msg = jsonObj.getString("error_message");
        		}
        		listener.OnErrorGetDriverInfo(error_msg);        		
            }
    	} catch (JSONException e) {
    		error = true;
    		listener.OnErrorGetDriverInfo("JSONException");
			e.printStackTrace();
		}
	    	
	}

}