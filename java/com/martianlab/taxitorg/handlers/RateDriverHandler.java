package com.martianlab.taxitorg.handlers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.martianlab.taxitorg.AppSettings;

import android.os.AsyncTask;
import android.util.Log;

/**
 * ������� ��������
 * @author awacs
 *
 */
public class RateDriverHandler extends AsyncTask<String, Void, String> {
	
	private static final String TAG = "RateDriverHandler";
	
	private final HttpClient client = new DefaultHttpClient();
	private String content;

	private long driverId;
	private long orderId;
	
	private boolean error;
	private String error_msg;
	
	private RateDriverListener listener;
	
	public RateDriverHandler( RateDriverListener l ){
		listener = l;
		error = false;
		error_msg = "";
	}
	
	public void rateDriver(long order_id, long driver_id, String device_id, int rating){ 
    	List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
    	
    	this.orderId = order_id;
    	this.driverId = driver_id;
    	
	    nameValuePairs.add(new BasicNameValuePair("request",AppSettings.REQUEST_RATE_DRIVER));
	    nameValuePairs.add(new BasicNameValuePair("order_id", String.valueOf(order_id)));
	    nameValuePairs.add(new BasicNameValuePair("driver_id", String.valueOf(driver_id)));
	    nameValuePairs.add(new BasicNameValuePair("device_id", device_id));
	    nameValuePairs.add(new BasicNameValuePair("rating", String.valueOf(rating)));
	    String paramString = URLEncodedUtils.format(nameValuePairs, "utf-8");
    	execute(AppSettings.API_TAXITORG_DOMAIN + "?" + paramString);		
	}
	
	@Override
	protected String doInBackground(String... urls) {
    	try {
            Log.d(TAG, urls[0]);
            HttpPost httppost = new HttpPost(urls[0]);
            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            content = client.execute( httppost, responseHandler );
        } catch (ClientProtocolException e) {
        	Log.e(TAG, "ClientProtocolException");
        	e.printStackTrace();
        	error = true;
        	cancel(true);
         } catch (IOException e) {
        	Log.e(TAG, "IOException");
        	e.printStackTrace();
        	error = true;
        	cancel(true);
         }
		return content;
	}
	 
	@Override
    protected void onCancelled() {
        if( error ){
        	listener.OnErrorRateDriver("Offline");
        }
	}
	
	@Override
	protected void onPostExecute(String content) {
		Log.d(TAG, content);
		
		JSONArray result = null;
    	try {
    		JSONObject jsonObj = new JSONObject(content);
    		String status = jsonObj.getString("status");
        	if( status.equals("ok") ){
        		listener.OnCompleteRateDriver(orderId, driverId);
        	} else {
        		error = true;
        		error_msg = "Unknown error";
        		if( !jsonObj.isNull("error_message") ){
        		    error_msg = jsonObj.getString("error_message");
        		}
        		listener.OnErrorRateDriver(error_msg);
            }
    	} catch (JSONException e) {
			error = true;
			error_msg = "Error in parsing JSON from server";
			listener.OnErrorRateDriver(error_msg);
    		e.printStackTrace();
		}
	    	
	}

}