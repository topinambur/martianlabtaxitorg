package com.martianlab.taxitorg.handlers;

import com.martianlab.taxitorg.common.Order;

public interface CancelOrderListener{
    void onCancelOrderCompleted(Order order);
    void onCancelOrderError(String error_msg);
}
