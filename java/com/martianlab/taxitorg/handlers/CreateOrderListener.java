package com.martianlab.taxitorg.handlers;

public interface CreateOrderListener {
	void OnCreateOrderError( String errorMsg );
	void onCreateOrderComplete( long orderId, long row_id );
}
