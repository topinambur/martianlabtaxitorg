package com.martianlab.taxitorg.handlers;

import java.util.ArrayList;

import com.google.android.gms.maps.model.LatLng;

public interface RouteListener {
    void onRouteCalcBegin();
    void onBoundsCalculated( LatLng southwest, LatLng northeast  );
	void onRouteCompleted( ArrayList<LatLng> route, long distance, long duration );
	void onRouteCalcError( String error_msg );
}
