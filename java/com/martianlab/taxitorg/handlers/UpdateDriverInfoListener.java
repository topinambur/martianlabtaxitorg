package com.martianlab.taxitorg.handlers;

public interface UpdateDriverInfoListener {
   void OnUpdateDriverInfoError( String error_msg );
   void OnUpdateDriverInfoComplete();
}
