package com.martianlab.taxitorg.handlers;

public interface CheckOrderListener {
    void OnErrorCheckOrder( long orderId, String error_msg );
    void OnCompleteCheckOrder( long orderId, int status, String comment );
}
