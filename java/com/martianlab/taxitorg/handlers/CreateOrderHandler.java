package com.martianlab.taxitorg.handlers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;
import android.os.AsyncTask;
import android.util.Log;

import com.martianlab.taxitorg.AppSettings;
import com.martianlab.taxitorg.common.Order;

/**
 * ����� ��� �������� ������� �� �������� ������ � �������� ������
 * @author awacs
 *
 */
public class CreateOrderHandler extends AsyncTask<String, Void, String> {
	
	private static final String TAG = "CreateOrderHandler";
	
	private final HttpClient client = new DefaultHttpClient();
	
	private String content;
	private String status;
	
	private boolean error = false;
	
	private CreateOrderListener listener;
	
	/**
	 * ������������� ������ ������ � ��������� ���� ������
	 */
	private long row_id;
	
	public CreateOrderHandler( CreateOrderListener l, long _id ){
		listener = l;
		row_id = _id;
	}
	
	public void sendOrder( Order order, String device_id){
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(  );
		nameValuePairs.add(new BasicNameValuePair("request", AppSettings.REQUEST_CREATE_ORDER));
		nameValuePairs.add(new BasicNameValuePair("start_lat", String.valueOf(order.getStartLatitude())));
		nameValuePairs.add(new BasicNameValuePair("start_lon", String.valueOf(order.getStartLongitude())));
	   nameValuePairs.add(new BasicNameValuePair("end_lat", String.valueOf(order.getEndLatitude())));
	   nameValuePairs.add(new BasicNameValuePair("end_lon", String.valueOf(order.getEndLongitude())));
	   long voyageTime = order.getVoyageTime();
	   if( voyageTime == 0 ){
		   voyageTime = System.currentTimeMillis()/1000;
	   }
	   nameValuePairs.add(new BasicNameValuePair( "voyage_time", String.valueOf(voyageTime) ));
	   nameValuePairs.add(new BasicNameValuePair("price", order.getPrice()));
	   nameValuePairs.add(new BasicNameValuePair("currency", order.getCurrency()));
	   nameValuePairs.add(new BasicNameValuePair("params", order.getParams()));
	   nameValuePairs.add(new BasicNameValuePair("device_id", device_id));
	   String paramString = URLEncodedUtils.format(nameValuePairs, "utf-8");
	   execute( AppSettings.API_TAXITORG_DOMAIN + "?" + paramString );
	}
	
    @Override
	protected String doInBackground(String... urls) {
		
		Log.d(TAG, "PassengerCreateOrder::doInBackground");

    	try {
    		
    		Log.d(TAG, "PassengerCreateOrder::"+urls[0]);
            HttpPost httppost = new HttpPost(urls[0]);
            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            content = client.execute( httppost, responseHandler );
        } catch (ClientProtocolException e) {
        	Log.e(TAG, "PassengerCreateOrder::ClientProtocolException");
        	e.printStackTrace();
        	error = true;
        	cancel(true);
         } catch (IOException e) {
        	Log.e(TAG, "PassengerCreateOrder::IOException");
        	e.printStackTrace();
        	error = true;
        	cancel(true);
         }
		return content;
	}
	
    @Override
    protected void onCancelled() {
        if( error ){
        	listener.OnCreateOrderError("Offline");
        }
    }    
    
	protected void onPostExecute(String content) {
		
		Log.d(TAG, "onPostExecute");
		
		long orderId = -1;
		
		if (error) {
		} else {
	    	try {
        		Log.d(TAG, content);
        		JSONObject jsonObj = new JSONObject(content);
        		status = jsonObj.getString("status");
        		
	        	if( status.equals("ok") ){
	        		orderId = jsonObj.getLong("order_id");
	        	} else {
	        		listener.OnCreateOrderError("Unknown error");
	        		if( !jsonObj.isNull("error_message") ){
	        			listener.OnCreateOrderError(jsonObj.getString("error_message"));
	        		}
	            }
        	} catch (JSONException e) {
        		listener.OnCreateOrderError( e.getMessage() );
				e.printStackTrace();
			}
	    	
	    }
		
		listener.onCreateOrderComplete( orderId, row_id );
		
	}

}