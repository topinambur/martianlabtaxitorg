package com.martianlab.taxitorg.handlers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.martianlab.taxitorg.AppSettings;
import com.martianlab.taxitorg.common.Driver;
import com.martianlab.taxitorg.driver.ScreenDriverCurrentOrdersList;
import com.martianlab.taxitorg.passenger.ScreenSearchOffers;

import android.os.AsyncTask;
import android.util.Log;

/**
 * ����� ��� �������� ������� �� ��������� ������� ������ � ������� statuses
 * @author awacs
 *
 */
public class SetOrderStatusHandler extends AsyncTask<String, Void, String> {
	
	private final static String TAG = "SetOrderStatusHandler";
	
	private final HttpClient client = new DefaultHttpClient();
	private String content;
	private String status;
	
	private String comment;
	
	private int order_status;
	private Driver driver;
	private long order_id;
	
	private boolean error = false; 
	private String error_msg = "";
	
	private SetOrderStatusListener listener;
	
	
	public SetOrderStatusHandler(SetOrderStatusListener l){
		this.listener = l;
	}
	
	public void setStatus( long _orderId, Driver _driver, int _status, String comment){
		this.order_status = _status;
		this.driver = _driver;
		this.order_id = _orderId;
		this.comment = comment;
		
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("request", AppSettings.REQUEST_SET_ORDER_STATUS ));
		nameValuePairs.add(new BasicNameValuePair("order_id", String.valueOf(_orderId)));
		nameValuePairs.add(new BasicNameValuePair("driver_id", String.valueOf(driver.getId())  ));
		nameValuePairs.add(new BasicNameValuePair("status", String.valueOf(_status)));
		if( !comment.equals("") ){
			nameValuePairs.add(new BasicNameValuePair("comment", String.valueOf(comment)));
		}
		String paramString = URLEncodedUtils.format(nameValuePairs, "utf-8");
		this.execute(AppSettings.API_TAXITORG_DOMAIN + "?" + paramString);
	}
	
	
    @Override
	protected String doInBackground(String... urls) {
		
		Log.d(TAG, "PassengerSetOrderStatusHandler::doInBackground");

    	try {
    		Log.d(TAG, urls[0]);
            HttpPost httppost = new HttpPost(urls[0]);
            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            content = client.execute( httppost, responseHandler );
        } catch (ClientProtocolException e) {
        	Log.d(TAG, "PassengerSetOrderStatusHandler::ClientProtocolException");
        	e.printStackTrace();
        	error = true;
        	cancel(true);
         } catch (IOException e) {
        	Log.d(TAG, "PassengerSetOrderStatusHandlerr::IOException");
        	e.printStackTrace();
        	error = true;
        	cancel(true);
         }
		return content; 
	}
	
    /**
     * ������ �� ����� ����������
     */
	@Override
    protected void onCancelled() {
        if( error ){
        	listener.OnSetOrderStatusError(order_id, "Offline");
        }
	}        
    
	protected void onPostExecute(String content) {
		if (error) {
	        error = true;
	        error_msg = "Offline";
	    } else {
	    	try {
        		Log.d(TAG, content);
        		JSONObject jsonObj = new JSONObject(content);
        		status = jsonObj.getString("status");
        		
	        	if( status.equals("ok") ){
				    listener.OnSetOrderStatusComplete(order_status, driver, order_id, comment);
	        	} else {
	        		error_msg = "Unknown error";
	        		if( !jsonObj.isNull("error_message") ){
	        		    error_msg = jsonObj.getString("error_message");
	        		}
	        		listener.OnSetOrderStatusError(order_id, error_msg);
	            }
        	} catch (JSONException e) {
				error = true;
				error_msg = "JSONException";
				listener.OnSetOrderStatusError(order_id, error_msg);
				e.printStackTrace();
			}
	    	
	    }     	
	}

}   