package com.martianlab.taxitorg.handlers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;

import com.martianlab.taxitorg.AppSettings;


/**
 * ����� ��� �������� ������� �� ������ ������� 
 * @author awacs
 *
 */
public class GetOrdersInRadiusHandler extends AsyncTask<String, Void, String> {
	
	private static final String TAG = "GetOrdersInRadiusHandler";
	
	private final HttpClient client = new DefaultHttpClient();
	private String content;
	private String status;
	
	private boolean error = false; 
	private String error_msg;
	
	private OrdersInRadiusListener listener; 
	

	public boolean isError(){
		return error;
	}
	
	public String getErrorMessage(){
		return error_msg;
	}

	public GetOrdersInRadiusHandler(OrdersInRadiusListener l){
		listener = l;
	}
	
	public void getOrders( double latitude, double longitude, double radius ){
    	listener.OnGetOrdersInRadius();
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
	    nameValuePairs.add(new BasicNameValuePair("request",AppSettings.REQUEST_GET_ORDERS));
	    nameValuePairs.add(new BasicNameValuePair("lat", String.valueOf(latitude)));
	    nameValuePairs.add(new BasicNameValuePair("lon", String.valueOf(longitude)));
	    nameValuePairs.add(new BasicNameValuePair("radius", String.valueOf(radius)));
	    String paramString = URLEncodedUtils.format(nameValuePairs, "utf-8");
	    execute( AppSettings.API_TAXITORG_DOMAIN + "?" + paramString );
}    	
	
    @Override
	protected String doInBackground(String... urls) {
		
		Log.d(TAG, "GetOrdersHandler::doInBackground");

    	try {
    		
    		Log.d(TAG, "GetOrdersHandler::"+urls[0]);
            HttpPost httppost = new HttpPost(urls[0]);
            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            content = client.execute( httppost, responseHandler );
        } catch (ClientProtocolException e) {
        	Log.d(TAG, "GetOrdersHandler::ClientProtocolException");
        	e.printStackTrace();
        	error = true;
        	cancel(true);
         } catch (IOException e) {
        	Log.d(TAG, "GetOrdersHandler::IOException");
        	e.printStackTrace();
        	error = true;
        	cancel(true);
         }
		return content;
	}
	
    /**
     * ������ �� ����� ����������
     */
	@Override
    protected void onCancelled() {
        if( error ){
        	listener.OnErrorGetOrdersInRadius("Offline");
        }
	}      
    
	@Override
	protected void onPostExecute(String content) {
    	try {
    		Log.d(TAG, content);
    		JSONObject jsonObj = new JSONObject(content);
    		status = jsonObj.getString("status");
    		
        	if( status.equals("ok") ){
        		JSONArray result = jsonObj.getJSONArray("results");
        		listener.OnReceiveOrdersInRadius(result);
        	} else {
        		error = true;
        		error_msg = "Unknown error";
        		if( !jsonObj.isNull("error_message") ){
        		    error_msg = jsonObj.getString("error_message");
        		}
        		listener.OnErrorGetOrdersInRadius(error_msg);
            }
    	} catch (JSONException e) {
			error = true;
			error_msg = e.getMessage();
			listener.OnErrorGetOrdersInRadius(error_msg);
			e.printStackTrace();
		}
	}

}
