package com.martianlab.taxitorg.handlers;

import org.json.JSONArray;

public interface GetDriverListListener {
	void OnCompleteGetDriverList( long orderId, JSONArray list );
	void OnErrorGetDriverList( long orderId, String error_msg );
}
