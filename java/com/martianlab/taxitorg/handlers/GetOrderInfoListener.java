package com.martianlab.taxitorg.handlers;

import org.json.JSONArray;

import com.martianlab.taxitorg.common.Order;

public interface GetOrderInfoListener {
   void OnBeginGetOrderInfo();
   void OnEndGetOrderInfo( Order order );
   void OnErrorGetOrderInfo(String errorMsg);
}
