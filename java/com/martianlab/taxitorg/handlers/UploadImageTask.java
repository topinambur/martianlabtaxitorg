package com.martianlab.taxitorg.handlers;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;

import com.martianlab.taxitorg.AppSettings;

/**
 * Asynchronous task to upload file to server
 */
public class UploadImageTask extends AsyncTask<Bitmap, Integer, Boolean> { 
	
	private static final String TAG = "UploadImageTask";
	
	private int id_img;
	
	private UploadImageListener listener;
	
	public UploadImageTask(UploadImageListener l, int id){
		this.listener = l;
		this.id_img = id;
	}
	
    /**
     * Prepare activity before upload
     */
    @Override
    protected void onPreExecute() {
    	listener.OnBeginUpload();
    }

    /**
     * Clean app state after upload is completed
     */
    @Override
    protected void onPostExecute(Boolean result) {
        listener.OnEndUpload();
    	super.onPostExecute(result);
    }

    @Override
    protected Boolean doInBackground(Bitmap... image) {
        return doFileUpload(image[0], "UPLOAD_URL");
    }
    
    @Override
    protected void onCancelled(){
    }
    
    @Override
    protected void onProgressUpdate(Integer... values) {
        listener.OnProgressUpload((int) (values[0]));
    }


    private boolean doFileUpload(Bitmap bitmap, String uploadUrl) {
        HttpURLConnection connection = null;
        DataOutputStream outputStream = null;

        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary =  "*****";

        int bytesRead, bytesAvailable, bufferSize;
        
        int maxBufferSize = 1*1024*1024;

        // log filesize
        
        String buffers= String.valueOf(maxBufferSize);
        
        Log.d("buffers",buffers);

        try {
        	ByteArrayOutputStream bao = new ByteArrayOutputStream();
        	bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bao);
        	byte [] ba = bao.toByteArray();
        	ByteArrayInputStream bs = new ByteArrayInputStream(ba);	
            
        	long fileSize = bao.size();
        	Log.d(TAG, AppSettings.API_TAXITORG_URI_UPLOAD_IMAGE);
            URL url = new URL(AppSettings.API_TAXITORG_URI_UPLOAD_IMAGE);
            connection = (HttpURLConnection) url.openConnection();

            // Allow Inputs & Outputs
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);

            // Enable POST method
            connection.setRequestMethod("POST");

            connection.setRequestProperty("Connection", "Keep-Alive");
            connection.setRequestProperty("Content-Type", "multipart/form-data;boundary="+boundary);

            outputStream = new DataOutputStream( connection.getOutputStream() );
            outputStream.writeBytes(twoHyphens + boundary + lineEnd);
            Log.d(TAG, "Content-Disposition: form-data; name=\"uploadedfile\";filename=\"" + String.valueOf(AppSettings.currentDriver.getId()) + String.valueOf(id_img) +".jpg\"" + lineEnd);
            outputStream.writeBytes("Content-Disposition: form-data; name=\"uploadedfile\";filename=\"" + String.valueOf(AppSettings.currentDriver.getId()) + String.valueOf(id_img) +".jpg\"" + lineEnd);
            outputStream.writeBytes(lineEnd);

            bytesAvailable = bs.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            byte[] buffer = new byte[bufferSize];

            // Read file
            bytesRead = bs.read(buffer, 0, bufferSize);
            int sentBytes = 0;
            while (bytesRead > 0) {
	            outputStream.write(buffer, 0, bufferSize);
	            bytesAvailable = bs.available();
	            bufferSize = Math.min(bytesAvailable, maxBufferSize);
	            bytesRead = bs.read(buffer, 0, bufferSize);
	
	            sentBytes += bufferSize;
	            publishProgress((int)(sentBytes * 100 / fileSize * 2));
	            bytesAvailable = bs.available();
	            bufferSize = Math.min(bytesAvailable, maxBufferSize);
	            bytesRead = bs.read(buffer, 0, bufferSize);
            }
            publishProgress(100);
            
            outputStream.writeBytes(lineEnd);
            outputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

            // Responses from the server (code and message)
            //int serverResponseCode = connection.getResponseCode();
            //String serverResponseMessage = connection.getResponseMessage();

            bs.close();
            outputStream.flush();
            outputStream.close();

            try {
                int responseCode = connection.getResponseCode();
                return responseCode == 200;
            } catch (IOException ioex) {
                listener.OnErrorUpload("Upload file failed: " + ioex.getMessage());
            	ioex.printStackTrace();
                return false;
            } catch (Exception e) {
                listener.OnErrorUpload("Upload file failed: " + e.getMessage());
            	e.printStackTrace();
                return false;
            }
        } catch (Exception ex) {
        	listener.OnErrorUpload("Upload file failed: " + ex.getMessage());
        	ex.printStackTrace();
        }
        return true;
    }
}