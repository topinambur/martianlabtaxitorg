package com.martianlab.taxitorg.handlers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.martianlab.taxitorg.AppSettings;
import com.martianlab.taxitorg.common.Order;
import com.martianlab.taxitorg.service.TaxitorgServicePassenger;

import android.os.AsyncTask;
import android.util.Log;

/**
 * Check order task class
 * @author awacs
 *
 */
public class GetOrderInfoHandler extends AsyncTask<String, Void, String> {
	
	private static final String TAG = "CheckOrderHandler";
	
	private final HttpClient client = new DefaultHttpClient();
	private String content;
	private String status;
	private boolean error = false;
	private String error_msg = "";
	
	private GetOrderInfoListener listener;
	
	public boolean isError(){
		return error;
	}
	
	public String getErrorMsg(){
		return error_msg;
	}
	
	public GetOrderInfoHandler(GetOrderInfoListener l){
		listener = l;
	}
	
	public void getOrderInfo( long orderId ){
    	listener.OnBeginGetOrderInfo();
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
	    nameValuePairs.add(new BasicNameValuePair("request", AppSettings.REQUEST_GET_ORDER_INFO));
	    nameValuePairs.add(new BasicNameValuePair("order_id", String.valueOf(orderId)));
	    String paramString = URLEncodedUtils.format(nameValuePairs, "utf-8");
	    execute( AppSettings.API_TAXITORG_DOMAIN + "?" + paramString );		
	}
	
	@Override
	protected String doInBackground(String... urls) {
		
    	try {
            Log.d(TAG, urls[0]);
            HttpPost httppost = new HttpPost(urls[0]);
            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            content = client.execute( httppost, responseHandler );
        } catch (ClientProtocolException e) {
        	Log.e(TAG, TAG+"::ClientProtocolException");
        	e.printStackTrace();
        	error = true;
        	error_msg = "ClientProtocolException";
        	cancel(true);
         } catch (IOException e) {
        	Log.e(TAG, TAG+"::IOException");
        	error_msg = "IOException";
        	e.printStackTrace();
        	error = true;
        	cancel(true);
         }
		return content;
	}
	
	@Override
	protected void onPostExecute(String content) {
		
		if(error) {
	        error_msg = "Offline";
	    } else {
	    	try {
        		Log.d(TAG, content);
        		JSONObject jsonObj = new JSONObject(content);
        		status = jsonObj.getString("status");
	        	if( status.equals("ok") ){
	        		Order order = new Order( jsonObj.getJSONArray("results") );
	        		listener.OnEndGetOrderInfo(order);
	        	} else {
	        		error = true;
	        		error_msg = "Unknown error";
	            }
        	} catch (JSONException e) {
        		error = true;
        		error_msg = "JSONException";
				e.printStackTrace();
			}
	    	
	    }
		
	}

}