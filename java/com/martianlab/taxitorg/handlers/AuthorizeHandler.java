package com.martianlab.taxitorg.handlers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.martianlab.taxitorg.AppSettings;

import android.os.AsyncTask;
import android.util.Log;

/**
 * Authorize driver handler
 * @author awacs
 *
 */
public class AuthorizeHandler extends AsyncTask<String, Void, String> {
	
	private static final String TAG = "AuthorizeHandle";
	
	private final HttpClient client = new DefaultHttpClient();
	private String content;
	private String status;
	private boolean error;
	private String error_msg;
	
	private AuthorizeListener listener;
	
	public AuthorizeHandler( AuthorizeListener l ){
		listener = l;
		error = false;
		error_msg = "";
	}
	
	public void authorize(String login, String password){ 
		Log.d(TAG, login + " " + password);
    	List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
	    nameValuePairs.add(new BasicNameValuePair("request",AppSettings.REQUEST_AUTHORIZE_DRIVER));
	    nameValuePairs.add(new BasicNameValuePair("login", login));
	    nameValuePairs.add(new BasicNameValuePair("password", password));
	    String paramString = URLEncodedUtils.format(nameValuePairs, "utf-8");
		listener.OnBeginAuthorize(); 
    	execute(AppSettings.API_TAXITORG_DOMAIN + "?" + paramString);		
	}
	
	@Override
	protected String doInBackground(String... urls) {
    	try {
            Log.d(TAG, urls[0]);
            HttpPost httppost = new HttpPost(urls[0]);
            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            content = client.execute( httppost, responseHandler );
        } catch (ClientProtocolException e) {
        	Log.e(TAG, "ClientProtocolException");
        	e.printStackTrace();
        	error = true;
        	cancel(true);
         } catch (IOException e) {
        	Log.e(TAG, "IOException");
        	e.printStackTrace();
        	error = true;
        	cancel(true);
         }
		return content;
	}
	 
	@Override
    protected void onCancelled() {
        if( error ){
        	listener.OnErrorAuthorize("Offline");
        }
	}
	
	@Override
	protected void onPostExecute(String content) {
		Log.d(TAG, content);
		
		JSONArray result = null;
    	try {
    		JSONObject jsonObj = new JSONObject(content);
    		status = jsonObj.getString("status");
        	if( status.equals("ok") ){
        		if( !jsonObj.isNull("results") ){
        			result = jsonObj.getJSONArray("results");
        			listener.OnEndAuthorize(result);
        		} else {
        			error = true;
        			error_msg = "Auth fail";
        			listener.OnErrorAuthorize(error_msg);
        		}
        		
        	} else {
        		error = true;
        		error_msg = "Unknown error";
        		if( !jsonObj.isNull("error_message") ){
        		    error_msg = jsonObj.getString("error_message");
        		}
        		listener.OnErrorAuthorize(error_msg);
            }
    	} catch (JSONException e) {
			error = true;
			error_msg = "Error in parsing JSON from server";
			listener.OnErrorAuthorize(error_msg);
    		e.printStackTrace();
		}
	    	
	}

}