package com.martianlab.taxitorg.handlers;

public interface RegisterListener {
   void OnCompleteRegister(long driverId);
   void OnErrorRegister(String error_msg);
}
