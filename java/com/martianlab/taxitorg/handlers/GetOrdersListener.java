package com.martianlab.taxitorg.handlers;

public interface GetOrdersListener {
    void OnCompleteGetOrdersListener( String result );
    void OnErrorGetOrdersListener(String error_msg );
}
