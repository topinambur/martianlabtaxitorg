package com.martianlab.taxitorg.handlers;

import org.json.JSONArray;

public interface AuthorizeListener {
   void OnBeginAuthorize();
   void OnEndAuthorize( JSONArray driverParamsJSONArray );
   void OnErrorAuthorize(String errorMsg);
}
