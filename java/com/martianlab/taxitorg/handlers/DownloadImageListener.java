package com.martianlab.taxitorg.handlers;

import android.graphics.Bitmap;

public interface DownloadImageListener {
   void onDownloadImageComplete( int imageId, Bitmap data );
   void onDownloadImageError( String error_msg );
}
