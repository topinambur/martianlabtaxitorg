package com.martianlab.taxitorg.handlers;

import com.martianlab.taxitorg.common.Driver;

public interface SetOrderStatusListener {
   void OnSetOrderStatusComplete(int order_status, Driver driver, long order_id, String comment);
   void OnSetOrderStatusError(long order_id, String error_msg);
}
