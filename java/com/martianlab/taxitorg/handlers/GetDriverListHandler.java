package com.martianlab.taxitorg.handlers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.martianlab.taxitorg.AppSettings;

import android.os.AsyncTask;
import android.util.Log;

/**
 * Check order task class
 * @author awacs
 *
 */
public class GetDriverListHandler extends AsyncTask<String, Void, String> {
	
	private static final String TAG = "GetDriverListHandler";
	
	private final HttpClient client = new DefaultHttpClient();
	private String content;
	private String status;
	
	private long orderId;
	
	private GetDriverListListener listener;
	
	/**
	 * ������, � ������� �������� ������
	 */
	private boolean error = false;
	private String error_msg = "";
	
	public GetDriverListHandler( GetDriverListListener l ){
		this.listener = l;
	}
	
	public void GetDriverList( long order_id ){
    	this.orderId = order_id;
		
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
	    nameValuePairs.add(new BasicNameValuePair("request", AppSettings.REQUEST_GET_DRIVER_LIST));
	    nameValuePairs.add(new BasicNameValuePair("order_id", String.valueOf(orderId)));
	    String paramString = URLEncodedUtils.format(nameValuePairs, "utf-8");
	    execute( AppSettings.API_TAXITORG_DOMAIN + "?" + paramString );
	}
	
	@Override
	protected String doInBackground(String... urls) {
		
    	try {
            Log.d(TAG, "GetDriverListHandler::"+urls[0]);
            HttpPost httppost = new HttpPost(urls[0]);
            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            content = client.execute( httppost, responseHandler );
        } catch (ClientProtocolException e) {
        	error_msg = "ClientProtocolException";
        	e.printStackTrace();
        	error = true;
        	cancel(true);
         } catch (IOException e) {
         	error_msg = "IOException";
        	e.printStackTrace();
        	error = true;
        	cancel(true);
         }
		return content;
	}
	
    /**
     * ������ �� ����� ����������
     */
	@Override
    protected void onCancelled() {
        if( error ){
        	listener.OnErrorGetDriverList(orderId, error_msg);
        }
	}		
	
	@Override
	protected void onPostExecute(String content) {
		
	    JSONArray result = null;
		
    	try {
    		Log.d(TAG, content);
    		JSONObject jsonObj = new JSONObject(content);
    		status = jsonObj.getString("status");
        	if( status.equals("ok") ){
        		result = jsonObj.getJSONArray("results");
        		listener.OnCompleteGetDriverList(orderId, result);
        	} else {
        		error_msg = "Unknown error";
        		if( !jsonObj.isNull("error_message") ){
        		    error_msg = jsonObj.getString("error_message");
        		}
        		listener.OnErrorGetDriverList(orderId, error_msg);
            }
    	} catch (JSONException e) {
    		listener.OnErrorGetDriverList(orderId, e.getLocalizedMessage());
			e.printStackTrace();
		}
		
	}

}