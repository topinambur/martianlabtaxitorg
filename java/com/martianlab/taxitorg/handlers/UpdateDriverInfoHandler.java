package com.martianlab.taxitorg.handlers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;
import android.os.AsyncTask;
import android.util.Log;

import com.martianlab.taxitorg.AppSettings;

/**
 * ����� ��� �������� ������� �� ��������� ������� ������ � ������� statuses
 * ��������� ������ � statuses, ������������� ���������� ��������, �� ��������������� status=1
 * @author awacs
 *
 */
public class UpdateDriverInfoHandler extends AsyncTask<String, Void, String> {
	
	private static final String TAG = "AcceptOrderHandler";
	
	private final HttpClient client = new DefaultHttpClient();
	private String content;
	private String status;
	private boolean error = false; 
	private String error_msg = "";
	
	private UpdateDriverInfoListener listener;
	
	public UpdateDriverInfoHandler( UpdateDriverInfoListener l ){
		this.listener = l;
	}
	
	public void updateDriver( long driver_id, String params ){
    	List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
	    nameValuePairs.add(new BasicNameValuePair("request", AppSettings.REQUEST_UPDATE_DRIVER));
	    nameValuePairs.add(new BasicNameValuePair("driver_id", String.valueOf(driver_id) ));
	    nameValuePairs.add(new BasicNameValuePair("params", params ));
	    String paramString = URLEncodedUtils.format(nameValuePairs, "utf-8");
	    execute( AppSettings.API_TAXITORG_DOMAIN + "?" + paramString );
	}    	
	
	public boolean isError(){
		return error;
	}
	
	public String getErrorMsg(){
		return error_msg;
	}
	
    @Override
	protected String doInBackground(String... urls) {
		
    	try {
            Log.d(TAG, urls[0]);
    		HttpPost httppost = new HttpPost(urls[0]);
            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            content = client.execute( httppost, responseHandler );
        } catch (ClientProtocolException e) {
        	e.printStackTrace();
        	error = true;
        	cancel(true);
         } catch (IOException e) {
        	e.printStackTrace();
        	error = true;
        	cancel(true);
         }
		return content;
	}
	
	protected void onPostExecute(String content) {
		if (error) {
			error = true;
			error_msg = "Offline";
	    } else {
	    	try {
        		Log.d(TAG, content);
        		JSONObject jsonObj = new JSONObject(content);
        		status = jsonObj.getString("status");
	        	if( status.equals("ok") ){
	        		listener.OnUpdateDriverInfoComplete();
	        	} else {
	        		error = true;
	        		error_msg = "Unknown error";
	        		if( !jsonObj.isNull("error_message") ){
	        		    error_msg = jsonObj.getString("error_message");
	        		}
	        		listener.OnUpdateDriverInfoError(error_msg);
	            }
        	} catch (JSONException e) {
				error = true;
				error_msg = "JSONException";
				listener.OnUpdateDriverInfoError(error_msg);
        		e.printStackTrace();
			}
	    	
	    }     	
	}

}  	