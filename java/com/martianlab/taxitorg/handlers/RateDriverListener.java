package com.martianlab.taxitorg.handlers;

public interface RateDriverListener {
   void OnCompleteRateDriver( long order_id, long driver_id );
   void OnErrorRateDriver(String errorMsg);
}
