package com.martianlab.taxitorg.handlers;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import java.net.URL;
import java.net.URLConnection;

import com.martianlab.taxitorg.AppSettings;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;


/**
 * ���������� �������� � �������
 * 
 * @author awacs
 *
 */
public class DownloadImageHandler extends AsyncTask<String, Integer, Bitmap> {
	
	private static final String TAG = "DownloadImageHandler";
	
	private boolean error = false; 
	private String error_msg = "";	
	private long driver_id;
	private String dirForImages;
	
	// ��� ����������� - 1-������� 2-����
	private int type;
	private DownloadImageListener listener;
	
	public DownloadImageHandler( DownloadImageListener l ){ 
		listener = l;
	}
	
	public boolean isError(){
		return error;
	}
	
	public String getErrorMsg(){
		return error_msg;
	}	
	
	public void getImage( long _driver_id, int _type, String dir_for_images ){
		Log.d(TAG, "getImage");
		
		this.type = _type;
		this.driver_id = _driver_id; 
		this.dirForImages = dir_for_images;
		
		File taxitorgDirectory = new File(dirForImages);
	    if (!taxitorgDirectory.exists()) {
	        if( !taxitorgDirectory.mkdirs() ) {
	            Log.e(TAG, "Problem creating Image folder");
	        }
	    }
	    
	    if( taxitorgDirectory.exists() ){
	    	execute(AppSettings.URL_IMAGES + String.valueOf(driver_id) + String.valueOf(type) + AppSettings.IMAGES_EXTENSION);
	    }
	}
	
	
	@Override
	protected Bitmap doInBackground(String... sUrl) {
	    Bitmap bm = null;
	    try {
	        Log.d(TAG, sUrl[0]);
	    	URL url = new URL(sUrl[0]); 
	        URLConnection connection = url.openConnection();
	        
	        connection.connect();
	        
	        int fileLength = connection.getContentLength();
	        
	        InputStream input = new BufferedInputStream(url.openStream());
	        
	        String path = dirForImages + String.valueOf(driver_id) + String.valueOf(type) + AppSettings.IMAGES_EXTENSION;
	        
	        Log.d(TAG, path);
	        
	        
	        OutputStream output = new FileOutputStream( path );

	        byte data[] = new byte[1024];
            long total = 0;
            int count;
            while ((count = input.read(data)) != -1) {
                total += count;
                publishProgress((int) (total * 100 / fileLength));
                output.write(data, 0, count);
            }

	        output.flush();
            output.close();
	        input.close();
	        
	        BitmapFactory.Options options = new BitmapFactory.Options();
	        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
	        
	        bm = BitmapFactory.decodeFile(path, options);
	        
	    } catch (IOException e) {
	    	error = true;
	    	error_msg = "Error getting the image from server : " + e.getMessage().toString();
	        Log.e(TAG, "Error getting the image from server : " + e.getMessage().toString());
	    } 
	    return bm;
	}
	
    /**
     * ������ �� ����� ����������
     */
	@Override
    protected void onCancelled() {
        if( error ){
        	listener.onDownloadImageError(error_msg);
        }
	}  	
	
	@Override
	protected void onPostExecute(Bitmap result) {
		if(!error) {
			listener.onDownloadImageComplete(type, result);
		}
	}


}
