package com.martianlab.taxitorg.handlers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;
import android.os.AsyncTask;
import android.util.Log;

import com.martianlab.taxitorg.AppSettings;
import com.martianlab.taxitorg.common.Order;

public class CancelOrderHandler extends AsyncTask<String, Void, String> {
	
	private static final String TAG = "CancelOrderHandler";
	
	private final HttpClient client = new DefaultHttpClient();
	private Order order;
	private String content;
	private String status;
	private boolean error = false;  
	private String error_msg = "";
	
	private CancelOrderListener listener;
	
	public CancelOrderHandler( CancelOrderListener l ){
		this.listener = l;
	}
	
	public void cancelOrder( Order _order ){
		
		this.order = _order;
		
    	List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
	    nameValuePairs.add(new BasicNameValuePair("request", AppSettings.REQUEST_CANCEL_ORDER));
	    nameValuePairs.add(new BasicNameValuePair("order_id", String.valueOf(order.getId()) ));
	    String paramString = URLEncodedUtils.format(nameValuePairs, "utf-8");
	    execute( AppSettings.API_TAXITORG_DOMAIN + "?" + paramString );
	}    	
	
    @Override
	protected String doInBackground(String... urls) {
		
    	try {
            Log.d(TAG, urls[0]);
    		HttpPost httppost = new HttpPost(urls[0]);
            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            content = client.execute( httppost, responseHandler );
        } catch (ClientProtocolException e) {
        	Log.e(TAG, e.getMessage());
        	error = true;
        	cancel(true);
         } catch (IOException e) {
       	    Log.e(TAG, e.getMessage()); 
        	error = true;
        	cancel(true);
         }
		return content;
	}
	
    /**
     * ������ �� ����� ����������
     */
	@Override
    protected void onCancelled() {
        if( error ){
        	listener.onCancelOrderError("Offline");
        }
	}    
    
	@Override
	protected void onPostExecute(String content) {
		if (error) {
			error = true;
			error_msg = "Offline";
	    } else {
	    	try {
        		Log.d(TAG, content);
        		JSONObject jsonObj = new JSONObject(content);
        		status = jsonObj.getString("status");
	        	if( status.equals("ok") ){
	        		listener.onCancelOrderCompleted( order );
	        	} else {
	        		error = true;
	        		error_msg = "Unknown error";
	        		if( !jsonObj.isNull("error_message") ){
	        		    error_msg = jsonObj.getString("error_message");
	        		}
	        		listener.onCancelOrderError(error_msg);
	            }
        	} catch (JSONException e) {
				error = true;
				error_msg = "JSONException";
				listener.onCancelOrderError(error_msg);
        		e.printStackTrace();
			}
	    	
	    }     	
	}

}  	