package com.martianlab.taxitorg.handlers;

import com.martianlab.taxitorg.common.Driver;

public interface GetDriverInfoListener {
   void OnBeginGetDriverInfo( long driver_id );
   void OnEndGetDriverInfo( Driver driver );
   void OnErrorGetDriverInfo(String errorMsg);
}
