package com.martianlab.taxitorg;


import java.io.File;
import java.util.Locale;

import com.martianlab.taxitorg.R;
import com.martianlab.taxitorg.common.DBAdapter;
import com.martianlab.taxitorg.common.Driver;
import com.martianlab.taxitorg.common.Order;
import com.martianlab.taxitorg.driver.ScreenDriverLogin;
import com.martianlab.taxitorg.handlers.RateDriverHandler;
import com.martianlab.taxitorg.handlers.RateDriverListener;
import com.martianlab.taxitorg.passenger.ScreenPassengerMain;
import com.martianlab.taxitorg.passenger.ScreenPassengerOrder;
import com.martianlab.taxitorg.service.TaxitorgServiceDriver;
import com.martianlab.taxitorg.service.TaxitorgServicePassenger;

import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings.Secure;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.PopupWindow;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;


/**
 * ������� ���� ����������. �����������: 
 *   ������������ � ����� �������� ��� ���������
 *   ����� ������ ���/���/���
 *   �������� ���� � �����������
 * @author awacs
 *
 */
public class ScreenMainActivity extends Activity implements RateDriverListener {
	
    private final static String TAG = "ScreenMainActivity";
	
	private DBAdapter db;
	
	private String current_locale;
	
	private PopupWindow popUpInfo;
	private PopupWindow popUpShare;
	
	private String device_id;
	
	//private Typeface typeFaceBanner;
	
	//private TextView advertisingPhraseTextView;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_main);
        //advertisingPhraseTextView = (TextView) findViewById(R.id.screen_main_advertising_phrase);
        //typeFaceBanner = Typeface.createFromAsset(getAssets(), "fonts/banner_font_bold.ttf");
        //typeFaceBanner = Typeface.createFromAsset(getAssets(), "fonts/times.ttf");
        //advertisingPhraseTextView.setTypeface(typeFaceBanner);
        initApp();
    }
    
    /**
     * ������������� ����������
     */
    private void initApp(){  
    	
        //Button backButton = (Button) findViewById(R.id.header_backbutton);
        //backButton.setVisibility(View.GONE);
        
        //TextView title = (TextView) findViewById(R.id.header_title);
        //title.setText(getString(R.string.title_activity_main));
        
        //backButton.setOnClickListener(new OnClickListener(){
        //	@Override
		//	public void onClick(View v) {
        //		finish();
		//	}
        //});    	
    	
    	// �������� device_id
    	device_id = Secure.getString(getApplicationContext().getContentResolver(), Secure.ANDROID_ID);
    	
    	// ���� ������
    	try{
    		db = new DBAdapter(this);
    	} catch ( Error e ) {
			Toast toast = Toast.makeText(getApplicationContext(), e.getLocalizedMessage(), Toast.LENGTH_LONG);
			toast.show(); 
    	}
    	
		File taxitorgDirectory = new File(Environment.getExternalStorageDirectory(), AppSettings.DIR_TAXITORG);
		Log.d(TAG, "Taxitorg directory: " + taxitorgDirectory.toString());
	    if (!taxitorgDirectory.exists()) {
	        if( !taxitorgDirectory.mkdirs() ) {
	            Log.e(TAG, "Problem creating Image folder");
	        }
	    } else {
	    	Log.d(TAG, "Taxitorg directory already exist");
	    }
    	
		// �������� ������ ������ ���������
    	startPassengerService();
    	
		// �������� ������ ������ ��������
        startDriverService();
        
    	SharedPreferences settings = getSharedPreferences(AppSettings.APP_PREFERENCES, Context.MODE_PRIVATE);
    	current_locale = settings.getString(AppSettings.APP_PREFERENCES_LOCALE, "en");
    	changeLocale( current_locale ); 
    }
    
    
    /**
     * ��������� ��� �� ����������� ���������, ���������� �������
     */
    private void checkUnratedDriver(){
    	long driverId = -1;
    	long orderId = -1;
    	String orderParams = "";
    	String driverParams = "";
    	try{
    		db.open();
    		Cursor c = db.getUnratedDriver();
        	if( c.moveToFirst() ) {
        		driverId = c.getLong( c.getColumnIndex(DBAdapter.KEY_PASSENGER_ORDERS_DRIVERID) );
        		orderId = c.getLong( c.getColumnIndex(DBAdapter.KEY_PASSENGER_ORDERS_ORDERID) );
        		orderParams = c.getString( c.getColumnIndex(DBAdapter.KEY_PASSENGER_ORDERS_PARAMS) );
        		driverParams = c.getString( c.getColumnIndex(DBAdapter.KEY_PASSENGER_ORDERS_DRIVERPARAMS) );
        	}
        	c.close();
    		db.close();
    	} catch ( Error e )  {
			e.printStackTrace();
			Toast toast = Toast.makeText(getApplicationContext(), e.getLocalizedMessage(), Toast.LENGTH_LONG);
			toast.show(); 
    	} finally{
    		db.close();
    	}
    	
    	if( driverId > 0 && orderId > 0 ){
    		rateDriver( driverId, orderId, orderParams, driverParams );
    	}
    }
    
    
    /**
     * �������� ������ ������������� ��������
     * @param _driver_id
     * @param _order_id
     */
    private void rateDriver( long _driver_id, long _order_id, String orderParams, String driverParams ){
    	
    	Log.d(TAG, "rateDriver " + _driver_id + _order_id + orderParams + driverParams);
    	
    	final long driverId = _driver_id;
    	final long orderId = _order_id;  
    	
    	Driver driver = new Driver();
    	driver.setParams( driverParams );
    	
    	Order order = new Order();
    	order.setParams( orderParams );
    	
    	LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
    	View popupLayout = inflater.inflate(R.layout.popup_rate_driver, null);
    	
    	
    	AlertDialog.Builder builder = new AlertDialog.Builder(this);
    	builder.setTitle(getString(R.string.popup_rate_driver_title));
    	builder.setView(popupLayout);
    	 
    	final TextView descriptionTextView = (TextView) popupLayout.findViewById(R.id.popup_rate_driver_description);
    	final RatingBar ratingBar = (RatingBar) popupLayout.findViewById(R.id.popup_rate_driver_rating_bar); 
    	
    	String descriptionFormat = getString( R.string.popup_rate_driver_description );
    	String descriptionStr = String.format( 
    			descriptionFormat
    		  , driver.getNickname()
    		  , order.getAddressStart()
    		  , order.getAddressEnd() 
    	);

    	// ������ �������
    	builder.setPositiveButton(getString( R.string.popup_rate_driver_button_rate ),new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				int rating = (int) ratingBar.getRating();
				if( rating > 0 ){
					rate_driver(orderId, driverId, rating);
					dialog.cancel();
				}
			}
		});

    	// ������ ��������
    	builder.setNeutralButton(getString( R.string.popup_rate_driver_button_defer ),new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				dialog.cancel();
			}
		});    	
    	
    	// ������ �� ���������
    	builder.setNegativeButton(getString( R.string.popup_rate_driver_button_cancel ),new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				// TODO: �������� � ���� ��� ����������������
				dialog.cancel();
			}
		});    	
    	
    	descriptionTextView.setText( descriptionStr ); 
    	
    	final AlertDialog popupWindow =  builder.create();
    	popupWindow.show();
    	
    }
    
    /**
     * ����� ������� ��������, ���� �������������
     */
    private void startDriverService(){
        if( !isTaxitorgServiceDriverRunning() ){
    		Intent intent = new Intent(this, TaxitorgServiceDriver.class);
    		startService(intent);         
    	}    	
    }

    /**
     * ����� ������� ���������, ���� �������������
     */
    private void startPassengerService(){
        if( !isTaxitorgServicePassengerRunning() ){
        	Intent intent = new Intent(this, TaxitorgServicePassenger.class);
    		startService(intent);        	
        }    		
    	
    }
    
    /**
     * ��� �������� �� ������ ����� ��������� �������� ������, ������ ������� ���������
     */
    @Override
    protected void onResume(){
    	super.onResume();
    	checkUnratedDriver();
    }
    
    @Override
    protected void onStart(){
		super.onStart();
    }
    
    @Override
    protected void onPause(){ 
    	if( popUpInfo != null && popUpInfo.isShowing() ){
    		popUpInfo.dismiss(); 
    	}
    	if( popUpShare != null && popUpShare.isShowing() ){
    		popUpShare.dismiss(); 
    	}    	
    	super.onPause();
    }    
    
    /**
     * ����� ��������� � ������ ���������. ���� ��� �������� �������, ��������� ����� �� ����� �������� ������
     * @param v
     */
	public void startPassengerButton_click(View v){
    	// ��������� ���������� �������. ���� ���� - ��������� ����� �� ����� �������� ������
		int countOrders = 0;
		try{
			db.open();
			countOrders = db.getOrdersCount();
		} catch ( Error e )  {
			e.printStackTrace();
			Toast toast = Toast.makeText(getApplicationContext(), e.getLocalizedMessage(), Toast.LENGTH_LONG);
			toast.show(); 
    	} finally{
    		db.close();
    	}

		Intent intent = new Intent();
		if( countOrders > 0 ){
		    intent.setClass(ScreenMainActivity.this, ScreenPassengerMain.class); 
		} else {
		    intent.setClass(ScreenMainActivity.this, ScreenPassengerOrder.class);
		}
		startActivity(intent);
    }    
    
    /**
     * ����� ��������� � ������ ��������
	 *	@param v
     */
    public void startDriverButton_click(View v){
    	Intent intent = new Intent(); 
		intent.setClass(ScreenMainActivity.this, ScreenDriverLogin.class); 
		startActivity(intent);
    }
    
    
    /**
     * ������ PopUp � �������� ��������� ���������� � ���������
     * @param v
     */
    public void screen_main_button_information_click(View v){
    	showInfoPopUp(); 
    }
    
    /**
     * ������ PopUp � �������� "���������� � ������"
     * @param v
     */
    public void screen_main_button_share_click(View v){
    	showSharePopUp();
    }
    
    /**
     * ��������� ����� "About - � ���������"
     * @param v
     */
    public void button_popup_info_about_click(View v){
    	popUpInfo.dismiss();
    	Intent intent = new Intent(); 
    	intent.setClass(ScreenMainActivity.this, ScreenMainInfoActivity.class);
    	intent.putExtra("content", "about");
		startActivity(intent);    	
    }

    /**
     * ��������� ����� "FAQ - ����� ���������� �������"
     * @param v
     */
    public void button_popup_info_faq_click(View v){
    	popUpInfo.dismiss();
    	Intent intent = new Intent(); 
    	intent.setClass(ScreenMainActivity.this, ScreenMainInfoActivity.class);
    	intent.putExtra("content", "faq");
		startActivity(intent);    	
    	
    }

    /**
     * ��������� ����� "License - ���������������� ����������"
     * @param v
     */
    public void button_popup_info_license_click(View v){
    	popUpInfo.dismiss();
    	Intent intent = new Intent(); 
    	intent.setClass(ScreenMainActivity.this, ScreenMainInfoActivity.class);
    	intent.putExtra("content", "license");
		startActivity(intent);    	
    }
    
    /**
     * ������� ���������� ���� � ������������ ������� ���������� � �������/����������
     * @param v
     */
    public void button_popup_info_cancel_click(View v){
    	popUpInfo.dismiss();
    }
    
    /**
     * ������������� �� ���������
     * @param v
     */
    public void screen_main_button_locale_spanish_click(View v){
    	//changeLocale( "es" );
        //setContentView(R.layout.activity_screen_main);
    	//advertisingPhraseTextView.setTypeface(typeFaceBanner);
    }

    /**
     * ������������� �� �������
     * @param v
     */
    public void screen_main_button_locale_russian_click(View v){
    	changeLocale( "ru" );
        setContentView(R.layout.activity_screen_main);
        //advertisingPhraseTextView.setTypeface(typeFaceBanner);
    }
    
    /**
     * ������������� �� ����������
     * @param v
     */
    public void screen_main_button_locale_english_click(View v){
    	changeLocale( "en" );
        setContentView(R.layout.activity_screen_main);
        //advertisingPhraseTextView.setTypeface(typeFaceBanner);
    }
    
    
    
    
    /**
     * ������� POPUP � ���������� ��������� ������� � ���������
     * @param v
     */
    private void showInfoPopUp(){
    	
    	DisplayMetrics metrics = this.getApplicationContext().getResources().getDisplayMetrics();
    	int width = metrics.widthPixels;
    	int height = metrics.heightPixels;    	
    	
    	popUpInfo = new PopupWindow(this); 
    	 
    	LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View popLayout = inflater.inflate(R.layout.popup_info, null);
    	
        popUpInfo.setContentView(popLayout);
        popLayout.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        popUpInfo.setWidth( width );
        
        int defaultHeight = Math.round( height * 0.8f ); 
        
        if( defaultHeight > popLayout.getMeasuredHeight() ){
        	popUpInfo.setHeight( defaultHeight );
        } else {
        	popUpInfo.setHeight( popLayout.getMeasuredHeight() );        	
        }

        
        popUpInfo.showAtLocation(popLayout, Gravity.CENTER, 0, 0);
        
    }    

    /**
     * ������� POPUP ��� "���������� � ������"
     * @param v
     */
    private void showSharePopUp(){
    	final Intent intent = new Intent(Intent.ACTION_SEND);
    	intent.setType("text/plain");
    	intent.putExtra(Intent.EXTRA_SUBJECT, "������ ����������");
    	intent.putExtra(Intent.EXTRA_TEXT, "������ ����� ������ ���������� TAXITORG! http://www.taxitorg.com (���� ������ �� ������)");
    	startActivity(Intent.createChooser(intent, getString(R.string.app_name)));    	
    }        
    
    /**
     * ������� ������ ����������
     * @param locale
     */
    private void changeLocale( String locale ){
    	Resources res = getResources();
    	
    	DisplayMetrics dm = res.getDisplayMetrics();
        android.content.res.Configuration conf = res.getConfiguration();
        conf.locale = new Locale(locale);
        res.updateConfiguration(conf, dm);
    	storeLocaleInPreferences( locale ); // ��������� ����� ������ � ���������� ����������
    }
    
    /**
     * ��������� ����� ������ � ���������� ����������
     * @param _locale
     */
    private void storeLocaleInPreferences(String _locale){
    	SharedPreferences settings = getSharedPreferences(AppSettings.APP_PREFERENCES, Context.MODE_PRIVATE);
    	Editor editor = settings.edit();
    	editor.putString( AppSettings.APP_PREFERENCES_LOCALE, _locale );
    	editor.commit();
    }
    
    
    
    
	/**
	 * �������� ��������� ������� ��� �������� 
	 * @return
	 */
	private boolean isTaxitorgServiceDriverRunning() {
		ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
			if (AppSettings.BROADCAST_ACTION_DRIVER.equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}
    
    
	/**
	 * ��������� ������� �� ������ ������ ���������
	 * @return
	 */
	private boolean isTaxitorgServicePassengerRunning() {
		ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
			if (AppSettings.BROADCAST_ACTION_PASSENGER.equals(service.service.getClassName())) {
				return true; 
			}
		}
		return false;
	}
	
	// ================================================================
	// RateDriver
	// ================================================================
	
	/**
	 * �������� ������� � ���������� �������� ��������
	 */
	private void rate_driver(long order_id, long driver_id, int rating){
		new RateDriverHandler(this).rateDriver(order_id, driver_id, device_id, rating);
	}
	
	/**
	 * ������� ��������� ������������ �������� ��������, ������ �� ���� ������ � ��������� ��
	 */
	@Override
	public void OnCompleteRateDriver(long order_id, long driver_id) {
		try{
			db.open();
			db.rateDriver(driver_id, order_id);
		} catch ( Error e )  {
			e.printStackTrace();
			Toast toast = Toast.makeText(getApplicationContext(), e.getLocalizedMessage(), Toast.LENGTH_LONG);
			toast.show(); 
    	} finally{
    		db.close();
			Toast toast = Toast.makeText(getApplicationContext(), getString(R.string.success_rate_driver), Toast.LENGTH_LONG);
			toast.show(); 
    	}
	}
	
	/**
	 * ������ ��� ������������ �������� ��������
	 */
	@Override
	public void OnErrorRateDriver(String errorMsg) {
		Toast toast = Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_LONG);
		toast.show(); 
	}    
    
}
